﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.Symbols
{
	using System.Reflection;
	using System.Linq.Expressions;

	internal delegate object DynamicStaticMethod(params object[] args);

	/// <summary>
	/// Generates compiled delegates for these late-bound invocations:
	///		- Calling a constructor;
	///		- Calling a method;
	///		- Calling a property getter/setter;
	///		- Calling a field setter/setter;
	/// Inspired by: http://kohari.org/2009/03/06/fast-late-bound-invocation-with-expression-trees/
	/// Measured times show that calling It's about 24 times faster than calling MethodInfo.Invoke()
	/// </summary>
	internal static class LateBoundMethodFactory
	{
		// static cached reflection
		
		private static readonly MethodInfo ThrowArgumentNullExceptionInfo;
		private static readonly MethodInfo VoidThrowArgumentNullExceptionInfo;
		private static readonly MethodInfo ThrowTargetParameterCountExceptionInfo;
		
		static LateBoundMethodFactory()
		{
			BindingFlags staticNonPublic = BindingFlags.Static | BindingFlags.NonPublic;
			ThrowArgumentNullExceptionInfo = typeof(LateBoundMethodFactory).GetMethod("ThrowArgumentNullException", staticNonPublic);
			VoidThrowArgumentNullExceptionInfo = typeof(LateBoundMethodFactory).GetMethod("VoidThrowArgumentNullException", staticNonPublic);
			ThrowTargetParameterCountExceptionInfo = typeof(LateBoundMethodFactory).GetMethod("ThrowTargetParameterCountException", staticNonPublic);
		}

		/// <summary>
		///	Creates and compiles an Expression like this:
		/// (object[] args) =>
		/// (object)(
		///     (args == null) ? (object)ThrowArgumentNullException("args") :
		///     (args.Length != {constructor.ArgumentCount}) ? (object)ThrowTargetParameterCountException() :
		///     new {ConstructedType}(args[0], args[1], ...)
		/// )
		/// </summary>
		static public DynamicStaticMethod CreateConstructor(ConstructorInfo constructor)
		{
			if (constructor == null) throw new ArgumentNullException("constructor");
			
			//Create 'args' parameter expression
			ParameterExpression argsParameter = Expression.Parameter(typeof(object[]), "args");
			
			//Create body expression
			ParameterInfo[] constructorParams = constructor.GetParameters();
			Expression body = Expression.New(
				constructor,
				CreateParameterExpressions(constructorParams, argsParameter)
			);
			
			//Prepend parameter validation
			body = EnsureArgumentCount(argsParameter, constructorParams.Length, body);
			body = EnsureParameterNotNull(argsParameter, body);
			
			//Create and compile lambda
			var lambda = Expression.Lambda<DynamicStaticMethod>(
				Expression.Convert(body, typeof(object)),
				argsParameter
			);
			return lambda.Compile();
		}
		
		// private helper methods

		private static Expression[] CreateParameterExpressions(ParameterInfo[] methodParams, ParameterExpression argsParameter)
		{
			return methodParams.Select(
				(parameter, index) =>
				Expression.Convert(
					Expression.ArrayIndex(argsParameter, Expression.Constant(index)),
					parameter.ParameterType
				)
			).ToArray();
		}

		private static ConditionalExpression EnsureParameterNotNull(ParameterExpression parameter, Expression continuation)
		{
			//Choose how to throw exception according to continuation type
			Type continuationType = continuation.Type;
			Expression throwArgNullExpression;
			if (continuationType == typeof(void)) {
				throwArgNullExpression = Expression.Call(VoidThrowArgumentNullExceptionInfo, Expression.Constant(parameter.Name));
			} else {
				throwArgNullExpression =  Expression.Convert(
					Expression.Call(ThrowArgumentNullExceptionInfo, Expression.Constant(parameter.Name)),
					continuationType
				);
			}
			
			//Build conditional expression
			return Expression.Condition(
				Expression.Equal(parameter, Expression.Constant(null)),
				throwArgNullExpression,
				continuation
			);
		}

		private static ConditionalExpression EnsureArgumentCount(ParameterExpression argsParameter, int expectedArgCount, Expression continuation)
		{
			//Build conditional expression
			return Expression.Condition(
				Expression.NotEqual(Expression.ArrayLength(argsParameter), Expression.Constant(expectedArgCount)),
				Expression.Convert(
					Expression.Call(ThrowTargetParameterCountExceptionInfo),
					continuation.Type
				),
				continuation
			);
		}

		[System.Diagnostics.DebuggerHidden]
		private static object ThrowArgumentNullException(string paramName)
		{
			throw new ArgumentNullException(paramName);
		}

		[System.Diagnostics.DebuggerHidden]
		private static void VoidThrowArgumentNullException(string paramName)
		{
			throw new ArgumentNullException(paramName);
		}

		[System.Diagnostics.DebuggerHidden]
		private static object ThrowTargetParameterCountException()
		{
			throw new TargetParameterCountException();
		}
	}
}
