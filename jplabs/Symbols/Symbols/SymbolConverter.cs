﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Globalization;

namespace JpLabs.Symbols
{
	internal static class SymbolLateBoundConverter
	{
		private static IDictionary<Type,DynamicStaticMethod> contructorFuncDict = new Dictionary<Type,DynamicStaticMethod>();

		private static ConstructorInfo GetSymbolConstructorInfo(Type enumType)
		{
			return typeof(Symbol<>)
				.MakeGenericType(enumType)
				.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new []{ typeof(Symbol) }, null)
			;
		}

		private static DynamicStaticMethod CreateSymbolConstructorFunc(Type enumType)
		{
			return LateBoundMethodFactory.CreateConstructor(GetSymbolConstructorInfo(enumType));
		}

		private static DynamicStaticMethod GetSymbolConstructor(Type enumType)
		{
			DynamicStaticMethod func;
			if (!contructorFuncDict.TryGetValue(enumType, out func)) {
				func = CreateSymbolConstructorFunc(enumType);
				contructorFuncDict[enumType] = func;
			}
			return func;
		}

		public static Symbol MakeTyped(Type enumType, Symbol original)
		{
			return (Symbol)GetSymbolConstructor(enumType)(original);
		}

		public static Symbol ConvertTo(this Symbol symbol, Type destinationType)
		{
			return (Symbol)Symbol.Converter.ConvertTo(symbol, destinationType);
		}
	}

	internal class SymbolConverter : TypeConverter
	{
		//http://www.hanselman.com/blog/TypeConvertersTheresNotEnoughTypeDescripterGetConverterInTheWorld.aspx
		
		public static bool TypeIsSymbol(Type type)
		{
			if (typeof(Symbol).IsAssignableFrom(type)) return true;

			return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Symbol<>);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || TypeIsSymbol(sourceType) || base.CanConvertFrom(context, sourceType);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return TypeIsSymbol(destinationType) || base.CanConvertFrom(context, destinationType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string) return Symbol.Parse((string)value);

			if (value is Symbol) return value;
			
			return base.ConvertFrom(context, culture, value);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null) throw new ArgumentNullException("destinationType");

			if (destinationType == typeof(object)) return value;

			if (TypeIsSymbol(destinationType)) {
				var sourceType = value.GetType();
				if (destinationType.IsAssignableFrom(sourceType)) return value;

				var destinationEnumType = destinationType.GetGenericArguments().First();
				var symbol = (Symbol)value;
				if (!symbol.Is(destinationEnumType)) throw GetInvalidCastException(symbol.EnumType, destinationType);
			
				return SymbolLateBoundConverter.MakeTyped(destinationEnumType, value as Symbol);
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool IsValid(ITypeDescriptorContext context, object value)
		{
			return value == null || value is Symbol;
		}

		static InvalidCastException GetInvalidCastException(Type enumType, Type destinationType)
		{
			return new InvalidCastException(string.Format("Can't convert from 'Symbol of {0}' to '{1}'", enumType, destinationType));
		}
	}
}
