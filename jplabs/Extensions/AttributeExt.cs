﻿using System;
using System.Linq;
using System.Reflection;

namespace JpLabs.Extensions
{
	public static class AttributeExt
	{
		public static T GetSingleAttrOrNull<T>(this ICustomAttributeProvider source, bool inherit) where T : Attribute
		{
			return (T)source.GetCustomAttributes(typeof(T), inherit).SingleOrDefault();
		}
		
		public static T GetSingleAttr<T>(this ICustomAttributeProvider source, bool inherit) where T : Attribute
		{
			return (T)source.GetCustomAttributes(typeof(T), inherit).Single();
		}

		public static bool IsDefinedAttr<T>(this ICustomAttributeProvider source, bool inherit) where T : Attribute
		{
			return source.IsDefined(typeof(T), inherit);
		}

		//public static IEnumerable<T> GetSingleAttrOrEmpty<T>(this ICustomAttributeProvider source, bool inherit) where T : Attribute
		//{
		//    var attr = source.GetSingleAttrOrNull<T>(inherit);
		//    return (attr != null) ? EnumerableExt.Yield(attr) : Enumerable.Empty<T>();
		//}
	}
}
