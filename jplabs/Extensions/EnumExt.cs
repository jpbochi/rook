﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Serialization;

namespace JpLabs.Extensions
{
	public static class EnumExt
	{
		/// <summary>
		/// Returns the contents of the <see cref="DescriptionAttribute"/>
		/// or the <see cref="XmlEnumAttribute"/> applied to the member of this enum.
		/// </summary>
		/// <param name="value">The enum member whose description will be returned.</param>
		/// <returns>The description of this enum member.</returns>
		public static string GetDescription(this Enum value)
		{
			string valueAsString = value.ToString();
			
			var fi = value.GetType().GetField(valueAsString);
			
			var descAttrib = fi.GetSingleAttrOrNull<DescriptionAttribute>(false);
			if (descAttrib != null) return descAttrib.Description;

			var xmlEnumAttrib = fi.GetSingleAttrOrNull<XmlEnumAttribute>(false);
			return (xmlEnumAttrib != null) ? xmlEnumAttrib.Name : valueAsString;
		}
	}
}
