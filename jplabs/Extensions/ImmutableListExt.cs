﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.Extensions
{
	public static class ImmutableListExt
	{
		public static class EmptyList<T>
		{
			public static readonly IList<T> Instance = Array.AsReadOnly(new T[0]);
		}
		
	    public static IList<T> ImutAdd<T>(this IList<T> source, T item)
	    {
			int currLen = source.Count;
			T[] newArray = new T[currLen + 1];
			
			source.CopyTo(newArray, 0);
			newArray[currLen] = item;
			
			return Array.AsReadOnly(newArray); //System.Collections.ObjectModel.ReadOnlyCollection<int>()
			
			////Simpler, but much slower solution
			//return Array.AsReadOnly(source.Concat(item).ToList().ToArray());
	    }

		public static IList<T> ImutRemoveAt<T>(this IList<T> source, int index)
		{
			return Array.AsReadOnly(source.Where( (ev, i) => (i != index) ).ToArray());
		}

		public static IList<T> ImutClear<T>(this IList<T> source)
		{
			return EmptyList<T>.Instance; //Array.AsReadOnly(new T[0]);
		}
		
		//--- "Immutable" Array (http://blogs.msdn.com/ericlippert/archive/2008/09/22/arrays-considered-somewhat-harmful.aspx)

		public static class EmptyArray<T>
		{
			public static readonly T[] Instance = new T[0];
		}
		
	    public static T[] ImutAdd<T>(this T[] source, T item)
	    {
			int currLen = source.Length;
			T[] newArray = new T[currLen + 1];
			
			source.CopyTo(newArray, 0);
			newArray[currLen] = item;
			
			return newArray;
	    }

		public static T[] ImutRemoveAt<T>(this T[] source, int index)
		{
			return source.Where( (ev, i) => (i != index) ).ToArray();
		}

		public static T[] ImutClear<T>(this T[] source)
		{
			return EmptyArray<T>.Instance; //new T[0];
		}
	}
}
