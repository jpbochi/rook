﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Extensions;
using Should;
using Should.Core;

namespace JpLabs.Extensions.Tests
{
	public class MultiValueDictionaryTests
	{
		MultiValueDictionary<string,string> dict = new MultiValueDictionary<string,string> {
			{ "key1", "value1" },
			{ "key1", "value2" },
			{ "key2", "value3" }
		};

		[Fact]
		void should_return_the_count_of_keys()
		{
			dict.Count.ShouldEqual(2);
		}

		[Fact]
		void should_get_values_from_their_key()
		{
			dict["key1"].ShouldContainSameAs(new [] { "value1", "value2" });
			dict["key2"].ShouldContainSameAs(new [] { "value3" });
			dict["key3"].ShouldBeEmpty();
		}

		[Fact]
		void should_yield_values()
		{
			dict.Values.ShouldContainSameAs(new [] { "value1", "value2", "value3" });
		}

		[Fact]
		void should_tell_if_it_contains_a_value_pair()
		{
			dict.Contains("key1", "value1").ShouldBeTrue();
			dict.Contains("key1", "value3").ShouldBeFalse();
			dict.Contains("key2", "value1").ShouldBeFalse();
		}

		[Fact]
		void should_tell_if_it_contains_a_key()
		{
			dict.ContainsKey("key1").ShouldBeTrue();
			dict.ContainsKey("key0").ShouldBeFalse();
		}
	}
}
