﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace JpLabs.Extensions
{
	internal class Grouping<TKey, TElement> : IGrouping<TKey, TElement>
	{
		private readonly TKey _key;
		private readonly IEnumerable<TElement> _elements;

		public Grouping(TKey key, IEnumerable<TElement> elements)
		{
			_key = key;
			_elements = elements;
		}

		TKey IGrouping<TKey, TElement>.Key
		{
			get { return _key; }
		}
		
		public IEnumerator<TElement> GetEnumerator()
		{
			if(_elements==null)
			{
				return null;
			}
			return _elements.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
	}
}
