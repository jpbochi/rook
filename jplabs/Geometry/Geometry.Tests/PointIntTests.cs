﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using Xunit;
using Xunit.Extensions;

namespace Geometry.Tests
{
	public class PointIntTests
	{
		[Theory]
		[InlineData("{0,1}", new [] {0, 1})]
		[InlineData("{1,0}", new [] {1, 0})]
		[InlineData("{0,1,2}", new [] {0, 1, 2})]
		[InlineData("{2,1,0}", new [] {2, 1, 0})]
		[InlineData("{5,6,7,8}", new [] {5, 6, 7, 8})]
		void should_parse_valid_integer_points(string stringToParse, int[] values)
		{
			var point = PointInt.Parse(stringToParse);

			Assert.Equal(values, point.ToArray());
		}
	}
}
