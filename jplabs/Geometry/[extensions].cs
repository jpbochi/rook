﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace JpLabs.Geometry
{
	internal static class BitwiseRotateExt
	{
		static public int RotateLeft(this int value, int count)
		{
			//return (int)RotateLeft((uint)value, count);
			return (value << count) | (value >> (32 - count));
		}

		static public int RotateRight(this int value, int count)
		{
			return (value >> count) | (value << (32 - count));
		}

		static public uint RotateLeft(this uint value, int count)
		{
			return (value << count) | (value >> (32 - count));
		}

		static public uint RotateRight(this uint value, int count)
		{
			return (value >> count) | (value << (32 - count));
		}
	}

	public static class PointExt
	{
		static public IPoint<int> Create(params int[] p)
		{
			return new PointInt(p);
		}

		static public IPoint<double> Create(params double[] p)
		{
			return new PointReal(p);
		}

		static public double DistanceSquared(this IPoint<int> p1, IPoint<int> p2)
		{
			int dim = p1.Dimensions;
			if (p2.Dimensions != dim) throw new ArgumentException("Points have to have the same number of dimensions");

			int len = 0;
			for (int i=0; i<dim; i++) {
				var d = p1[i] - p2[i];
				len += d * d;
			}
			return len;
		}

		static public double DistanceSquared(this IPoint p1, IPoint p2)
		{
		    return p1.Subtract(p2).LengthSquared;
		}
	}
	
	public static class BoundingBoxExt
	{
		static public IBoundingBox Create(IPoint<int> p1, IPoint<int> p2)
		{
			return new BoundingBoxInt((PointInt)p1, (PointInt)p2);
		}

		static public IBoundingBox Create(IPoint<double> p1, IPoint<double> p2)
		{
			return new BoundingBoxReal((PointReal)p1, (PointReal)p2);
		}

		static public IBoundingBox MergeAll(this IEnumerable<IBoundingBox> source)
		{
			//*
			return source.Aggregate((partial, b) => partial.Merge(b));			
			/* /
			return source.Cast<BoundingBoxInt>().Aggregate((partial, b) => partial + b);
			/* /
			/// This is a attempt to optimize the calculation of a bounding box of several bounding boxes
			/// Incredibly enough, it proved to be slower than other approaches
			var boxes = source.Cast<BoundingBoxInt>();

			int dim = -1;
			int[] low = null;
			int[] upp = null;

			using (var it = boxes.GetEnumerator()) {
				//Find first non-void box
				while (it.MoveNext()) {
					var b = it.Current;
					if (!b.IsVoid) {
						dim = b.Dimensions;
						low = new int[dim];
						upp = new int[dim];
						for (int i=0; i<dim; i++) {
							low[i] = b.Lower[i];
							upp[i] = b.Upper[i];
						}
						break;
					}
				}
				
				//Merge with the others
				if (dim == -1) {
					return BoundingBoxInt.VoidBoundingBox; //All boxes are void
				} else {
					while (it.MoveNext()) {
						var b = it.Current;
						if (!b.IsVoid) {
							//if (b.Dimensions != dim) throw new ArgumentException("Boxes have to have the same number of dimensions");
							
							for (int i=0; i<dim; i++) {
								low[i] = Math.Min(low[i], b.Lower[i]);
								upp[i] = Math.Max(upp[i], b.Upper[i]);
							}
						}
					}
					return new BoundingBoxInt(new PointInt(low), new PointInt(upp));
				}
			}
			//*/
		}

		//static public IBoundingBox Intersection(this IBoundingBox b1, IBoundingBox b2)
		//{
		//    return ((BoundingBoxInt)b1) & ((BoundingBoxInt)b2);
		//}
		
		static public IBoundingBox GetVoidBoundingBox()
		{
			return BoundingBoxInt.VoidBoundingBox;
		}
	}
}
