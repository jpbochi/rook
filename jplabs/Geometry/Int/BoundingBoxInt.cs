﻿using System;
using System.Linq;

using TNumeric = System.Int32;

namespace JpLabs.Geometry
{
	/*
	public sealed class VoidBoundingBox : IBoundingBox<int>
	{
		static public VoidBoundingBox Instance = new VoidBoundingBox();
		private VoidBoundingBox() {}
		
		IPoint<int> IBoundingBox<int>.Lower		{ get { return null; } }
		IPoint<int> IBoundingBox<int>.Upper		{ get { return null; } }
		IPoint<int> IBoundingBox<int>.Center	{ get { return null; } }
		public IPoint Lower		{ get { return null; } }
		public IPoint Upper		{ get { return null; } }
		public IPoint Center	{ get { return null; } }
		public int Surface		{ get { return 0; } }
		public int Volume		{ get { return 0; } }
		public int Dimensions	{ get { return -1; } }
		public bool IsVoid		{ get { return true; } }

		public IBoundingBox GetBoundingBox()
		{
			return this;
		}
	}//*/
	
	public class BoundingBoxInt : IBoundingBox<TNumeric>
	{
		private readonly PointInt lower;
		private readonly PointInt upper;
		private readonly VectorInt diff;

		private int volume = -1;
		private int surface = -1;
		//private bool isPunctual;
		private bool isVoid;
		private int dimensions;
		
		public PointInt Lower							{ get {return lower;} }
		public PointInt Upper							{ get {return upper;} }
		IPoint<TNumeric> IBoundingBox<TNumeric>.Lower	{ get {return lower;} }
		IPoint<TNumeric> IBoundingBox<TNumeric>.Upper	{ get {return upper;} }
		IPoint IBoundingBox.Lower						{ get {return lower;} }
		IPoint IBoundingBox.Upper						{ get {return upper;} }
		
		double IBoundingBox.Surface			{ get {return (double)this.Surface;} }
		double IBoundingBox.Volume			{ get {return (double)this.Volume;} }

		//public bool IsPunctual	{ get; protected set; }
		//public bool IsVoid		{ get; protected set; }
		//public int Dimensions	{ get; protected set; }

		public bool IsPunctual	{ get { return (diff.LengthSquared == 0); } }
		public bool IsVoid		{ get { return isVoid; } }
		public int Dimensions	{ get { return dimensions; } }
		
		
		IPoint<TNumeric> IBoundingBox<TNumeric>.Center { get { return this.Center; } }
		IPoint IBoundingBox.Center			 { get { return this.Center; } }

		static public BoundingBoxInt VoidBoundingBox = new BoundingBoxInt();
		
		public BoundingBoxInt(PointInt p1, PointInt p2)
		{
			if (p1.Dimensions != p2.Dimensions) throw new ArgumentException("Points must have same number of dimensions");
			
			dimensions = p1.Dimensions;
			
			TNumeric[] vLower = new TNumeric[dimensions];
			TNumeric[] vUpper = new TNumeric[dimensions];
			for (int i=0; i<dimensions; i++) {
				if (p1[i] < p2[i]) {
					vLower[i] = p1[i];
					vUpper[i] = p2[i];
				} else {
					vLower[i] = p2[i];
					vUpper[i] = p1[i];
				}
			}
			this.lower = new PointInt(vLower);
			this.upper = new PointInt(vUpper);
			
			this.diff = GetDiffVector(vLower, vUpper);
			
			//isPunctual = (diff.LengthSquared == 0); //(this.lower == this.upper);
			
			//if (IsPunctual) upper = lower; //ensure single PointInt instance (update: PointInt is not a class anymore)
		}

		internal BoundingBoxInt(TNumeric[] lower, TNumeric[] upper)
		{
			dimensions = lower.Length;
			
			this.lower = new PointInt(lower);
			this.upper = new PointInt(upper);
			
			this.diff = GetDiffVector(lower, upper);
			//isPunctual = (diff.LengthSquared == 0); //(this.lower == this.upper);
		}

		public BoundingBoxInt(PointInt p)
		{
			dimensions = p.Dimensions;
			lower = p;
			upper = p;
			isVoid = false;
			//isPunctual = true;
		}

		protected BoundingBoxInt()
		{
			isVoid = true;
		}
		
		IBoundingBox IShape.GetBoundingBox()
		{
			return this;
		}

		bool IShape.Contains(IShape other)
		{
			return this.Contains(other);
		}

		bool IShape.Intersects(IShape other)
		{
			return this.Intersects(other);
		}
		
		public PointInt Center
		{
			get {
				if (this.isVoid) return default(PointInt);
				//if (this.isPunctual) return lower;
				var r = new TNumeric[dimensions];
				for (int i=0; i<dimensions; i++ ) r[i] = (int)Math.Round((upper[i] + lower[i]) / 2d);
				return new PointInt(r);
			}
		}

		private VectorInt GetDiffVector(TNumeric[] lower, TNumeric[] upper)
		{
			/// Minimum volume is 1 x 1 x 1
			int dim = lower.Length;
			var v = new TNumeric[dim];
			
			for (int i=0; i<dim; i++ ) v[i] = upper[i] - lower[i] + 1;
			return new VectorInt(v);
		}
		
		//private VectorInt DiffVector
		//{
		//    get {
		//        return GetDiffVector(lower.InternalArray, upper.InternalArray);
		//    }
		//}
		
		public int Volume
		{
			get {
				if (this.isVoid) return 0;
				if (volume == -1) {
					/*
					volume = DiffVector.Aggregate((val1, val2) => val1 * val2);
					/*/
					//Optimization attempt
					{
						var diff = this.diff.InternalArray; //DiffVector.InternalArray;
						int dim = dimensions;
						TNumeric vol = diff[0];
						for (int i=1; i<dim; i++) vol *= diff[i];
						volume = vol;
					}
					//*/
				}
				return volume;
			}
		}
		
		public int Surface
		{
			get {
				if (this.isVoid) return 0;
				if (surface == -1) {
					var diff = this.diff.InternalArray; //DiffVector.InternalArray;
					var dim = this.dimensions;
					if (dim == 2) {
						surface = (diff[0] + diff[1]) * 2;
					} else if (dim == 3) {
						surface = diff[0] * diff[1];
						surface += diff[0] * diff[2];
						surface += diff[1] * diff[2];
						surface *= 2;
					} else {
						/*
						var q = from i in Enumerable.Range(0, 2)
								from j in Enumerable.Range(0, i)
								select new {I = i, J = j};
						/*/
						//give just an estimate
						surface = diff.Sum();
						//*/
					}
				}
				return surface;
			}
		}
		
		internal IBoundingBox<TNumeric> Merge(BoundingBoxInt other)
		{
			return this + other;
		}

		internal IBoundingBox<TNumeric> Intersection(BoundingBoxInt other)
		{
			return this & other;
		}

		IBoundingBox IBoundingBox.Merge(IBoundingBox other)
		{
			return this + (BoundingBoxInt)other;
		}

		IBoundingBox IBoundingBox.Intersection(IBoundingBox other)
		{
			return this & (BoundingBoxInt)other;
		}
		
		/// <summary> + = Merge operator </summary>
		static public BoundingBoxInt operator +(BoundingBoxInt b1, BoundingBoxInt b2)
		{
			if (b1 == null || b1.isVoid) return b2;
			if (b2 == null || b2.isVoid) return b1;
			if (b1.dimensions != b2.dimensions) throw new ArgumentException("Boxes must have same number of dimensions");
			
			/*
			var dim = b1.Dimensions;
			var low = new TNumeric[dim];
			var upp = new TNumeric[dim];
			var b1Lower = b1.Lower; var b1Upper = b1.Upper;
			var b2Lower = b2.Lower; var b2Upper = b2.Upper;
			for (int i=0; i<dim; i++) {
				low[i] = Math.Min(b1Lower[i], b2Lower[i]);
				upp[i] = Math.Max(b1Upper[i], b2Upper[i]);
			}
			return new BoundingBoxInt(new PointInt(low), new PointInt(upp));
			/*/
			int dim = b1.dimensions;
			var b1Lower = b1.lower.ToArray();	var b1Upper = b1.upper.ToArray();
			var b2Lower = b2.lower.ToArray();	var b2Upper = b2.upper.ToArray();
			var low = new TNumeric[dim];
			var upp = new TNumeric[dim];
			for (int i=0; i<dim; i++) {
				low[i] = Math.Min(b1Lower[i], b2Lower[i]);
				upp[i] = Math.Max(b1Upper[i], b2Upper[i]);
			}
			return new BoundingBoxInt(low, upp);
			//*/
		}

		/// <summary> & = Insertection operator </summary>
		static public BoundingBoxInt operator &(BoundingBoxInt b1, BoundingBoxInt b2)
		{
			if (b1 == null || b1.isVoid) return VoidBoundingBox;
			if (b2 == null || b2.isVoid) return VoidBoundingBox;
			if (b1.dimensions != b2.dimensions) throw new ArgumentException("Boxes must have same number of dimensions");
			
			var dim = b1.dimensions;
			var b1Lower = b1.lower.ToArray(); var b1Upper = b1.upper.ToArray();
			var b2Lower = b2.lower.ToArray(); var b2Upper = b2.upper.ToArray();
			var low = new TNumeric[dim];
			var upp = new TNumeric[dim];
			for (int i=0; i<dim; i++) {
				low[i] = Math.Max(b1Lower[i], b2Lower[i]);
				upp[i] = Math.Min(b1Upper[i], b2Upper[i]);
				if (low[i] > upp[i]) return VoidBoundingBox;
			}
			
			return new BoundingBoxInt(low, upp);
		}

		static public bool operator >(BoundingBoxInt bContainer, BoundingBoxInt bContained)
		{
			return bContainer.Contains(bContained);
		}

		static public bool operator <(BoundingBoxInt bContained, BoundingBoxInt bContainer)
		{
			return bContained.Contains(bContainer);
		}

		public override string ToString()
		{
			if (isVoid)		return "{box: void}";
			if (IsPunctual)	return string.Concat("{box: ", Lower.ToString(), "}");
			
			return string.Concat("{box: lower = ", lower.ToString(), ", upper = ", upper.ToString(), "}");
		}
	}
}
