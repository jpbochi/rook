﻿//#define POINT_IS_CLASS

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using TNumeric = System.Int32;

namespace JpLabs.Geometry
{
	/*
	[DataContract]
	#if POINT_IS_CLASS
	sealed public class PointInt : IPoint<TNumeric>, IEnumerable<TNumeric>, ICloneable
	#else
	public struct PointInt : IPoint<TNumeric>, IEnumerable<TNumeric>, ICloneable
	#endif
	{
		#if POINT_IS_CLASS
		public static PointInt Null { get { return new PointInt(0); } }
		#else
		public static PointInt Null { get { return default(PointInt); } }
		#endif

		[DataMember(EmitDefaultValue=false)]
		private readonly TNumeric x;
		[DataMember(EmitDefaultValue=false)]
		private readonly TNumeric y;
		[DataMember(EmitDefaultValue=false)]
		private readonly TNumeric z;

		[DataMember(EmitDefaultValue=false)]
		public int Dimensions;

		//private TNumeric[] v;
		
		public TNumeric X { get {return x;} }
		public TNumeric Y { get {return y;} }
		public TNumeric Z { get {return z;} }

		int IShape.Dimensions { get { return Dimensions; } }
		
		public PointInt(TNumeric dimensions)
		{
			Dimensions = dimensions;
		    this.x = 0;
			this.y = 0;
			this.z = 0;

			//this.v = new TNumeric[0];
		}

		public PointInt(TNumeric x, TNumeric y)
		{
			Dimensions = 2;
		    this.x = x;
			this.y = y;
			this.z = 0;

			//this.v = new []{x, y};
		}
		
		public PointInt(TNumeric x, TNumeric y, TNumeric z)
		{
			Dimensions = 3;
		    this.x = x;
			this.y = y;
			this.z = z;

			//this.v = new []{x, y, z};
		}

		internal PointInt(bool reuseArray, params TNumeric[] p) : this(p)
		{}

		internal PointInt(params TNumeric[] p)
		{
			Dimensions = p.Length;
		    this.x = p[Core.DimX];
			this.y = p[Core.DimY];
			this.z = (Dimensions >= 3) ? p[Core.DimZ] : 0;

			//this.v = new TNumeric[Dimensions];
			//p.CopyTo(v, 0);
		}
		
		internal TNumeric[] ToArray()
		{
			//return v;
			return new [] { x, y, z };
		}
		
		public TNumeric this[int index]
		{
			get { return ToArray()[index]; }
		}

		IComparable IPoint.this[int index]
		{
		    get { return ToArray()[index]; }
		}

		static public VectorInt operator -(PointInt p1, PointInt p2)
		{
			var dim = Math.Min(p1.Dimensions, p2.Dimensions);
			//var r = new TNumeric[dim];
			//for (int i=0; i<dim; i++) r[i] = p1[i] - p2[i];
			//return new PointInt(r);

			if (dim == 2) return new VectorInt(p1.x - p2.x, p1.y - p2.y);
			if (dim == 3) return new VectorInt(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z);
			throw new NotSupportedException();
		}
		
		static public PointInt operator -(PointInt p, VectorInt v)
		{
			var dim = Math.Min(p.Dimensions, v.Dimensions);
			//var r = new TNumeric[dim];
			//for (int i=0; i<dim; i++) r[i] = p[i] - v[i];
			//return new PointInt(r);

			if (dim == 2) return new PointInt(p.x - v.X, p.y - v.Y);
			if (dim == 3) return new PointInt(p.x - v.X, p.y - v.Y, p.z - v.Z);
			throw new NotSupportedException();
		}

		static public PointInt operator +(PointInt p, VectorInt v)
		{
			return new PointInt(p.x + v.X, p.y + v.Y);
			var dim = Math.Min(p.Dimensions, v.Dimensions);
			//var r = new TNumeric[dim];
			//for (int i=0; i<dim; i++) r[i] = p[i] + v[i];
			//return new PointInt(r);

			if (dim == 2) return new PointInt(p.x + v.X, p.y + v.Y);
			if (dim == 3) return new PointInt(p.x + v.X, p.y + v.Y, p.z + v.Z);
			throw new NotSupportedException();
		}
		
		#if POINT_IS_CLASS
		static public bool operator ==(PointInt point1, PointInt point2)
		{
			return Equals(point1, point2);
		}		
		static public bool operator !=(PointInt point1, PointInt point2)
		{
			return !Equals(point1, point2);
		}		
		
		public override bool Equals(object obj)
		{
			if (obj is PointInt) return Equals(this, (PointInt)obj);
			return false;
		}

		static bool Equals(PointInt p, PointInt q)
		{
			if (((object)p) == null) return ((object)q) == null;
			if (((object)q) == null) return false;
			
			int dim = p.Dimensions;
			if (dim != q.Dimensions) return false;
			for (int i=0; i<dim; i++) if (p[i] != p[i]) return false;
			return true;
		}
		#else
		static public bool operator ==(PointInt p1, PointInt p2)
		{
			return p1.Equals(p2);
		}
		static public bool operator !=(PointInt p1, PointInt p2)
		{
			return !p1.Equals(p2);
		}
		
		public override bool Equals(object obj)
		{
			if (obj is PointInt) return Equals((PointInt)obj);
			return false;
		}
		
		public bool Equals(PointInt other)
		{
			return this.x == other.x
				&& this.y == other.y
				&& this.z == other.z
				&& this.Dimensions == other.Dimensions;
		}
		#endif

		public override int GetHashCode()
		{
			//int rotateBits = 32 / (Dimensions + 1); //rotateBits for 3 dimensions == 8 bits
			const int rotateBits = 8; //i.e., one byte

			if (Dimensions == 0) return -1;
			
			int hash = x;
			hash ^= y.RotateLeft(rotateBits);
			if (Dimensions >= 3) hash ^= z.RotateLeft(rotateBits * 2);
			return hash;
		}

		public override string ToString()
		{
			return this.Aggregate(
				new StringBuilder(),
				(str, value) => ((str.Length == 0) ? str.Append("{") : str.Append(",")).Append(value)
			).Append("}").ToString();
		}

		IVector IPoint.Subtract(IPoint other)
		{
			return this - (PointInt)other;
		}

		bool IShape.IsVoid
		{
			get { return false; }
		}

		IBoundingBox IShape.GetBoundingBox()
		{
			return new BoundingBoxInt(this);
		}

		bool IShape.Contains(IShape other)
		{
			return this.Contains(other);
		}

		bool IShape.Intersects(IShape other)
		{
			return this.Intersects(other);
		}

		public IEnumerator<TNumeric> GetEnumerator()
		{
			yield return x;
			yield return y;
			if (Dimensions >= 3) yield return z;
		}

		IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

		public object Clone()
		{
			//TODO: since PointInt is a struct, isn't it safe to simply return 'this'?
			//TODO: Dimension information is being lost here
			return new PointInt(x, y, z);
		}
	}
	//*/
}
