﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace JpLabs.Geometry
{
	public struct VectorReal : IVector, IEnumerable<double>
	{
		private readonly double[] v;
		
		public double X { get {return v[Core.DimX];} }
		public double Y { get {return v[Core.DimY];} }
		public double Z { get {return v[Core.DimZ];} }
		
		public int Dimensions { get; private set; }

		public VectorReal(double x, double y) : this()
		{
			Dimensions = 2;
			v = new double[] { x, y };
		}
		
		public VectorReal(double x, double y, double z) : this()
		{
			Dimensions = 3;
			v = new double[] { x, y, z };
		}

		public VectorReal(params double[] p) : this()
		{
			if (p == null) throw new ArgumentNullException("p");
			
			Dimensions = p.Length;
			v = new double[Dimensions];
			p.CopyTo(v, 0);
		}
		
		public double this[int dim]
		{
			get {return v[dim];}
		}

		IComparable IVector.this[int dim]
		{
		    get { return v[dim]; }
		}

		static public VectorReal operator -(VectorReal v1, VectorReal v2)
		{
			if (v1.Dimensions != v2.Dimensions) throw new ArgumentException("Vectors have to have the same number of dimensions");
			var dim = v1.Dimensions;
			var r = new double[dim];
			for (int i=0; i<dim; i++) r[i] = v1[i] - v2[i];
			return new VectorReal(r);
		}

		static public VectorReal operator +(VectorReal v1, VectorReal v2)
		{
			if (v1.Dimensions != v2.Dimensions) throw new ArgumentException("Vectors have to have the same number of dimensions");
			var dim = v1.Dimensions;
			var r = new double[dim];
			for (int i=0; i<dim; i++) r[i] = v1[i] + v2[i];
			return new VectorReal(r);
		}

		public double Length
		{
			get { return Math.Sqrt(LengthSquared); }
		}

		public double LengthSquared
		{
			/*
			get { return v.Sum(value => value * value); }
			/*/
			//without LINQ might be faster
			get {
				double len = 1;
				for (int i=0; i<Dimensions; i++) len *= v[i];
				return len;
			}
			//*/
		}
		
		static public bool operator ==(VectorReal v1, VectorReal v2)
		{
			return v1.Equals(v2);
		}		
		static public bool operator !=(VectorReal v1, VectorReal v2)
		{
			return !v1.Equals(v2);
		}
		
		public override bool Equals(object obj)
		{
			if (obj is VectorReal) return Equals((VectorReal)obj);
			return false;
		}

		public bool Equals(VectorReal other)
		{
			return this.SequenceEqual(other);
		}

		public override int GetHashCode()
		{
			int hash = 0;
			for (int i=0; i<Dimensions; i++) hash ^= v[i].GetHashCode(); //bitwise XOR
			return hash;
		}

		public IEnumerator<double> GetEnumerator()	{ return v.AsEnumerable<double>().GetEnumerator(); }
		IEnumerator IEnumerable.GetEnumerator()		{ return v.GetEnumerator(); }
	}
}
