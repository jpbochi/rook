﻿using System;

namespace JpLabs.Geometry
{
	/// GENERICS PROBLEM:
	///	* How to make R-trees this work with Int and Real systems
	///   with the minimum code duplication and without loosing performance (by boxing ints and doubles)?
	/// 
	/// - Looks like the bigger problem resides in IBoundingBox's Surface and Volume properties
	/// - Maybe the best approach is make everything double by default and optimize in some parts when using an Int system
	/// - Investigate use of unsafe code with pointers (http://msdn.microsoft.com/en-us/library/t2yzs44b.aspx)
	/// - Maybe I can implement some super generics using this: System.Reflection.Emit.DynamicMethod
	
	internal static class Core
	{
		public const int DimX = 0;
		public const int DimY = 1;
		public const int DimZ = 2;
		public const int DimW = 3;
	}

	public interface IShape
	{
		int Dimensions { get; }
		bool IsVoid { get; }
		
		IBoundingBox GetBoundingBox();
		bool Contains(IShape other);
		bool Intersects(IShape other);
	}

	public interface IPoint : IShape
	{
		IComparable this[int dim] { get; }
		IVector Subtract(IPoint other);
	}

	public interface IVector
	{
		IComparable this[int dim] { get; }
		double Length { get; }
		double LengthSquared { get; }
	}

	public interface IPoint<T> : IPoint where T : struct, IComparable
	{
		new T this[int dim] { get; }
	}

	public interface IBoundingBox : IShape
	{
		IPoint Lower { get; }
		IPoint Upper { get; }
		IPoint Center { get; }
		
		bool IsPunctual { get; }
		
		IBoundingBox Merge(IBoundingBox other);
		IBoundingBox Intersection(IBoundingBox other);

		double Surface { get; }
		double Volume { get; }
	}

	internal interface IBoundingBox<T> : IBoundingBox where T : struct, IComparable
	{
		new IPoint<T> Lower { get; }
		new IPoint<T> Upper { get; }
		new IPoint<T> Center { get; }

		new T Surface { get; }
		new T Volume { get; }
	}
}
