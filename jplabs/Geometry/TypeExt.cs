﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.Extensions
{
	internal class TypeComparer : IComparer<Type>
	{
		static public IComparer<Type> Instance = new TypeComparer();
		
		private TypeComparer() {}
		
		public int Compare(Type x, Type y)
		{
			return TypeExt.Compare(x, y);
		}
	}
	
	internal static class TypeExt
	{
		static public int Compare(this Type x, Type y)
		{
			//if (x == null) {
			//    if (y == null) return 0; //null == null
			//    return -1; // null < y
			//}
			//if (y == null) return +1; // x > null
			if (x == null) throw new ArgumentNullException("x");
			if (y == null) throw new ArgumentNullException("y");
			
			if (x == y) return 0; //x == y
			if (x.IsAssignableFrom(y)) return +1; //x > y
			if (y.IsAssignableFrom(x)) return -1; //x < y
			return 0; //x and y are not directly related
		}
		
		static public bool IsBiggerOrEqual(this Type x, Type y)
		{
			return x.IsAssignableFrom(y);
		}

		static public bool IsLittlerOrEqual(this Type x, Type y)
		{
			return y.IsAssignableFrom(x);
		}
		
		static public object GetDefaultValue(this Type type)
		{
		    return type.IsValueType ? Activator.CreateInstance(type) : null;
		}
		
		static public bool IsAssignableFromValue(this Type type, object value)
		{
			if (value == null) return IsAssignableFromNull(type);
			
			return type.IsAssignableFrom(value.GetType());
		}

		static public bool IsAssignableFromNull(this Type type)
		{
			return !type.IsValueType || (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
		}
	}
}
