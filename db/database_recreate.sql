
DROP SCHEMA IF EXISTS `rook`;
CREATE SCHEMA `rook`;
USE `rook`;

DROP USER 'aspnetdev'@'localhost';
CREATE USER 'aspnetdev'@'localhost' IDENTIFIED BY 'aspnetdev';

GRANT USAGE ON *.* TO 'aspnetdev'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, SHOW VIEW ON `rook`.* TO 'aspnetdev'@'localhost';

CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `owner_user_id` varchar(256) DEFAULT NULL,
  `game_type` varchar(256) DEFAULT NULL,
  `zipped_data` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `last_played_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

delimiter $$

DROP PROCEDURE IF EXISTS `clear_old_games`$$
CREATE PROCEDURE `clear_old_games`(
    days int
)
BEGIN

	delete from `games`
	where owner_user_id like 'rook.apphb.com/guest%'
	and TIMESTAMPDIFF(DAY, last_played_at, UTC_TIMESTAMP()) >= days;

END
