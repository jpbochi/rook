echo off
set mysql="C:\Program Files (x86)\MySQL\MySQL Workbench 5.2 CE\"

echo Recreating DB schema AND 'aspnetdev' user using 'root' user...
%mysql%\mysql.exe -h localhost -u root -p -vvv -e "source database_recreate.sql" > "recreate_db.output.txt"

pause
