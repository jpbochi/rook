﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using JpLabs.Geometry;

namespace Rook
{
	public static class Tiles
	{
		public static RName MainBoard = "_board_";
		public static RName Abstract = "_class_";
		public static RName OffBoard = "_off_";
		public static PointInt OffBoardPosition { get { return PointInt.Null; } }
	}

	public static class TilePos
	{
		private static readonly Regex TileRegex = new Regex("(?<colsign>_?)(?<col>[a-z]+)(?<rowsign>-?)(?<row>[0-9]+)", RegexOptions.Compiled);

		public static PointInt Create(int col, int row)
		{
			return new PointInt(col, row);
		}

		public static RName ToTileName(this PointInt point)
		{
			if (point == Tiles.OffBoardPosition) return Tiles.OffBoard;

			int col = point[0];
			int row = point[1];

			var colSign = (col < 0) ? "_" : "";
			var rowSign = (row < 0) ? "-" : "";
			col = (col < 0) ? -col-1 : col;
			row = (row < 0) ? -row-1 : row;

			var strCol = TilePos.ToBase26(col);
			var strRow = (row + 1).ToString();

			var digits = Math.Max(strCol.Length, strRow.Length);

			strCol.PadLeft(digits, 'a');
			strRow.PadLeft(digits, '0');

			return string.Concat(colSign, strCol, rowSign, strRow);
		}

		public static PointInt? ToPointInt(this RName tileName)
		{
			return FromName(tileName.ToString());
		}

		public static PointInt? FromName(string tileName)
		{
			if (string.IsNullOrWhiteSpace(tileName) || tileName == Tiles.OffBoard) return Tiles.OffBoardPosition;

			var match = TileRegex.Match(tileName);
			if (!match.Success) return null;

			var strCol = match.Groups["col"].Value;
			var strRow = match.Groups["row"].Value;

			int col = ParseBase26(strCol);
			int row = int.Parse(strRow) - 1;

			if (match.Groups["colsign"].Length > 0) { col = -col-1; }
			if (match.Groups["rowsign"].Length > 0) { row = -row-1; }

			return new PointInt(col, row);
		}

		public static string ToBase26(int value)
		{
			const int BASE = 26;

			var chars = new Stack<char>();

			while (true)
			{
				int digit = value % BASE;

				chars.Push((char)('a' + digit));

				value /= BASE;
				if (value == 0) break;
				value--;
			}

			return new string(chars.ToArray());
		}

		private static int ParseBase26(string input)
		{
			const int BASE = 26;
			int value = -1;

			foreach (var c in input) {
				if (c < 'a' || c > 'z') throw new ArgumentException();

				value++;
				value *= BASE;
				value += (int)(c - 'a');
			}

			return value;
		}
	}
}
