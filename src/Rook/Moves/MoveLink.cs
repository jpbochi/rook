﻿using System;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using JpLabs.Extensions;

namespace Rook
{
	public interface IMoveLink
	{
		string Rel { get; }
		Uri Href { get; }
		string Title { get; }
		object Data { get; }
	}

	public class MoveLink : IMoveLink
	{
		public static readonly Uri BaseUri = new Uri("http://rook");

		public string Rel { get; private set; }

		public Uri Href { get; private set; }
		public string Title { get; private set; }

		public object Data { get; private set; }

		internal MoveLink(Uri href, string relation, string title)
		{
			this.Href = href;
			this.Rel = relation;
			this.Title = title;
		}

		public static Uri BuildUri(UriTemplate template, object uriArgs = null)
		{
			var args = uriArgs.ToObjectDictionary(value =>
				//HttpUtility.UrlEncode((value ?? "").ToString())			//might be necessary for AugProperties
				(value ?? "").ToString()
			);
			return new Uri(template.BindByName(BaseUri, args).PathAndQuery, UriKind.Relative);
		}

		public static MoveLink From(UriTemplate template, string relation, object uriArgs, object data = null)
		{
			var uri = BuildUri(template, uriArgs);
			var title = data.TryGetProperty("title") as string;
			return new MoveLink(uri, relation, title) { Data = data };
		}

		public static bool HasMatch(UriTemplate template, Uri moveUri)
		{
			return Match(template, moveUri) != null;
		}

		public static UriTemplateMatch Match(UriTemplate template, Uri moveUri)
		{
			return template.Match(BaseUri, new Uri(BaseUri, moveUri));
		}
	}

	public static class MoveLinkExt
	{
		public static XElement ToHyperlink(this IMoveLink self)
		{
			var link = new XElement("a", new XAttribute("rel", self.Rel), new XAttribute("href", self.Href));

			link.Add(
				self.Data.ToObjectDictionary().Select(
					kvp => new XAttribute("data-" + kvp.Key, kvp.Value)
				)
			);

			link.Add(self.Title ?? self.Rel);

			return link;
		}

		public static RName GetPlayerId(this IMoveLink self)
		{
			return self.Data.TryGetProperty("playerid") as RName;
		}

		public static RName GetOrigin(this IMoveLink self)
		{
			return self.Data.TryGetProperty("origin") as RName;
		}

		public static RName GetDestination(this IMoveLink self)
		{
			return self.Data.TryGetProperty("destination") as RName;
		}

		public static bool Is<T>(this IMoveLink self) where T : IGameMove
		{
			//TODO: remove this magic (looks like I've been programming in Ruby too much these days)
			var relationProp = typeof(T).GetField("Relation", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			var relation = relationProp.GetValue(null) as string;
			return self.Rel == relation;
		}
	}
}
