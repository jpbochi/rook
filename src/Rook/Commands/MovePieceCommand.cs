﻿using System;
using System.Collections;

namespace Rook.Commands
{
	internal class MovePieceCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return MovePieceCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new MovePieceCommand(
					state.Board.Pieces.ById(match.BoundVariables["piece"]),
					match.BoundVariables["destination"]
				);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/move/{piece}/{destination}");
		internal const string Relation = RookRelations.Commands.MovePiece;

		public IGamePiece Piece { get; private set; }
		public RName Destination { get; private set; }

		public MovePieceCommand(IGamePiece piece, RName destination)
		{
			this.Piece = piece;
			this.Destination = destination;
		}

		public IMoveLink Link
		{
			get { return MoveLink.From(Template, Relation, new { Piece = Piece.Id, Destination }); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			if (Piece == null) throw new InvalidOperationException();

			context.State.Board.MovePiece(Piece, Destination);
		}
	}
}
