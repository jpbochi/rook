﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Rook.Commands
{
	public class CycleTurnCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return CycleTurnCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new CycleTurnCommand();
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/cycle-turn");
		internal const string Relation = RookRelations.Commands.CycleTurn;

		public IMoveLink Link
		{
			get { return MoveLink.From(Template, Relation, null); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			var player = GetNextPlayer(context.State);
			context.State.CurrentPlayerId = (player == null) ? null : player.Id;
		}

		private static IEnumerable<T> AsCyclic<T>(IEnumerable<T> source, int maxCycles)
		{
			for (int i=0; i<maxCycles; i++) {
				foreach (var item in source) yield return item;
			}
		}

		public static IGamePlayer GetNextPlayer(IGameState state)
		{
			var players = state.Players;

			var currentPlayerId = state.CurrentPlayerId;
			if (!players.Any(p => p.Id == currentPlayerId)) return null;

			return AsCyclic(players, 2)
				.SkipWhile(p => p.Id != currentPlayerId).Skip(1) //Find current player and skip him/her
				.FirstOrDefault(p => p.Score == GameScore.StillPlaying);
		}
	}
}
