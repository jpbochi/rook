﻿using System;
using System.Collections;

namespace Rook.Commands
{
	internal class AddPieceCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return AddPieceCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new AddPieceCommand(match.BoundVariables["piece"], match.BoundVariables["position"]);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/add/{piece}/{position}");
		internal const string Relation = RookRelations.Commands.AddPiece;

		public RName Piece { get; private set; }
		public RName Position { get; private set; }

		public AddPieceCommand(RName pieceClass, RName position)
		{
			this.Piece = pieceClass;
			this.Position = position;
		}

		public IMoveLink Link
		{
			get { return MoveLink.From(Template, Relation, new { Piece, Position }); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			if (Piece == null) throw new InvalidOperationException();

			var board = context.State.Board;
			board.AddPiece(board.Pieces.ById(Piece).New(), Position);
		}
	}
}
