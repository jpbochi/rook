﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rook.Commands
{
	internal class SetPlayerScoreCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return SetPlayerScoreCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new SetPlayerScoreCommand(match.BoundVariables["player"], match.BoundVariables["score"]);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/set-player-score/{player}/{score}");
		internal const string Relation = RookRelations.Commands.SetPlayerScore;

		private readonly RName player;
		private readonly RName score;

		public SetPlayerScoreCommand(RName player, RName score)
		{
			this.player = player;
			this.score = score;
		}

		public IMoveLink Link
		{
			get { return MoveLink.From(Template, Relation, new { player, score }); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			context.State.GetPlayerById(player).Score = score;
		}
	}
}
