﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Symbols;
using Ninject;
using Ninject.Modules;
using Rook.Commands;

namespace Rook
{
	public class DefaultCommandsModule : NinjectModule
	{
		public override void Load()
		{
			Bind<IGameReferee>().To<GameCommandReferee>().InSingletonScope();

			Bind<IGameMove>().To<AddPieceCommand>().Named(CommandType.AddPiece);
			Bind<IGameMove>().To<MovePieceCommand>().Named(CommandType.MovePiece);
			Bind<IGameMove>().To<CapturePieceCommand>().Named(CommandType.CapturePiece);
			Bind<IGameMove>().To<CycleTurnCommand>().Named(CommandType.CycleTurn);
			Bind<IGameMove>().To<SetGamePropertyCommand>().Named(CommandType.SetGameProperty);
			Bind<IGameMove>().To<SetGameOverCommand>().Named(CommandType.SetGameOver);
		}
	}

	public class GameCommandReferee : IGameReferee
	{
		private static readonly MoveBuilderTable moveTable = new MoveBuilderTable(new [] {
			AddPieceCommand.Builder,
			MovePieceCommand.Builder,
			CapturePieceCommand.Builder,
			CycleTurnCommand.Builder,
			SetGamePropertyCommand.Builder,
			ResetGamePropertyCommand.Builder,
			SetGameOverCommand.Builder,
			SetPlayerPropertyCommand.Builder,
			SetPlayerScoreCommand.Builder,
		});

		public IEnumerable<IMoveLink> GetMoves(IGameState state)
		{
			return Enumerable.Empty<IMoveLink>();
		}

		public IGameMove FindMove(Uri moveUri, IGameState state)
		{
			return moveTable.Match(moveUri, state);
		}

		bool IGameReferee.ExecuteMove(Uri moveUri, IMoveExecutionContext context)
		{
			var move = moveTable.Match(moveUri, context.State);
			if (move == null) return false;
			move.Execute(context);
			return true;
		}
	}
}
