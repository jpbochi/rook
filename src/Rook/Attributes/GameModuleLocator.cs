﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using JpLabs.Extensions;
using Ninject.Modules;

namespace Rook
{
	public interface IModuleLocator
	{
		Type FindType(string moduleTypeName);
		string GetTypeName(Type moduleType);

		IEnumerable<Type> FindModuleTypes();
		IEnumerable<Type> FindInitializerTypesFor(Type moduleType);
	}

	public class GameModuleLocator : IModuleLocator
	{
		private readonly IEnumerable<Assembly> assemblies;

		internal GameModuleLocator(IEnumerable<Assembly> assemblies)
		{
			this.assemblies = assemblies;
		}

		public static IModuleLocator FromCurrentDomain()
		{
			return new GameModuleLocator(AppDomain.CurrentDomain.GetAssemblies());
		}

		public Type FindType(string gameTypeName)
		{
			if (gameTypeName == null) return null;

			return TypeExt.ResolveTypeFromFullName(gameTypeName, false, assemblies);
		}

		public string GetTypeName(Type gameType)
		{
			return gameType.FullName;
		}

		public IEnumerable<Type> FindModuleTypes()
		{
			return assemblies
				.Where(asm => !asm.IsDynamic && !asm.IsBuiltByMicrosoft())
				.SelectMany(asm => asm.GetExportedTypes())
				.Where(t => typeof(INinjectModule).IsAssignableFrom(t))
				.Where(t => t.IsDefinedAttr<GameModuleAttribute>(false));
			;
		}

		public IEnumerable<Type> FindInitializerTypesFor(Type gameType)
		{
			return assemblies
				.Where(asm => !asm.IsDynamic && !asm.IsBuiltByMicrosoft())
				.SelectMany(asm => asm.GetExportedTypes())
				.Where(type => {
					var attr = type.GetSingleAttrOrNull<GameInitializerAttribute>(false);
					return attr != null && attr.GameType == gameType;
				});
			;
		}
	}
}
