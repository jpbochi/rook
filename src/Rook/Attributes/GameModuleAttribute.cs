﻿using System;
using System.ComponentModel;
using Ninject.Modules;

namespace Rook
{
	public interface IGameModuleInitializer//<out T> where T : INinjectModule
	{
		void Initialize(INinjectModule module);
	}

	public class GameModuleAttribute : DescriptionAttribute
	{
		public string DisplayName { get; set; }
		public string WikiUrl { get; set; }
		public bool InDev { get; set; }

		public GameModuleAttribute(string displayName)
		{
			DisplayName = displayName;
		}

		public override string Description
		{
			get { return DisplayName; }
		}
	}

	public class GameInitializerAttribute : Attribute
	{
		public Type GameType { get; set; }

		public GameInitializerAttribute(Type gameType)
		{
			GameType = gameType;
		}
	}
}
