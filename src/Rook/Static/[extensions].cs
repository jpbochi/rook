﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Symbols;

namespace Rook
{
	public static class RookDataContracts
	{
		public const string Namespace = "http://jplabs.bochi.it/rook";
	}

	public static class AugObjectExt
	{
		public static T GetValue<T>(this IAugObject augObj, Augmentation.AugProperty ap)
		{
			var value = augObj.GetValue(ap);

			if (value is T) return (T)value;
			return (T)ap.ConvertValue(value);
		}

		public static Symbol<TEnum> GetSymbolValue<TEnum>(this IAugObject augObj, Augmentation.AugProperty ap) where TEnum : SymbolEnum
		{
			return augObj.GetValue<Symbol>(ap).As<TEnum>();
		}
	}

	public static class GameBoardExt
	{
		public static IGraphLink GetLabeledOutLink(this IGameTileGraph graph, RName origin, string label)
		{
			return graph.GetOutLinks(origin).FirstOrDefault(l => l.Label == label);
		}
	}

	public static class GameStateExt
	{
		public static bool IsOver(this IGameState state)
		{
			return state.CurrentPlayerId == null;
		}

		public static IEnumerable<Uri> SetWinnerCommands(this IGameState state, RName winner)
		{
			return state.Players
				.Where(p => p.Score == GameScore.StillPlaying)
				.Select(p => MoveLink.BuildUri(CommandTemplates.SetPlayerScore, new { Player = p.Id, Score = (p.Id == winner) ? GameScore.Won : GameScore.Lost }))
			;
		}

		public static IEnumerable<Uri> SetDrawCommands(this IGameState state)
		{
			return state.Players.Select(p => MoveLink.BuildUri(CommandTemplates.SetPlayerScore, new { Player = p.Id, Score = GameScore.Drawn }));
		}

		public static IGamePlayer GetPlayerById(this IGameState state, RName id)
		{
			return state.Players.FirstOrDefault(p => p.Id == id);
		}

		public static IGamePlayer GetCurrentPlayer(this IGameState state)
		{
			return state.Players.FirstOrDefault(p => p.Id == state.CurrentPlayerId);
		}
	}
}
