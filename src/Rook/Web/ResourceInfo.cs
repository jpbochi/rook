﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using JpLabs.Extensions;

namespace Rook	
{
	/// <summary>
	/// http://en.wikipedia.org/wiki/Internet_media_type
	/// http://www.w3schools.com/media/media_mimeref.asp
	/// </summary>
	public class MediaType
	{
		public const string Application = "application";
		public const string Image = "image";
		public const string Text = "text";

		public string Type { get; private set; }
		public string SubType { get; private set; }

		private MediaType(string type, string subtype)
		{
			Type = type;
			SubType = subtype;
		}

		public static MediaType Parse(string input)
		{
			var regex = new Regex(@"^(?<t>[\w-]+)/(?<s>[\w+-]+)$");

			var results = regex.Match(input);
			if (!results.Success) throw new FormatException();
			
			return new MediaType(results.Groups["t"].Value.ToLower(), results.Groups["s"].Value.ToLower());
		}

		public override string ToString()
		{
			return string.Concat(Type, "/", SubType);
		}
	}

	public interface IResourceInfo
	{
		string ResourceName { get; set; }

		string Description { get; set; }
		MediaType MediaType { get; set; }
		IDictionary<string,string> Data { get; set; }
	}

	public class WebResourceInfo : IResourceInfo
	{
		public Assembly Assembly { get; set; }
		public string ResourceName { get; set; }

		public string CssClass { get; set; }
		public string Description { get; set; }
		public MediaType MediaType { get; set; }
		public IDictionary<string,string> Data { get; set; }

		public Stream GetStream()
		{
			return Assembly.GetManifestResourceStream(ResourceName);
		}

		public override int GetHashCode()
		{
			return (Assembly == null ? 0 : Assembly.FullName.GetHashCode()) ^ (ResourceName ?? "").GetHashCode();
		}

		public string AsHtml(string url)
		{
			if (MediaType.Type != Rook.MediaType.Image) throw new NotSupportedException();

			return string.Format(
				"<img src='{0}' alt='{1}' class='{2}' {3} />",
				url,
				Description,
				CssClass,
				Data.EmptyIfNull().Select(kvp => string.Format("data-{0}='{1}'", kvp.Key, kvp.Value)).Join(" ")
			);
		}
	}
}
