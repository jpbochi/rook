﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using JpLabs.Extensions;

namespace Rook
{
	public class BasicGameMachine : IGameMachine
	{
		private IGameState state;
		private IGameReferee referee;

		IGameState IGameMachine.State { get { return state; } }

		public IEnumerable<IResourceInfo> Resources { get; private set; }
		public IJavascriptRenderer JSRenderer { get; private set; }

		public BasicGameMachine(IGameState state, IEnumerable<IGameReferee> referees, IJavascriptRenderer jsRenderer, params IResourceInfo[] resources)
		{
			this.state = state;
			this.referee = MasterReferee.MergeReferees(referees);
			this.JSRenderer = jsRenderer;
			this.Resources = resources.ToReadOnlyColl();
		}

		public IEnumerable<IMoveLink> GetMoveOptions()
		{
			return referee.GetMoves(state);
		}

		public bool TryPlay(Uri moveUri, IPrincipal user)
		{
			return referee.ExecuteMove(moveUri, new MoveExecutionContext(state, referee, user));
		}
	}
}
