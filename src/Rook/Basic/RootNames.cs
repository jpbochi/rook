﻿using System;
using JpLabs.Symbols;
using Rook.Commands;

namespace Rook
{
	public static class ClassicPlayer
	{
		const string Namespace = "player";

		public static readonly RName White = RName.Get("white", Namespace);
		public static readonly RName Black = RName.Get("black", Namespace);
	}

	public static class GameScore
	{
		const string Namespace = "score";

		public static readonly RName StillPlaying = RName.Get("playing", Namespace);
		public static readonly RName Won		  = RName.Get("won", Namespace);
		public static readonly RName Lost		  = RName.Get("lost", Namespace);
		public static readonly RName Drawn		  = RName.Get("drawn", Namespace);
		public static readonly RName Resigned	  = RName.Get("resigned", Namespace);
	}

	public static class RookRoles
	{
		public static RName Referee = RName.Get("referee", "role");
	}

	public static class RookRelations
	{
		public static class Commands
		{
			public const string AddPiece = "cmd/add-piece";
			public const string MovePiece = "cmd/move-piece";
			public const string CapturePiece = "cmd/capture-piece";

			public const string CycleTurn = "cmd/cycle-turn";
			public const string SetGameProperty = "cmd/set-property";
			public const string ResetGameProperty = "cmd/reset-property";
			public const string SetGameOver = "cmd/game-over";

			public const string UpdateScore = "cmd/update-score";
			public const string SetPlayerScore = "cmd/set-player-score";
			public const string SetPlayerProperty = "cmd/set-player-property";
		}

		public static class Moves
		{
			public const string MovePiece = "move-piece";
			public const string DropPiece = "drop-piece";

			public const string PassTurn = "pass-turn";
			public const string Resign = "resign";
		}
	}

	public static class CommandTemplates //TODO: drive REST through relations, not URI templates
	{
		public static UriTemplate AddPiece { get { return AddPieceCommand.Template; } }
		public static UriTemplate MovePiece { get { return MovePieceCommand.Template; } }
		public static UriTemplate CapturePiece { get { return CapturePieceCommand.Template; } }

		public static UriTemplate CycleTurn { get { return CycleTurnCommand.Template; } }
		public static UriTemplate SetGameProperty { get { return SetGamePropertyCommand.Template; } }
		public static UriTemplate ResetGameProperty { get { return ResetGamePropertyCommand.Template; } }

		public static UriTemplate SetGameOver { get { return SetGameOverCommand.Template; } }
		public static UriTemplate SetPlayerScore { get { return SetPlayerScoreCommand.Template; } }
		public static UriTemplate SetPlayerProperty { get { return SetPlayerPropertyCommand.Template; } }
	}

	public abstract class CommandType : SymbolEnum
	{
		static CommandType() { SymbolEnum.InitEnum<CommandType>(); }

		public static readonly Symbol<CommandType> AddPiece;
		public static readonly Symbol<CommandType> MovePiece;
		public static readonly Symbol<CommandType> CapturePiece;

		public static readonly Symbol<CommandType> CycleTurn;
		public static readonly Symbol<CommandType> SetGameProperty;
		public static readonly Symbol<CommandType> SetGameOver;

		public static readonly Symbol<CommandType> UpdateScore;
	}
}
