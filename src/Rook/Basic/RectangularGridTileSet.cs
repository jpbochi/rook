﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using JpLabs.Extensions;
using JpLabs.Geometry;
using MongoDB.Bson.Serialization.Attributes;

namespace Rook
{
	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class RectangularGridTileSet : IGraphTileSet
	{
		[DataMember(Order=0)]public ushort Cols { get; private set; } // aka Files
		[DataMember(Order=1)]public ushort Rows { get; private set; } // aka Ranks

		[IgnoreDataMember]private BoundingBoxInt boundingBox;

		public RectangularGridTileSet(ushort columns, ushort rows)
		{
			this.Cols = columns;
			this.Rows = rows;

			CreateBoundingBox();
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			CreateBoundingBox();
		}

		[BsonElement(null, Order=int.MaxValue)]
		[BsonDefaultValue(null), BsonIgnoreIfDefault]
		[DataMember]
		private object OnSerialization
		{
			get { return null; }
			set {
				OnDeserialized(default(StreamingContext));
			}
		}

		private void CreateBoundingBox()
		{
			boundingBox = new BoundingBoxInt(new PointInt(0, 0), new PointInt(Cols - 1, Rows - 1));
		}

		public bool Contains(RName tile)
		{
			var position = tile.ToPointInt();
			return (position != null) && position.Value.Dimensions == 2 && boundingBox.Contains(position.Value);
		}

		[IgnoreDataMember]
		private IEnumerable<RName> allTiles;
		public IEnumerable<RName> GetAllTiles()
		{
			if (allTiles == null) allTiles = CreateAllTiles().ToReadOnlyColl();
			return allTiles;
		}

		private IEnumerable<RName> CreateAllTiles()
		{
			return
				from c in Enumerable.Range(0, Cols)
				from r in Enumerable.Range(0, Rows)
				select TilePos.Create(c, r).ToTileName();
		}
	}
}
