﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using JpLabs.Extensions;
using System;

namespace Rook
{
	public static class TileSetExtensions
	{
		[Obsolete("As it turns out, there's no need for _off_ to be on the graph.", true)]
		public static IGraphTileSet MergeWithOffBoardTile(this IGraphTileSet set)
		{
			return new MergedTileSet(new BasicTileSet(Tiles.OffBoard.ToEnumerable()), set);
		}
	}

	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class MergedTileSet : IGraphTileSet
	{
		[DataMember]
		private ICollection<IGraphTileSet> sets;

		public MergedTileSet(params IGraphTileSet[] sets) : this(sets.AsEnumerable())
		{}

		[Ninject.Inject]
		public MergedTileSet(IEnumerable<IGraphTileSet> sets)
		{
			this.sets = sets.ToReadOnlyColl();
		}

		public bool Contains(RName tile)
		{
			return sets.Any(s => s.Contains(tile));
		}

		public IEnumerable<RName> GetAllTiles()
		{
			return sets.SelectMany(s => s.GetAllTiles());
		}
	}
}
