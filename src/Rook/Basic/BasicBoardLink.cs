﻿
namespace Rook
{
	public class BasicBoardLink : IGraphLink
	{
		public string Label { get; private set; }
		public RName Origin { get; private set; }
		public RName Target { get; private set; }

		public BasicBoardLink(string name, RName origin, RName target)
		{
			this.Label = name;
			this.Origin = origin;
			this.Target = target;
		}
	}
}
