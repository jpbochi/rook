﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace Rook
{
	public class MoveExecutionContext : IMoveExecutionContext
	{
		public IGameState State { get; private set; }
		public IGameReferee Referee { get; private set; }
		public IPrincipal User { get; private set; }

		public MoveExecutionContext(IGameState state, IGameReferee referee, IPrincipal user)
		{
			State = state;
			Referee = referee;
			User = user;
		}

		public bool ExecuteMove(Uri moveUri)
		{
			return Referee.ExecuteMove(moveUri, this);
		}
	}

	public static class MoveExecutionContextExt
	{
		public static IMoveExecutionContext AsReferee(this IMoveExecutionContext context)
		{
			return new MoveExecutionContext(
				context.State,
				context.Referee,
				new GenericPrincipal(new GenericIdentity("referee"), new string[] { RookRoles.Referee })
			);
		}
	}
}
