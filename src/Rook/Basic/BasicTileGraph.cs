﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using JpLabs.Extensions;

namespace Rook
{
	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class BasicTileGraph : IGameTileGraph
	{
		[DataMember]
		public IGraphTileSet TileSet { get; private set; }

		[DataMember]
		public IGraphLinkSet LinkSet { get; private set; }

		public BasicTileGraph(IGraphTileSet tileSet, IGraphLinkSet linkSet)
		{
			this.TileSet = tileSet;
			this.LinkSet = linkSet;
		}

		public bool Contains(RName tile)
		{
			return TileSet.Contains(tile);
		}

		public IEnumerable<IGraphLink> GetOutLinks(RName tile)
		{
			if (LinkSet == null) return Enumerable.Empty<IGraphLink>();
			return  LinkSet.GetOutLinks(tile).Where(link => Contains(link.Target));
		}

		public IEnumerable<RName> Region(RName region)
		{
			if (region == Tiles.MainBoard) return TileSet.GetAllTiles();
			if (Contains(region)) return region.ToEnumerable();
			return Enumerable.Empty<RName>();
		}

		public IEnumerable<RName> GetAllTiles()
		{
			return TileSet.GetAllTiles();
		}
	}
}
