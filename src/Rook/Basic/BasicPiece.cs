﻿using System.Runtime.Serialization;
using Rook.Augmentation;

namespace Rook
{
	[DataContract(Namespace=BasicPiece.Namespace)]
	public class BasicPiece : AugObject, IGamePiece
	{
		[IgnoreDataMember]UniqueIdGenerator pieceIdGenerator;

		[IgnoreDataMember]IGamePiece basePiece;

		[DataMember(EmitDefaultValue=false)]public RName Position { get; private set; }

		[DataMember]public RName Id { get; private set; }
		[DataMember(EmitDefaultValue=false)]public RName BasePieceId { get; private set; }
		[DataMember(EmitDefaultValue=false)]public RName Class { get; private set; }

		protected override AugObject AugParent { get { return (basePiece == null) ? null : (AugObject)basePiece; } }

		public BasicPiece() {}

		public BasicPiece(RName pieceClass)
		{
			Class = pieceClass;
			Id = pieceClass;
		}

		public BasicPiece(IGamePiece basePiece, RName id)
		{
			this.basePiece = basePiece;
			BasePieceId = basePiece.Id;
			Class = basePiece.Class;
			Id = id;
		}

		void IGamePiece.ResolveReferencesAt(IPieceContainer container)
		{
			basePiece = container.ById(BasePieceId);
		}

		void IGamePiece.SetPosition(RName position)
		{
			Position = position;
		}

		public override System.Xml.Linq.XName DefaultXName
		{
			get { return System.Xml.Linq.XName.Get(this.GetType().Name, Namespace); }
		}

		private int GetNextId()
		{
			if (pieceIdGenerator == null) pieceIdGenerator = new UniqueIdGenerator();
			return pieceIdGenerator.Next();
		}
		
		public IGamePiece New()
		{
			var id = RName.Get(string.Format("{0}-{1:x}", Class.Name, GetNextId()), Class.Namespace);
			return new BasicPiece(this, id);
		}
	}

	public static class PieceProperties
	{
		public static AugProperty BannerProperty = AugProperty.Create("Banner", typeof(RName), typeof(PieceProperties));

		public static RName GetBanner(this IGamePiece piece)
		{
			return piece.GetValue<RName>(BannerProperty);
		}

		public static IGamePiece SetBanner(this IGamePiece piece, RName banner)
		{
			piece.SetValue(BannerProperty, banner);
			return piece;
		}
	}
}
