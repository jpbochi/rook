﻿using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace Rook
{
	/// ### Complex Move Resolution Example:
	/// Expected command sequence for a Duellum Shot:
	/// Root: ShootMove {Player, PlayerPuppet, Direction} + implicit params { Weapon, Ammo, FiringAccuracy, Stance, etc. }
	///		- UpdatePuppet
	/// 		- ConsumeCosts
	///				- DecrementAmmo
	///				- SpendTime
	/// 	- Fire (resolves: AdjustedDirection, TimeCost)
	///			- EmitSignals for shot (sight, hearing, smell, touch, and taste)
	///			- FireProjectiles (complex simulation of projectile trajectory involved)
	///				- Loop of ProjectileEnteringTile / ProjectileLeavingTile
	///				- HitPiece, Explode, Incinerate, Teleport, DestroyPiece, CreateDebris, InjureCreature, SpillBlood, etc.
	///				- EmitSignals foreach outcome
	///				- What if the player kills all his own pieces? No problem, his only option will be PassTurn.
	///			- EmitTimeGoingBySignal (Time is a Signal that is universally Perceived by TimeSensors!!)
	///		- ConcludeMove
	///			- PropagateQueuedSignals
	///				- PerceiveSignals foreach Sensor in board
	///			- UpdateScore
	///			- CheckIfGameIsOver
	///	
	/// Think about this epiphany:
	///		- A Move IS a Command that is directly visible by players.
	///		- An URI could describe a Move or a Command.
	///		- Move-Referees and Command-Referees should work in the exactly same way.
	///		- Atomic Commands alter the State directly. They do not break down into other Commands. Como faz?
	///		- Composite Command (such as Moves) can recursively ask the Referee for smaller Commands until they are atomic.
	///		- Example:
	///			- ShootMove => SpendAmmo + SpendTimeUnits + FireProjectile + AssimilateSignals + MoveConclusion
	///			- FireProjectile => EmitSignals(...) + RunProjectileTrajectory
	///			- MoveConclusion => CheckEndGameConditions + CalculateScore
	///		- Go:
	///			- MoveConclusion => CalculateScore + RecordMoveIntoPublicHistory
	///			- PassTurn => ((lastMove == PassTurn) ? SetGameOver : CycleTurn) + MoveConclusion
	///			- GoMove => AddPiece + CaptureOpponentPieces + CycleTurn + MoveConclusion
	///			- GoMove.Preconditions => TargetTileIsEmpty && NoSuicide && NoKo
	///	.
	///	.
	///	http://www.codeproject.com/KB/cs/symbol.aspx
	///	http://www.codeproject.com/KB/dotnet/gointerfaces.aspx

	/// <summary>
	/// Represents the rule set of a game. It must be state-less.
	/// It's responsible for telling which are the valid moves for any provided game state.
	/// </summary>
	public interface IGameReferee
	{
		IEnumerable<IMoveLink> GetMoves(IGameState state); //GET /moves
		IGameMove FindMove(Uri moveUri, IGameState state); //GET /move/{*moveUri}
		bool ExecuteMove(Uri moveUri, IMoveExecutionContext context); //POST /move/{*moveUri}
	}

	public interface IMoveExecutionContext
	{
		IGameState State { get; }
		IGameReferee Referee { get; }
		IPrincipal User { get; }

		bool ExecuteMove(Uri moveUri);
	}

	/// <summary>
	/// Move:
	/// Represents an executable move. Its usually tied to a Command-Referee that provides command composition.
	/// It contains an description object that identifies the move to a player.
	/// Atomic Command:
	/// Represents a command to be executed in a IGameState
	/// Operation examples: MovePiece, AddPiece, UpdatePiece, PassTurn, Resign
	/// An operation can also be a sequence of smaller ops
	/// </summary>
	public interface IGameMove
	{
		IMoveLink Link { get; }
		string RequiredRole { get; }
		void Execute(IMoveExecutionContext context); //TODO: replace this by IEnumerable<Uri> Execute(state); AND hide referee from the move object
	}

	/// <summary>
	/// Encapsulates everything necessary to play a game (i.e. a State + a Referee).
	/// </summary>
	public interface IGameMachine
	{
		IGameState State { get; }

		IEnumerable<IResourceInfo> Resources { get; }
		IJavascriptRenderer JSRenderer { get; }

		IEnumerable<IMoveLink> GetMoveOptions();

		bool TryPlay(Uri move, IPrincipal user);
	}
}
