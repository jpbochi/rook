﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Linq;
using JpLabs.Extensions;

namespace Rook.Augmentation
{
	/// http://msdn2.microsoft.com/en-us/library/ms752914.aspx = Dependency Properties Overview
	/// http://msdn.microsoft.com/en-us/library/ms745795.aspx = Dependency Property Callbacks and Validation (and Coercion)
	/// http://msdn2.microsoft.com/en-us/library/ms753197.aspx = Property Value Inheritance
	/// http://msdn2.microsoft.com/en-us/library/ms749011.aspx = Attached Properties Overview
	/// http://msdn.microsoft.com/en-us/library/ms742806.aspx = Routed Events Overview
	
	//public struct DependentPropertyChangedEventArgs
	//{
	//    public object NewValue { get; }
	//    public object OldValue { get; }
	//    public DependentProperty Property { get; }
	//}
	//public delegate void PropertyChangedCallback(DependentObject d, DependentPropertyChangedEventArgs e);
	//public class PropMetadata
	//{
	//    public object DefaultValue { get; set; }
	//    //public PropertyChangedCallback PropertyChangedCallback { get; set; }
	//    public PropMetadata(object defaultValue)
	//    {
	//        DefaultValue = defaultValue;
	//    }
	//}
	
	[Flags]
	public enum AugPropertyFlags
	{
		None = 0,
		DoesNotInherit = 1
	}

	public class AugValueChangedEventArgs : EventArgs
	{
		public delegate void EventHandler(AugObject sender, AugValueChangedEventArgs evArgs);
		
		public object NewValue { get; private set; }
		public object OldValue { get; private set; }
		public AugProperty Property { get; private set; }
		
		public AugValueChangedEventArgs(AugProperty property, object oldValue, object newValue)
		{
			this.Property = property;
			this.OldValue = oldValue;
			this.NewValue = newValue;
		}
	}

	public class CancellableAugValueChangedEventArgs : AugValueChangedEventArgs //seealso: System.ComponentModel.CancelEventArgs
	{
		public bool Cancel { get; set; }
		
		public CancellableAugValueChangedEventArgs(AugProperty property, object oldValue, object newValue)
			: base(property, oldValue, newValue)
		{}
	}
	
	public sealed class AugProperty : IEquatable<AugProperty>
	{
		static public readonly object UnsetValue = new object();

		public const string MandatoryFieldSuffix = "Property";
		
		private object DefaultValue;// { get; private set; }
		//private IAugPropertyBehavior defaultBehavior;
		//private IList<KeyValuePair<Type,IAugPropertyBehavior>> behaviorList; //TODO: use this
		
		private ITypeConverter propertyTypeConverter;
		
		public string Name { get; private set; }
		public AugPropertyFlags Flags { get; private set; }

		public string DeclaringTypeName { get; private set; }
		public Type PropertyType { get; private set; }
		
		//public bool ReadOnly { get; private set; }
		//public Func<object,bool> ValidateValueCallback { get; private set; }
		//public Func<object,bool> OnSetCoerceValueCallback { get; private set; }
		//public Func<object,bool> OnGetCoerceValueCallback { get; private set; }
		
		public bool InheritsValue { get { return (Flags & AugPropertyFlags.DoesNotInherit) == 0; } }
		
		[IgnoreDataMember]
		private string toString;
		[IgnoreDataMember]
		private int hashCode;

		private AugProperty(string name, Type propType, Type declaringType, AugPropertyFlags flags, object defaultValue)//, IAugPropertyBehavior defaultBehavior)
		{
			Name = name;
			PropertyType = propType;
			DeclaringTypeName = (declaringType == null) ? null : declaringType.FullName;
			DefaultValue = defaultValue;//this.defaultBehavior = defaultBehavior;
			Flags = flags;
			
			propertyTypeConverter = AugTypeConverter.Get(PropertyType);
			toString = string.Concat(DeclaringTypeName, "!", Name);
			hashCode = toString.GetHashCode();
		}

		static public AugProperty Create(string name, Type propType, Type declaringType)
		{
		    return Create(name, propType, declaringType, AugPropertyFlags.None, propType.GetDefaultValue());
		}

		static public AugProperty Create(string name, Type propType, Type declaringType, object defaultValue)
		{
			return Create(name, propType, declaringType, AugPropertyFlags.None, defaultValue);
		}

		static public AugProperty Create(string name, Type propType, Type declaringType, AugPropertyFlags flags, object defaultValue)
		{
			if (!AugTypeConverter.Get(propType).CanConvert(defaultValue)) throw new ArgumentException(
				string.Format(
					"defaultValue '{0}' is not convertible to '{1}'",
					defaultValue ?? "{null}",
					propType.FullName
				)
			);

			return new AugProperty(name, propType, declaringType, AugPropertyFlags.None, defaultValue);
		}

		/*
		static public AugProperty Create(string name, Type propType, Type declaringType, object defaultValue)
		{
			IAugPropertyBehavior behavior = BehaviorFromDefaultValue(defaultValue, propType);
			return Create(name, propType, declaringType, AugPropertyFlags.None, behavior);
		}
		static public AugProperty Create(string name, Type propType, Type declaringType, AugPropertyFlags flags, IAugPropertyBehavior defaultBehavior)
		{
			if (defaultBehavior == null) defaultBehavior = new SimpleAugPropBehavior(propType.GetDefaultValue());
			return new AugProperty(name, propType, declaringType, AugPropertyFlags.None, defaultBehavior);
		}
		static private IAugPropertyBehavior BehaviorFromDefaultValue(object defaultValue, Type propType)
		{
			if (defaultValue is IAugPropertyBehavior) return (IAugPropertyBehavior)defaultValue;
			if (!(defaultValue is IAugValueFactory)
			&&  !(defaultValue is IAugValue)
			&&  !propType.IsAssignableFromValue(defaultValue)) throw Error.ArgumentOfIncorrectType(propType, defaultValue, "defaultValue");
			return new SimpleAugPropBehavior(defaultValue);
		}
		public IAugPropertyBehavior GetDefaultBehavior()
		{
			return this.defaultBehavior;
		}
		public IAugPropertyBehavior GetBehavior(Type forType)
		{
			if (behaviorList != null) {
				//return first that is bigger or equal to requested type
				foreach (var kvPair in behaviorList) {
					if (kvPair.Key.IsBiggerOrEqual(forType)) return kvPair.Value;
				}
			}
			//or use default
			return this.defaultBehavior;
		}
		public void OverrideBehavior(Type forType, IAugPropertyBehavior typeBehavior)
		{
		    //TODO: implement this: some of the augProperty behavior may be overridden for specific types
		    throw new NotImplementedException();
		}
		//*/

		public object GetDefaultValue(Type forType)
		{
			//return ConvertValue(this.GetBehavior(forType).GetDefaultValue());
			return ConvertValue(DefaultValue);
		}

		//public bool IsValidValue(object value)
		//{
		//    //TODO: Test value type
		//    //TODO: Call ValidateValue callback, if any
		//    throw new NotImplementedException();
		//}
		//public object CoerceValue(object obj, object value)
		//{
		//    throw new NotImplementedException();
		//}
		
		public override bool Equals(object obj)
		{
			if (obj is AugProperty) return Equals((AugProperty)obj);
			return false;
		}

		public bool Equals(AugProperty other)
		{
			return Name == other.Name && DeclaringTypeName == other.DeclaringTypeName;
		}

		public override int GetHashCode()
		{
			return hashCode;
		}

		public override string ToString()
		{
			return toString;
		}

		public object ConvertValue(object value)
		{
			//if (value is IAugValue) return value;

			return propertyTypeConverter.Convert(value);
		}

		public XName ToXName()
		{
			//http://www.google.com/codesearch/p?hl=en#qS7vkPdvHXE/mcs/class/System.Xml.Linq/System.Xml.Linq/XName.cs

			return XName.Get(Name, DeclaringTypeName); //~= string.Concat("{", DeclaringTypeName, "}", Name);
		}

		public RName ToRName()
		{
			return RName.Get(Name, DeclaringTypeName);
		}

		public XObject ToXml(object value)
		{
			return XUtil.ToXml(ToXName(), value);
		}

		private static IEnumerable<FieldInfo> GetPropertyFields(Type declaringType, bool includeInherited)
		{
			var bindingFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

			if (includeInherited) bindingFlags |= BindingFlags.FlattenHierarchy;

			return declaringType.GetFields(bindingFlags).Where(
				fi => typeof(AugProperty).IsAssignableFrom(fi.FieldType)
			);
		}

		public static AugProperty FromXName(XName name)
		{
			return FromName(name.LocalName + MandatoryFieldSuffix, name.NamespaceName);
		}

		public static AugProperty FromRName(RName name)
		{
			return FromName(name.Name + MandatoryFieldSuffix, name.Namespace);
		}

		public static AugProperty FromName(string propertyName, string declaringTypeName)
		{
			var declaringType = TypeExt.ResolveTypeFromFullName(declaringTypeName, true);

			var prop = GetPropertyFields(declaringType, false)
				.Where(fi => fi.Name == propertyName)
				.Select(fi => (AugProperty)fi.GetValue(null))
				.FirstOrDefault();

			if (prop == null) throw new KeyNotFoundException(string.Format("AugProperty static field for '{0}' can't be found at class '{1}'", propertyName, declaringTypeName));

			return prop;
		}

		public static object ParseValue(XObject x)
		{
			throw new NotImplementedException();
		}
	}
}
