﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace Rook.Augmentation
{
	/// <summary>
	/// base class for objects that can have Augmented Properties
	/// </summary>
	[DataContract(Namespace=AugObject.Namespace)] //[KnownType(typeof(ConstantAugValue))]
	[System.Diagnostics.DebuggerDisplay("{ToXml()}")]
	public abstract class AugObject : IAugObject, IXSerializable//, IXmlSerializable //, INotifyPropertyChanged
	{
		protected const string Namespace = "http://jplabs.bochi.it/augmentation";

		protected virtual AugObject AugParent { get { return null; } }
		
		[IgnoreDataMember]//[DataMember(EmitDefaultValue=false)]
		private IDictionary<AugProperty,IAugValue> AugValues; //dictionary of values is lazily created

		public AugObject() {}

		public AugObject(XElement x)
		{
			ParseXml(x);
		}
		
		public IEnumerable<KeyValuePair<AugProperty,IAugValue>> GetLocalValues()
		{
			var propValues = this.AugValues;
			return (
					(propValues != null)
					? propValues.ToArray()
					: Enumerable.Empty<KeyValuePair<AugProperty,IAugValue>>()
				);
		}

		//[DataMember(Name="AugValues", EmitDefaultValue=false)]
		//[BsonIgnoreIfNull,BsonElement("AugValues")]
		//private XElement SerializableAugValues
		//{
		//	get {
		//		var v = this.ToXml(XName.Get("v", Namespace));
		//		return (v.HasAttributes || v.HasElements) ? v : null;
		//	}
		//	set { ParseXml(value); }
		//}

		//[BsonExtraElements]
		[DataMember,BsonIgnoreIfNull]
		public BsonDocument Aug
		{
			get { return GetBsonExtra(); }
			set { ParseBsonExtra(value); }
		}

		////DynamicObject members
		//public override bool TryGetMember(GetMemberBinder binder, out object result)
		//{
		//    this.values.TryGetValue(binder.Name, out result);
		//    return true;
		//}
		//public override bool TrySetMember(SetMemberBinder binder, object value)
		//{
		//    this.values[binder.Name] = value;
		//    return true;
		//}
		
		public object GetValue(AugProperty ap)
		{
			IAugValue augValue;
			if (TryGetLocalAugValue(ap, out augValue)) return augValue.GetValue(this);

			var value = GetBaseValue(ap);
			if (value != AugProperty.UnsetValue) return value;

			//TODO: implement Behaviors (they can override the default value according to the AugObject's type)
			//var behav = ap.GetBehavior(this.GetType());
			//var defaultValue = behav.GetDefaultValue();
			//value = defaultValue;

			var defaultValue = ap.GetDefaultValue(this.GetType());

			var valueAsAug = defaultValue as IAugValue;
			if (valueAsAug != null) defaultValue = valueAsAug.GetValue(this);

			return ap.ConvertValue(defaultValue);
		}

		public object GetBaseValue(AugProperty ap)
		{
			if (!ap.InheritsValue || AugParent == null) return AugProperty.UnsetValue;

			return AugParent.GetInheritedValue(ap, this);
		}

		//public void SetValue(AugProperty ap, IAugValue value)
		//{
		//    SetLocalAugValue(ap, value);
		//}
		
		public void SetValue(AugProperty ap, object value)
		{
			//TODO: [or not] validate value (using validate callback, if any) (might be useful for enum properties)
			
			//TODO: [or not] Coerce value (using coerce callback, if any) (what about a coerce event?)
			// Coercing should keep original value. It works like a FunctionAugValue
			
			value = ap.ConvertValue(value);
			
			SetLocalAugValue(ap, ConstantAugValue.Create(value));
		}
		
		//public void CombineValue(AugProperty ap, Delegate func)
		//{
		//    SetLocalAugValue(ap, FunctionAugValue.Create(func, ap));
		//}
		//public void CombineValue(AugProperty ap, LambdaExpression expr)
		//{
		//    SetLocalAugValue(ap, FunctionAugValue.Create(expr, ap));
		//}

		public bool IsValueUnset(AugProperty ap)
		{
			return GetInheritedValue(ap, this) == AugProperty.UnsetValue;
		}

		public void ClearValue(AugProperty ap)
		{
			InternalClearAugValue(ap);
		}
		
		protected virtual IDictionary<AugProperty,IAugValue> CreateAugValueDictionary()
		{
			return new Dictionary<AugProperty,IAugValue>();
		}
		
		private bool TryGetLocalAugValue(AugProperty ap, out IAugValue outValue)
		{
			outValue = null;
			var values = AugValues;
			if (values == null) return false;
			
			return values.TryGetValue(ap, out outValue);
		}

		private void SetLocalAugValue(AugProperty ap, IAugValue value)
		{
			var values = AugValues;
			if (values == null) values = CreateAugValueDictionary();
			AugValues = values;
			
			//object oldValue = this.GetValue(ap, false);
			
			if (IsDefaultValue(ap, value)) {
				//Remove augvalue because it's the same as default
				values.Remove(ap);
			} else {
				//Set (overwriting) the augvalue for the property
				values[ap] = value;
			}
			
			//AugValueChangedEventArgs valueChangedEvArgs = new AugValueChangedEventArgs(ap, oldValue, value.GetValue(this));
			//var behav = ap.GetBehavior(this.GetType());
			//behav.OnValueChanged(this, valueChangedEvArgs);
		}
		
		private bool IsDefaultValue(AugProperty ap, IAugValue value)
		{
			if (!(value is ConstantAugValue)) return false;
			
			//var behav = ap.GetBehavior(this.GetType());
			var defaultValue = ap.GetDefaultValue(this.GetType());//behav.GetDefaultValue();
			
			return ((ConstantAugValue)value).Value == defaultValue;
		}

		private void InternalClearAugValue(AugProperty ap)
		{
			var values = AugValues;
			if (values != null) values.Remove(ap);
		}

		private object GetInheritedValue(AugProperty ap, AugObject originalOwner)
		{
			IAugValue value;
			if (TryGetLocalAugValue(ap, out value)) return value.GetValue(originalOwner);

			return GetBaseValue(ap);
		}

		private BsonDocument GetBsonExtra()
		{
			var values = this.GetLocalValues().ToDictionary(
				kvp => kvp.Key.ToXName().ToString(),
				kvp => kvp.Value.GetValue(this).ToJson(kvp.Key.PropertyType)
			);
			return (values.Any()) ? new BsonDocument(values) : null;
		}

		private void ParseBsonExtra(BsonDocument bson)
		{
			if (bson == null) return;
			
			foreach (var elem in bson.Elements) {
				var prop = AugProperty.FromXName(XName.Get(elem.Name));

				var rawValue =  elem.Value.AsString;
				var djsonValue = BsonSerializer.Deserialize(rawValue, prop.PropertyType);

				var value = AugValue.Create(prop.ConvertValue(djsonValue), prop);

				SetLocalAugValue(prop, value);
			}
		}

		XObject IXSerializable.ToXml(XName name)
		{
		    return ToXml(name);
		}

		public virtual XElement ToXml()
		{
		    return ToXml(DefaultXName);
		}

		public virtual XName DefaultXName
		{
			get {
				var type = this.GetType();
				return XName.Get(type.Name, type.Namespace);
			}
		}

		protected virtual XElement ToXml(XName name)
		{
			var xElem = new XElement(name);
			
			xElem.Add(this.GetLocalValues().Select(pair => pair.ToXml()));

			if (xElem.Elements().Any()) throw new NotSupportedException();
			
			return xElem;
		}

		protected virtual void ParseXml(XElement x)
		{
			if (x == null) return;
			foreach (var attr in x.Attributes().Where(a => !a.IsNamespaceDeclaration)) {
				var kvp = AugValue.FromXml(attr);
				SetLocalAugValue(kvp.Key, kvp.Value);
			}

			/*foreach (var node in x.Nodes().OfType<XElement>()) {
				var kvp = AugValue.FromXml(node);
				SetLocalAugValue(kvp.Key, kvp.Value);
			}//*/
		}
	}
}
