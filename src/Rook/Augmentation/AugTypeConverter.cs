﻿using System;
using System.ComponentModel;

namespace Rook.Augmentation
{
	public interface ITypeConverter
	{
		object Convert(object value);

		bool TryConvert(object value, out object converted);
	}

	public static class TypeConverterExt
	{
		public static bool CanConvert(this ITypeConverter converter, object value)
		{
			object converted;
			return converter.TryConvert(value, out converted);
		}
	}
	
	public static class AugTypeConverter<TTargetType>
	{
		public static ITypeConverter Converter = AugTypeConverter.Get(typeof(TTargetType));
	}

	public sealed class AugTypeConverter : ITypeConverter
	{
		//TODO: research TypeConverterAttribute and ValueSerializerAttribute

		private readonly Type targetType;
		private readonly bool isTargetTypeNullable;
		//private readonly bool isTargetTypeConvertible;
		
		private AugTypeConverter(Type targetType, bool isTargetTypeNullable)
		{
			this.targetType = targetType;
			this.isTargetTypeNullable = isTargetTypeNullable;
			
			//this.isTargetTypeConvertible = typeof(IConvertible).IsAssignableFrom(targetType);
		}

		public static ITypeConverter Get(Type type)
		{
			if (type == null) throw new ArgumentNullException("type");
			
			var targetType = type;
			bool isTargetTypeNullable;
			
			//if (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>)) {
			//    isTargetTypeNullable = true;
			//    targetType = targetType.GetGenericArguments()[0];
			//} else {
				isTargetTypeNullable = !targetType.IsValueType;
			//}
			

			return new AugTypeConverter(targetType, isTargetTypeNullable);
		}

		public bool TryConvert(object value, out object converted)
		{
			converted = null;
			try {
				converted = Convert(value);
				return true;
			}
			catch (InvalidCastException) { return false; }
			catch (FormatException) { return false; }
			catch (OverflowException) { return false; }
			catch (NotSupportedException) { return false; }
		}
		
		public object Convert(object value)
		{
			if (isTargetTypeNullable && value == null) return null;

			////Try light conversion
			//if (isTargetTypeConvertible) return System.Convert.ChangeType(value, targetType, System.Globalization.CultureInfo.InvariantCulture);
			////Try equivalent to: return value as Type;
			//if (value != null && targetType.IsAssignableFrom(value.GetType())) return value;

			if (value != null && targetType.IsAssignableFrom(value.GetType())) return value;

			var converter = TypeDescriptor.GetConverter(targetType);//new System.ComponentModel.TypeConverter();
			//if (converter != null) {
				
				//if (value is string) return converter.ConvertFromInvariantString((string)value);

				return converter.ConvertFrom(value);
			//}
			
			//return null;
		}
	}
}
