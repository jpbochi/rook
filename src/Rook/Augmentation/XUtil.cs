﻿using System;
using System.ComponentModel;
using System.Xml;
using System.Xml.Linq;

namespace Rook.Augmentation
{
	public static class XUtil
	{
		public static string ToString(object o)
		{
			if (o == null) throw new InvalidOperationException ("Attempt to get string from null");

			switch (Type.GetTypeCode (o.GetType ())) {
				case TypeCode.String:	return (string)o;
				case TypeCode.DateTime:	return XmlConvert.ToString((DateTime) o, XmlDateTimeSerializationMode.RoundtripKind);
				case TypeCode.Double:	return ((double)o).ToString("r");
				case TypeCode.Single:	return ((float)o).ToString("r");
				case TypeCode.Boolean:
					// Valid XML values are `true' and `false', not `True' and `False' that boolean returns
					return o.ToString().ToLower();
				default:
					if (o is TimeSpan)		return XmlConvert.ToString((TimeSpan) o);
					if (o is DateTimeOffset)return XmlConvert.ToString((DateTimeOffset) o);
					return o.ToString();
			}
		}

		public static XObject ToXml(XName name, object value)
		{
			if (value is IXSerializable) return ((IXSerializable)value).ToXml(name);

			//TODO: what about ISerializable values?
			// And System.Xml.Serialization.IXmlSerializable?
			// And IConvertible?

			//if (value is IConvertible) return new XAttribute(name, value);
			#if DEBUG
			if (value != null) {
				if (TypeDescriptor.GetConverter(value.GetType()).GetType() == typeof(TypeConverter)) {
					throw new NotSupportedException(string.Format("Serialization of {0} is not supported", value.GetType()));
				}
			}
			#endif

			return new XAttribute(name, value);
		}
	}
}
