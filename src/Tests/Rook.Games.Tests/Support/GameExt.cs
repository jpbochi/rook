﻿using System.Linq;
using Ninject;

namespace Rook.Games.Tests
{
	internal static class GameExt
	{
		public static void AssertScoreIsIndeterminate(this IGameState state)
		{
			state.Players.ShouldEach(p => p.Score.ShouldBe(GameScore.StillPlaying));
		}

		public static void AssertDraw(this IGameState state)
		{
			var count = state.Players.Count();
			state.Players.Select(p => p.Score).ShouldBeEqualInOrder(Enumerable.Repeat(GameScore.Drawn, count));
		}

		public static void AssertWinner(this IGameState state, RName expectedWinnerId)
		{
			foreach (var p in state.Players) {
				p.Score.ShouldBe( (p.Id == expectedWinnerId) ? GameScore.Won : GameScore.Lost );
			}
		}

		public static void PlayMove(this IGameMachine game, RName origin, RName destination)
		{
			var move = game.GetMoveOptions().Where( m => m.GetOrigin() == origin && m.GetDestination() == destination ).First();
			game.TryPlay(move.Href, TestUser.ForCurrent(game.State)).ShouldBeTrue("Failed to play move.");
		}

		public static void PlayDropAt(this IGameMachine game, RName position)
		{
			var move = game.GetMoveOptions().Where( m => m.GetOrigin() == null && m.GetDestination() == position ).First();
			game.TryPlay(move.Href, TestUser.ForCurrent(game.State)).ShouldBeTrue("Failed to play drop move.");
		}

		public static IGameState GetNewState(this IKernel kernel, IGameBoard board)
		{
			kernel.Rebind<IGameBoard>().ToConstant(board);
		    return kernel.Get<IGameState>();
		}

		public static IGameMachine GetNewGameMachine(this IKernel kernel)
		{
		    return kernel.Get<IGameMachine>();
		}

		public static IGameMachine GetNewGameMachine(this IKernel kernel, IGameBoard board)
		{
			kernel.Rebind<IGameBoard>().ToConstant(board);
		    return kernel.Get<IGameMachine>();
		}
	}
}
