﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using JpLabs.Extensions;

namespace Rook.Games.Tests
{
	public static class TestUser
	{
		public static IPrincipal AsReferee
		{
			get { return For(RookRoles.Referee); }
		}

		public static IPrincipal ForCurrent(IGameState state)
		{
			return For(state.CurrentPlayerId);
		}

		public static IPrincipal For(params IGamePlayer[] players)
		{
			return new GenericPrincipal(new GenericIdentity("test"), players.Select(p => p.Id.ToString()).ToArray());
		}

		public static IPrincipal For(params string[] roles)
		{
			return new GenericPrincipal(new GenericIdentity("test"), roles);
		}
	}
}
