﻿using System.Linq;
using JpLabs.Symbols;
using Ninject;

namespace Rook.Games.Tests
{
	internal static class TestExt
	{
		public static string ClearLiteral(this string literal)
		{
			return literal
				.Replace(" ", "")
				.Replace("\t", "")
				.Replace("\r", "")
				.TrimEnd('\n');
		}

		public static Ninject.Syntax.IBindingToSyntax<T> RebindNamed<T>(this IKernel kernel, Symbol name)
		{
			return kernel.RebindNamed<T>(name.FullName);
		}

		public static Ninject.Syntax.IBindingToSyntax<T> RebindNamed<T>(this IKernel kernel, string name)
		{
			var bindingToRemove = kernel.GetBindings(typeof(T)).Where( b => b.Metadata.Name == name ).FirstOrDefault();
			if (bindingToRemove != null) kernel.RemoveBinding(bindingToRemove);

			var builder = kernel.Bind<T>();
			string.Intern(name);
			builder.BindingConfiguration.Metadata.Name = name;

			return builder;
		}
	}
}
