﻿using System.IO;
using System.Linq;
using System.Reflection;
using Xunit;

namespace Rook.Games.Tests
{
	public abstract class BaseSerializationTests
	{
		protected abstract string GetSerializedInitialGameInProd();

		protected abstract IGameState GetNewInitialState();
		protected abstract IGameState GetNewChangedState();
		protected abstract IGameMachine GetNewGameMachine();

		protected abstract void VerifyInitialState(IGameState state);
		protected abstract void VerifyAnyState(IGameState state);

		protected static string ReadTextResource(string resourceName, Assembly asm)
		{
			using (var reader = new StreamReader(asm.GetManifestResourceStream(resourceName))) {
				return reader.ReadToEnd();
			}
		}

		[Fact]
		protected virtual void should_serialize_initial_state()
		{
			var state = GetNewInitialState();

			GameSerializer.Serialize(state);
		}

		[Fact]
		protected virtual void should_serialize_and_deserialize_initial_state()
		{
			var state = GetNewInitialState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
			CopyToClipBoardAndBreak(serialized);
			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);

			VerifyInitialState(deserialized);
		}

		[System.Diagnostics.Conditional("DEBUG")]
		static void CopyToClipBoardAndBreak(string value)
		{
			if (System.Diagnostics.Debugger.IsAttached) {
				System.Windows.Clipboard.SetText(value);
				System.Diagnostics.Debugger.Break();
			}
		}

		[Fact]
		protected virtual void should_serialize_and_deserialize_changed_state()
		{
			var state = GetNewChangedState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);

			VerifyAnyState(deserialized);
		}

		[Fact]
		protected virtual void should_serialize_complex_state_twice_to_the_same_value()
		{
			var state = GetNewChangedState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);
			var reserialized = GameSerializer.Serialize<IGameState>(deserialized);

			Assert.Equal(serialized, reserialized);
		}

		[Fact]
		protected virtual void should_deserialize_saved_initial_game()
		{
			var serializedGameInProd = GetSerializedInitialGameInProd();

			var deserialized = GameSerializer.Deserialize<IGameState>(serializedGameInProd);

			VerifyInitialState(deserialized);
		}
	}
}
