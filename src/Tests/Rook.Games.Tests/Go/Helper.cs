﻿using System;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using Ninject;
using Rook.Games.Go;

namespace Rook.Games.Tests.Go
{
	internal class Helper
	{
		private readonly Lazy<IKernel> lazyKernel = new Lazy<IKernel>( StaticGameFactory.GetKernel<GoGameModule> );

		public IKernel Kernel 
		{
			get { return lazyKernel.Value; }
		}

		public IGameBoard GetBoard(ushort cols, ushort rows)
		{
			var kernel = Kernel;
			kernel.RebindNamed<PointInt>(GoGameModule.BoardSizeOptionName.FullName).ToConstant(new PointInt(cols, rows));
			return kernel.Get<IGameBoard>();
		}

	    protected internal IGameBoard ParseBoard(string strBoard)
		{
			var lines = strBoard.Split(new []{'\n'}, StringSplitOptions.RemoveEmptyEntries);
			int rows = lines.Length;
			int cols = lines[0].Length;

			var board = GetBoard((ushort)cols, (ushort)rows);

			var blackClass = board.Pieces.ById(GoPieces.BlackStone);
			var whiteClass = board.Pieces.ById(GoPieces.WhiteStone);

			for (int r=0; r<rows; r++) {
				for (int c=0; c<cols; c++) {
					if (lines[r][c] == 'x') board.AddPiece(blackClass.New(), TilePos.Create(c, r).ToTileName());
					else
					if (lines[r][c] == 'o') board.AddPiece(whiteClass.New(), TilePos.Create(c, r).ToTileName());
				}
			}

			return board;
		}

		public static string DumpBoard(IGameBoard board)
		{
			var lines = board.Region(Tiles.MainBoard).Where(t => t != Tiles.OffBoard).GroupBy(t => t.ToPointInt().Value.Y).ToArray();

			var builder = new StringBuilder();
			foreach (var line in lines) {
				builder.Append('\n');

				foreach (var tile in line) {
					var piece = board.PiecesAt(tile).FirstOrDefault();

					var ch = (piece == null) ? '.'
						   : (piece.GetBanner() == ClassicPlayer.Black) ? 'x'
						   : (piece.GetBanner() == ClassicPlayer.White) ? 'o'
						   : '?';
					
					builder.Append(ch);
				}
			}
			
			return builder.ToString();
		}
	}
}
