﻿using System;
using Rook.Games.Go;
using Xunit;

namespace Rook.Games.Tests.Go
{
	public class PassTurnMoveTests
	{
		[Fact]
		void should_CycleTurn_and_UpdateScore_when_last_move_was_not_a_PassTurn()
		{
			var state = new BasicGameState(null, null);
			var recorder = new UriCommandRecorder();

			var move = new PassTurnMove("player1");
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri(string.Concat("/set/Rook.Games.Go.GoProperties!PreviousMove/", move.Link.Href), UriKind.Relative),
				new Uri("/reset/Rook.Games.Go.GoProperties!PreviousBoard", UriKind.Relative),
				new Uri("/cycle-turn", UriKind.Relative),
				new Uri("/go/score", UriKind.Relative),
			});
		}

		[Fact]
		void should_SetGameOver_and_UpdateScore_when_last_move_was_a_PassTurn()
		{
			var state = new BasicGameState(null, null);
			state.SetValue(GoProperties.PreviousMoveProperty, "/pass/player2");
			var recorder = new UriCommandRecorder();

			var move = new PassTurnMove("player1");
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri("/set-game-over", UriKind.Relative),
				new Uri("/go/score", UriKind.Relative),
			});
		}
	}
}
