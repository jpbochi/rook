﻿using System.Linq;
using Xunit;

namespace Rook.Games.Tests.Go
{
	public class MachineTests
	{
		private readonly Mother mother = new Mother();

		private static void PlayPassTurn(IGameMachine game)
		{
			var move = game.GetMoveOptions().Where(m => m.Rel == Rook.Games.Go.PassTurnMove.Relation).First();
			game.TryPlay(move.Href, TestUser.ForCurrent(game.State)).ShouldBeTrue("Failed to pass turn");
		}

		[Fact]
		void should_provide_resources()
		{
			var resources = mother.GetNewGameMachine().Resources.Select(r => r.ShouldBeOfType<WebResourceInfo>());;

			resources.Select(r => r.CssClass).ShouldContainSameAs(new [] { "board-backboard go", "piece-set chess" });
		}

		[Fact]
		void should_have_valid_initial_state()
		{
			var game = mother.GetNewGameMachine();

			GoGameModuleTests.VerifyInitialState(game.State);
			GoRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		[Fact]
		void should_play_valid_first_move()
		{
			mother.ParseAndBindBoard(@"
				...
				...
				..."
			.ClearLiteral());

			var game = mother.GetNewGameMachine();

			game.PlayDropAt("b2");

			string expectedBoard = @"
				...
				.x.
				..."
			.ClearLiteral();

			Assert.Equal(expectedBoard, Helper.DumpBoard(game.State.Board));
			Assert.Equal(ClassicPlayer.White, game.State.CurrentPlayerId);
			game.State.AssertScoreIsIndeterminate();
		}

		[Fact]
		void should_end_game_after_double_PassTurn()
		{
			var game = mother.GetNewGameMachine();

			PlayPassTurn(game);
			PlayPassTurn(game);

			game.State.AssertDraw();
			game.State.CurrentPlayerId.ShouldBeNull();
		}
	}
}
