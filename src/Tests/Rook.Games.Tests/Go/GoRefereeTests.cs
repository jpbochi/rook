﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using JpLabs.Extensions;
using Moq;
using Rook.Games.Common;
using Rook.Games.Go;
using Xunit;
using Xunit.Extensions;

namespace Rook.Games.Tests.Go
{
	public class GoRefereeTests
	{
		private readonly Mother mother = new Mother();

		private static IMoveLink FindDropMove(IGameState state, IGameReferee referee, RName destination)
		{
			return referee
				.GetMoves(state)
				.Where(m => m.Is<GoDropStoneMove>() && m.GetDestination() == destination)
				.FirstOrDefault()
			;
		}

		private static void PlayDrop(IGameState state, IGameReferee referee, RName destination)
		{
			var move = FindDropMove(state, referee, destination);

			referee.ExecuteMove(move.Href, new MoveExecutionContext(state, referee, TestUser.ForCurrent(state))).ShouldBeTrue("Failed to play drop");
		}
		
		public static IEnumerable<object[]> BoardsWithValidMoves
		{
			get { return Mother.GetBoardsForCaptureTests().Select( t => new object[] { t.Item1, t.Item2 } ); }
		}

		public static IEnumerable<object[]> BoardsForSuicideMoveTests
		{
			get { return Mother.GetBoardsForSuicideMoveTests().Select( t => new object[] { t.Item1, t.Item2 } ); }
		}

		public static IEnumerable<object[]> BoardsForKoMoveTests
		{
			get { return Mother.GetBoardsForKoMoveTests().Select( t => new object[] { t.Item1, t.Item2, t.Item3 } ); }
		}

		[Theory]
		[InlineData(9)]
		[InlineData(13)]
		[InlineData(19)]
		void benchmark_calculation_of_initial_move_options(int boardSize)
		{
			mother.BindBoard(mother.GetBoard((ushort)boardSize, (ushort)boardSize));

			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			const int totalMilisecondsToWait = 444;
			double averageMs = 1;

			{
				//TODO: find out why the first call is considerably slower than the others
				var debugWatch = Stopwatch.StartNew();
				referee.GetMoves(state).Last();
				debugWatch.Stop();
				Trace.WriteLine(string.Format("1st pass calculated in {0}ms", debugWatch.Elapsed.TotalMilliseconds));

				GC.Collect();
				GC.WaitForPendingFinalizers();
			}

			var testWatch = Stopwatch.StartNew();

			int testCycles = 0;
			while (testWatch.ElapsedMilliseconds < totalMilisecondsToWait) {
				testCycles++;
				var debugWatch = Stopwatch.StartNew();

				var moves = referee.GetMoves(state).ToArray();

				debugWatch.Stop();
				var time = debugWatch.Elapsed.TotalMilliseconds;
				averageMs *= time;
			}

			averageMs = Math.Pow(averageMs,  1.0 / testCycles);
			Trace.WriteLine(string.Format("Go moves calculated in {0:0.00}ms for a {1}x{1} board (geometric mean of {2} cycles)", averageMs, boardSize, testCycles));
		}

		[Fact]
		void should_provide_initial_move_options()
		{
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			var moves = referee.GetMoves(state).ToArray();
			Assert.NotNull(moves);

			VerifyInitialMoveOptions_Links(moves, state.Board);
		}

		public static void VerifyInitialMoveOptions_Links(IEnumerable<IMoveLink> actualMoves, IGameBoard board)
		{
			var expectedPlayerId = ClassicPlayer.Black;
			var expectedPieceClass = GoPieces.BlackStone;

			var expectedMoves
				= board
				.Region(Tiles.MainBoard)
				.Select(
					tile => MoveLink.From(
						GoDropStoneMove.Template,
						GoDropStoneMove.Relation,
						new { PlayerId = expectedPlayerId, Piece = expectedPieceClass, Position = tile }
					).Href
				).Concat(
					MoveLink.From(
						PassTurnMove.Template,
						PassTurnMove.Relation,
						new { PlayerId = expectedPlayerId }
					).Href
				);

			actualMoves.Where(m => !m.Is<ResignMove>()).Select(m => m.Href).ShouldContainSameAs(expectedMoves);
		}

		[Theory]
		[PropertyData("BoardsWithValidMoves")]
		void should_allow_drop_stone_move_which_gives_correct_captures(string boardSetup, string dropTile)
		{
			mother.ParseAndBindBoard(boardSetup);
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			var move = FindDropMove(state, referee, dropTile);

			move.Is<GoDropStoneMove>().ShouldBeTrue();
		}

		[Theory]
		[PropertyData("BoardsForSuicideMoveTests")]
		void should_not_allow_suicide_move(string boardSetup, string suicidePos)
		{
			mother.ParseAndBindBoard(boardSetup);
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			var suicideMoveQuery = referee
				.GetMoves(state)
				.Where(m => m.Is<GoDropStoneMove>() && m.GetDestination() == suicidePos)
			;
			
			suicideMoveQuery.ShouldBeEmpty();
		}

		[Theory]
		[PropertyData("BoardsForKoMoveTests")]
		void should_not_allow_Ko_move(string boardSetup, string firstMove, string koMove)
		{
			mother.ParseAndBindBoard(boardSetup);
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			PlayDrop(state, referee, firstMove);

			var koMoveQuery = referee
				.GetMoves(state)
				.Where(m => m.Is<GoDropStoneMove>() && m.GetDestination() == koMove)
			;

			koMoveQuery.ShouldBeEmpty();
		}

		[Fact]
		void should_accept_move_if_user_is_authorized()
		{
			var state = mother.GetNewState();
			var referee = new GoReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == GoDropStoneMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(true).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeTrue("Failed to play move");
			userMock.Verify();
		}

		[Fact]
		void should_reject_move_if_user_is_not_authorized()
		{
			var state = mother.GetNewState();
			var referee = new GoReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == GoDropStoneMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(false).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeFalse("Referee accepted unauthorized move");
			userMock.Verify();
		}
	}
}
