﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Ninject.Modules;

namespace Rook.Games.Tests.Go
{
	internal class Mother : Helper
	{
		public TModule GetModule<TModule>() where TModule : INinjectModule
		{
			return Kernel.GetModules().OfType<TModule>().Single();
		}

		public IGameState GetNewState()
		{
			return Kernel.Get<IGameState>();
		}

		public IGameReferee GetNewReferee()
		{
		    return Kernel.GetReferee();
		}

		public IGameMachine GetNewGameMachine()
		{
		    return Kernel.Get<IGameMachine>();
		}

		public void BindState(IGameState state)
		{
			Kernel.Rebind<IGameState>().ToConstant(state);
		}

		public void BindBoard(IGameBoard board)
		{
			Kernel.Rebind<IGameBoard>().ToConstant(board);
		}

		public void ParseAndBindBoard(string boardSetup)
		{
			BindBoard(ParseBoard(boardSetup));
		}

		public static IEnumerable<string> GetBoards()
		{
			return GetBoardsWithExpectedScore().Select(t => t.Item1);
		}

		public static IEnumerable<Tuple<string,int,int, int, int>> GetBoardsWithExpectedScore()
		{
			yield return Tuple.Create(
				@"
					......
					..xo..
					.xo.o.
					..xo..
					......"
				.ClearLiteral(), 0, 1, 3, 5
			);
			yield return Tuple.Create(
				@"
					.x......
					x.x.....
					.x.x.ooo
					..x..o.o
					......o."
				.ClearLiteral(), 3, 2, 9, 8
			);
			yield return Tuple.Create(
				@"
					.x..o...
					.x.o..o.
					.x.o.o.o
					..x.o.o."
				.ClearLiteral(), 5, 10, 9, 18
			);
		}

		public static IEnumerable<Tuple<string,string,string[]>> GetBoardsForCaptureTests()
		{
			yield return Tuple.Create(@"
				. . x o .
				. x o . o
				. . x o .".ClearLiteral(), "d2", new [] {"c2"}
			);
			yield return Tuple.Create(@"
				o . o x .
				x o o x .
				. x x . .
				. . . . .".ClearLiteral(), "b1", new [] {"a1", "c1", "b2", "c2"}
			);
			yield return Tuple.Create(@"
				. . . x . .
				. . x o x .
				. x o . . .
				. . x . . .".ClearLiteral(), "d3", new [] {"d2", "c3"}
			);
		}

		public static IEnumerable<Tuple<string,string>> GetBoardsForSuicideMoveTests()
		{
			yield return Tuple.Create(@"
				. o . .
				o . o .
				. o . .".ClearLiteral(), "b2"
			);
			yield return Tuple.Create(@"
				x . x o .
				o x x o .
				. o o . .
				. . . . .".ClearLiteral(), "b1"
			);
		}

		public static IEnumerable<Tuple<string,string,string>> GetBoardsForKoMoveTests()
		{
			yield return Tuple.Create(@"
				.xo.
				xo.o
				.xo.".ClearLiteral(), "c2", "b2"
			);
			yield return Tuple.Create(@"
				....
				..xo
				.xo.".ClearLiteral(), "d3", "c3"
			);
		}
	}
}
