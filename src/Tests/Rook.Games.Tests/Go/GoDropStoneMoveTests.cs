﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rook.Games.Go;
using Xunit;
using Xunit.Extensions;
using JpLabs.Extensions;

namespace Rook.Games.Tests.Go
{
	public class GoDropStoneMoveTests
	{
		private readonly Mother mother = new Mother();

		public static IEnumerable<object[]> BoardsForCaptureTests
		{
			get { return Mother.GetBoardsForCaptureTests().Select( t => new object[] { t.Item1, t.Item2, t.Item3 } ); }
		}

		[Theory]
		[PropertyData("BoardsForCaptureTests")]
		void should_capture_pieces_add_one_piece_and_cycle_turn(string boardSetup, string tileToDropPiece, params string[] expectedCaptures)
		{
			mother.ParseAndBindBoard(boardSetup);
			var state = mother.GetNewState();
			var playerPieceClass = GoPieces.OfPlayer[state.CurrentPlayerId];

			var recorder = new UriCommandRecorder();
			var move = new GoDropStoneMove(state.CurrentPlayerId, playerPieceClass, tileToDropPiece);

			move.Execute(new MoveExecutionContext(state, recorder, null));

			var expectedCapturedPieces = expectedCaptures.Select(pos => state.Board.PiecesAt(pos).Single().Id).ToArray();

			var captures = recorder.Commands.TakeWhile(cmd => cmd.ToString().StartsWith("/capture/")).ToArray();
			captures.ShouldContainSameAs(
				expectedCapturedPieces.Select(p => new Uri(string.Format("/capture/{0}", p), UriKind.Relative))
			);
			recorder.Commands.Dequeue(captures.Length).Count();

			recorder.Commands.Dequeue().ShouldBe(new Uri(string.Format("/add/{0}/{1}", playerPieceClass, tileToDropPiece), UriKind.Relative));
			recorder.Commands.Dequeue().ShouldBe(new Uri(string.Concat("/set/Rook.Games.Go.GoProperties!PreviousMove/", move.Link.Href), UriKind.Relative));
			recorder.Commands.Dequeue().ShouldMatch("/set/Rook.Games.Go.GoProperties!PreviousBoard/{hash}");
			recorder.Commands.Dequeue().ShouldBe(new Uri("/cycle-turn", UriKind.Relative));
			recorder.Commands.Dequeue().ShouldBe(new Uri("/go/score", UriKind.Relative));

			recorder.Commands.ShouldBeEmpty();
		}
	}
}
