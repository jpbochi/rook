﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rook.Games.Go;
using Xunit;
using Xunit.Extensions;

namespace Rook.Games.Tests.Go
{
	public class GoScoreCommandTests
	{
		private readonly Mother mother = new Mother();

		public static IEnumerable<object[]> BoardsWithExpectedScore
		{
			get { return Mother.GetBoardsWithExpectedScore().Select( t => new object[] { t.Item1, t.Item2, t.Item3, t.Item4, t.Item5 } ); }
		}

		[Theory]
		[PropertyData("BoardsWithExpectedScore")]
		void should_calculate_territory_for_both_players(string boardDesc, int expectedBlackTerritory, int expectedWhiteTerritory, int expectedBlackArea, int expectedWhiteArea)
		{
			mother.ParseAndBindBoard(boardDesc);
			var state = mother.GetNewState();

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new GoScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.Black, expectedBlackTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.White, expectedWhiteTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.Black, expectedBlackArea), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.White, expectedWhiteArea), UriKind.Relative),
			});
		}

		[Fact]
		void should_set_draw_when_areas_are_the_same()
		{
			var board =	@"
				.x..oo
				x....o
				......".ClearLiteral();
			int expectedBlackTerritory = 1;
			int expectedWhiteTerritory = 0;
			int expectedBlackArea = 3;
			int expectedWhiteArea = 3;
			mother.ParseAndBindBoard(board);
			var state = mother.GetNewState();
			state.CurrentPlayerId = null;

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new GoScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.Black, expectedBlackTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.White, expectedWhiteTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.Black, expectedBlackArea), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.White, expectedWhiteArea), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ClassicPlayer.Black, GameScore.Drawn), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ClassicPlayer.White, GameScore.Drawn), UriKind.Relative),
			});
		}

		[Fact]
		void should_set_Black_as_winner_because_he_has_more_area()
		{
			var board =	@"
				.xx.oo
				x....o
				......".ClearLiteral();
			int expectedBlackTerritory = 1;
			int expectedWhiteTerritory = 0;
			int expectedBlackArea = 4;
			int expectedWhiteArea = 3;
			mother.ParseAndBindBoard(board);
			var state = mother.GetNewState();
			state.CurrentPlayerId = null;

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new GoScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.Black, expectedBlackTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.White, expectedWhiteTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.Black, expectedBlackArea), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.White, expectedWhiteArea), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ClassicPlayer.Black, GameScore.Won), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ClassicPlayer.White, GameScore.Lost), UriKind.Relative),
			});
		}

		[Fact]
		void should_set_White_as_winner_because_he_has_more_area()
		{
			var board =	@"
				.x..o.
				x...oo
				......".ClearLiteral();
			int expectedBlackTerritory = 1;
			int expectedWhiteTerritory = 1;
			int expectedBlackArea = 3;
			int expectedWhiteArea = 4;
			mother.ParseAndBindBoard(board);
			var state = mother.GetNewState();
			state.CurrentPlayerId = null;

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new GoScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.Black, expectedBlackTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Territory/{1}", ClassicPlayer.White, expectedWhiteTerritory), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.Black, expectedBlackArea), UriKind.Relative),
				new Uri(string.Format("/set-player/{0}/Rook.Games.Go.GoProperties!Area/{1}", ClassicPlayer.White, expectedWhiteArea), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ClassicPlayer.Black, GameScore.Lost), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ClassicPlayer.White, GameScore.Won), UriKind.Relative),
			});
		}
	}
}
