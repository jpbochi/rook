﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Rook.Games.TicTacToe;

namespace Rook.Games.Tests.TicTacToe
{
	internal class Mother : Helper
	{
		public IGameState GetNewState()
		{
			return Kernel.Get<IGameState>();
		}

		public IGameReferee GetNewReferee()
		{
			return Kernel.GetReferee();
		}

		public IGameMachine GetNewGameMachine()
		{
			return Kernel.Get<IGameMachine>();
		}

		public void BindState(IGameState state)
		{
			Kernel.Rebind<IGameState>().ToConstant(state);
		}

		public void BindBoard(IGameBoard board)
		{
			Kernel.Rebind<IGameBoard>().ToConstant(board);
		}

		public void ParseAndBindBoard(string boardSetup)
		{
			BindBoard(ParseBoard(boardSetup));
		}

		public static IEnumerable<Tuple<string, RName>> GetWinningBoards()
		{
			yield return Tuple.Create("\nXXX\n...\n...", TicTacToePlayer.X);
			yield return Tuple.Create("\n...\nXXX\n...", TicTacToePlayer.X);
			yield return Tuple.Create("\n...\n...\nXXX", TicTacToePlayer.X);
			yield return Tuple.Create("\nX..\nX..\nX..", TicTacToePlayer.X);
			yield return Tuple.Create("\n.X.\n.X.\n.X.", TicTacToePlayer.X);
			yield return Tuple.Create("\n..X\n..X\n..X", TicTacToePlayer.X);
			yield return Tuple.Create("\nX..\n.X.\n..X", TicTacToePlayer.X);
			yield return Tuple.Create("\n..X\n.X.\nX..", TicTacToePlayer.X);

			yield return Tuple.Create("\nOOO\n...\n...", TicTacToePlayer.O);
			yield return Tuple.Create("\n...\nOOO\n...", TicTacToePlayer.O);
			yield return Tuple.Create("\n...\n...\nOOO", TicTacToePlayer.O);
			yield return Tuple.Create("\nO..\nO..\nO..", TicTacToePlayer.O);
			yield return Tuple.Create("\n.O.\n.O.\n.O.", TicTacToePlayer.O);
			yield return Tuple.Create("\n..O\n..O\n..O", TicTacToePlayer.O);
			yield return Tuple.Create("\nO..\n.O.\n..O", TicTacToePlayer.O);
			yield return Tuple.Create("\n..O\n.O.\nO..", TicTacToePlayer.O);
		}

		internal static IEnumerable<string> GetDrawBoards()
		{
			yield return "\nXOX\nOXO\nOXO";
			yield return "\nOXO\nXOX\nXOX";
		}

		public static IEnumerable<string> GetGameNotEndedBoards()
		{
			yield return "\nXOX\nOXO\nOX.";
			yield return "\nOXO\nXOX\nXO.";
		}

		public static IEnumerable<string> GetBoards()
		{
			return
			GetGameNotEndedBoards()
			.Concat(GetDrawBoards())
			.Concat(GetWinningBoards().Select(t => t.Item1));
		}
	}
}
