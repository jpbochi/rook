﻿using System;
using Rook.Games.TicTacToe;
using Xunit;

namespace Rook.Games.Tests.TicTacToe
{
	public class TicTacToeMoveTests
	{
		private readonly Mother mother = new Mother();

		[Fact]
		void should_AddPiece_CycleTurn_and_UpdateScore()
		{
			var state = new BasicGameState(null, null);

			var recorder = new UriCommandRecorder();
			var move = new TicTacToeMove("player", "tac", "b2");

			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri("/add/tac/b2", UriKind.Relative),
				new Uri("/cycle-turn", UriKind.Relative),
				new Uri("/tic-tac-toe/score", UriKind.Relative),
			});
		}
	}
}
