﻿using System;
using System.Linq;
using System.Text;
using Ninject;
using Rook.Games.TicTacToe;

namespace Rook.Games.Tests.TicTacToe
{
	internal class Helper
	{
		private readonly Lazy<IKernel> lazyKernel = new Lazy<IKernel>( StaticGameFactory.GetKernel<TicTacToeGameModule> );

		public IKernel Kernel 
		{
			get { return lazyKernel.Value; }
		}
		
		/// <param name="strBoard">Example: "\n...\n.X.\n..O"</param>
		protected internal IGameBoard ParseBoard(string strBoard)
		{
			var board = Kernel.Get<IGameBoard>();

			var lines = strBoard.Split(new []{'\n'}, StringSplitOptions.RemoveEmptyEntries);

			var xClass = board.Pieces.ById(TicTacToePieces.X);
			var oClass = board.Pieces.ById(TicTacToePieces.O);

			for (int r=0; r<lines.Length; r++) {
				for (int c=0; c<lines[0].Length; c++) {
					if (lines[r][c] == 'X') board.AddPiece(xClass.New(), TilePos.Create(c, r).ToTileName());
					else if (lines[r][c] == 'O') board.AddPiece(oClass.New(), TilePos.Create(c, r).ToTileName());
				}
			}

			return board;
		}

		protected internal static string DumpBoard(IGameBoard board)
		{
			var builder = new StringBuilder();

			for (int r=0; r<3; r++) {
				builder.Append('\n');

				for (int c=0; c<3; c++) {
					var piece = board.PiecesAt(TilePos.Create(c, r).ToTileName()).FirstOrDefault();

					var ch = (piece == null) ? '.'
						   : (piece.GetBanner() == TicTacToePlayer.X) ? 'X'
						   : (piece.GetBanner() == TicTacToePlayer.O) ? 'O'
						   : '?';
					
					builder.Append(ch);
				}
			}

			return builder.ToString();
		}
	}
}
