﻿using System.Collections.Generic;
using System.Linq;
using Rook.Games.TicTacToe;
using Xunit;
using Xunit.Extensions;

namespace Rook.Games.Tests.TicTacToe
{
	public class TicTacToeStateTests
	{
		private readonly Mother mother = new Mother();

		public static IEnumerable<object[]> TestBoards
		{
			get { return Mother.GetBoards().Select( x => new object[] { x } ); }
		}

		[Fact]
		void should_create_initial_state()
		{
			var state = mother.GetNewState();
			
			VerifyInitialState(state);
		}

		public static void VerifyInitialState(IGameState state)
		{
			VerifyAnyState(state);

			state.CurrentPlayerId.ShouldBe(TicTacToePlayer.X);

			state.Board.PiecesAt(Tiles.MainBoard).ShouldBeEmpty();
			
			Assert.Equal(2, state.Board.Classes.Count());
			state.Board.Pieces.ById(TicTacToePieces.X).ShouldNotBeNull().GetBanner().ShouldBe(TicTacToePlayer.X);
			state.Board.Pieces.ById(TicTacToePieces.O).ShouldNotBeNull().GetBanner().ShouldBe(TicTacToePlayer.O);
		}

		public static void VerifyAnyState(IGameState state)
		{
			state.Players.Select( p => p.Id ).ShouldBeEqualInOrder(new []{ TicTacToePlayer.X, TicTacToePlayer.O });

			state.Board
			.ShouldNotBeNull()
			.Region(Tiles.MainBoard)
			.ShouldContainSameAs(
				from c in Enumerable.Range(0, 3)
				from r in Enumerable.Range(0, 3)
				select TilePos.Create(c, r).ToTileName()
			);

			state.Board.Pieces.ShouldEach( p => TicTacToePlayer.Piece[p.GetBanner()].ShouldBe(p.Class) );
		}

		[Theory]
		[PropertyData("TestBoards")]
		void should_parse_and_dump_board(string boardDesc)
		{
			var board = mother.ParseBoard(boardDesc);

			Assert.Equal(boardDesc, Helper.DumpBoard(board));
		}
	}
}
