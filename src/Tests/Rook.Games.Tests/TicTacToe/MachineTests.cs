﻿using System.Linq;
using Rook.Games.TicTacToe;
using Xunit;

namespace Rook.Games.Tests.TicTacToe
{
	public class MachineTests
	{
		private readonly Mother mother = new Mother();

		[Fact]
		void should_have_valid_initial_state()
		{
			var game = mother.GetNewGameMachine();

			TicTacToeStateTests.VerifyInitialState(game.State);
			TicTacToeRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		[Fact]
		void should_provide_resources()
		{
			var resources = mother.GetNewGameMachine().Resources.Select(r => r.ShouldBeOfType<WebResourceInfo>());;

			resources.Select(r => r.CssClass).ShouldContainSameAs(new [] { "board-backboard squares", "piece-set chess" });
		}

		[Fact]
		void should_play_valid_first_move()
		{
			var game = mother.GetNewGameMachine();

			game.PlayDropAt("b3");

			string expectedBoard = (@"
				...
				...
				.X."
			).ClearLiteral();

			Assert.Equal(expectedBoard, Helper.DumpBoard(game.State.Board));
			Assert.Equal(TicTacToePlayer.O, game.State.CurrentPlayerId);
			game.State.AssertScoreIsIndeterminate();
		}

		[Fact]
		void should_declare_win_after_killing_X_move()
		{
			string initialBoard = (@"
				X.X
				.O.
				X.O"
			).ClearLiteral();
			string finalBoard = (@"
				XXX
				.O.
				X.O"
			).ClearLiteral();

			mother.ParseAndBindBoard(initialBoard);
			var game = mother.GetNewGameMachine();

			game.PlayDropAt("b1");

			Assert.Equal(finalBoard, Helper.DumpBoard(game.State.Board));

			game.State.GetPlayerById(TicTacToePlayer.X).Score.ShouldBe(GameScore.Won);
			game.State.GetPlayerById(TicTacToePlayer.O).Score.ShouldBe(GameScore.Lost);
		}

		[Fact]
		void should_declare_win_after_killing_O_move()
		{
			string initialBoard = (@"
				X.X
				O.O
				X.X"
			).ClearLiteral();
			string finalBoard = (@"
				X.X
				OOO
				X.X"
			).ClearLiteral();

			mother.ParseAndBindBoard(initialBoard);
			var game = mother.GetNewGameMachine();
			game.State.CurrentPlayerId = TicTacToePlayer.O;

			game.PlayDropAt("b2");

			Assert.Equal(finalBoard, Helper.DumpBoard(game.State.Board));

			game.State.GetPlayerById(TicTacToePlayer.O).Score.ShouldBe(GameScore.Won);
			game.State.GetPlayerById(TicTacToePlayer.X).Score.ShouldBe(GameScore.Lost);
		}

		[Fact]
		void should_end_after_draw_move()
		{
			string initialBoard = (@"
				OXO
				XOX
				XO."
			).ClearLiteral();
			string finalBoard = (@"
				OXO
				XOX
				XOX"
			).ClearLiteral();

			mother.ParseAndBindBoard(initialBoard);
			var game = mother.GetNewGameMachine();

			game.PlayDropAt("c3");

			Assert.Equal(finalBoard, Helper.DumpBoard(game.State.Board));
			game.State.AssertDraw();
		}
	}
}
