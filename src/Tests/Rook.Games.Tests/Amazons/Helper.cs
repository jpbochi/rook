﻿using System;
using System.Linq;
using System.Text;
using Ninject;
using Rook.Games.Amazons;

namespace Rook.Games.Tests.Amazons
{
	public class Helper
	{
		private readonly Lazy<IKernel> lazyKernel = new Lazy<IKernel>( StaticGameFactory.GetKernel<AmazonsGameModule> );

		public IKernel Kernel 
		{
			get { return lazyKernel.Value; }
		}

		/// <param name="strBoard">Example: "\nA#.\n...\n.#a"</param>
		public IGameBoard ParseBoard(string strBoard)
		{
			var lines = strBoard.Split(new []{'\n'}, StringSplitOptions.RemoveEmptyEntries);

			int rows = lines.Length;
			int cols = lines[0].Length;
			Kernel.Rebind<IGameTileGraph>().ToConstant(new BasicTileGraph(
				new RectangularGridTileSet((ushort)cols, (ushort)rows),
				VectorBasedLinkSet.CreateCardinalPlusIntercardinal()
			));

			var board = Kernel.Get<BasicBoard>();

			var amazonClass = board.Pieces.ById(AmazonsPieces.Amazon);
			var arrowClass = board.Pieces.ById(AmazonsPieces.Arrow);

			for (int r=0; r<rows; r++) {
				for (int c=0; c<cols; c++) {
					if (lines[r][c] == 'A') board.AddPiece(amazonClass.New().SetBanner(ClassicPlayer.White), TilePos.Create(c, r).ToTileName());
					else
					if (lines[r][c] == 'a') board.AddPiece(amazonClass.New().SetBanner(ClassicPlayer.Black), TilePos.Create(c, r).ToTileName());
					else
					if (lines[r][c] == '#') board.AddPiece(arrowClass.New(), TilePos.Create(c, r).ToTileName());
				}
			}

			return board;
		}

		protected static string PrepareBoard(string boardSetup)
		{
			return boardSetup
				.Replace(" ", "")
				.Replace("\t", "")
				.Replace("\r", "")
				.TrimEnd('\n');
		}

		public static string DumpBoard(IGameBoard board)
		{
			var lines = board.Region(Tiles.MainBoard).GroupBy( t => t.ToPointInt().Value.Y ).ToArray();

			var builder = new StringBuilder();
			foreach (var line in lines) {
				builder.Append('\n');

				foreach (var tile in line) {
					var piece = board.PiecesAt(tile).FirstOrDefault();

					var ch = (piece == null) ? '.'
						   : (piece.GetBanner() == ClassicPlayer.White) ? 'A'
						   : (piece.GetBanner() == ClassicPlayer.Black) ? 'a'
						   : '#';
					
					builder.Append(ch);
				}
			}
			
			return builder.ToString();
		}
	}
}
