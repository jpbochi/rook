﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Moq;
using Rook.Games.Amazons;
using Rook.Games.Common;
using Xunit;

namespace Rook.Games.Tests.Amazons
{
	public class AmazonsRefereeTests
	{
		private readonly Mother mother = new Mother();

		static void PlayMoveTo(IGameState state, IGameReferee referee, RName origin, RName destination)
		{
			var move = referee.GetMoves(state).First(
				m => m.GetOrigin() == origin && m.GetDestination() == destination
			);

			referee.ExecuteMove(move.Href, new MoveExecutionContext(state, referee, TestUser.ForCurrent(state))).ShouldBeTrue("Failed to play move");
		}

		[Fact]
		void should_provide_initial_move_options()
		{
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			var moves = referee.GetMoves(state).ToArray();
			Assert.NotNull(moves);

			VerifyInitialMoveOptions_Links(moves, state.Board);
		}

		internal static void VerifyInitialMoveOptions_Links(IEnumerable<IMoveLink> moveDescs, IGameBoard board)
		{
			var expectedValidMoves = new [] {
				"a3-a1", "a3-a2", "a3-a4", "a3-b2", "a3-b3", "a3-b4", "a3-c3", "a3-c5", "a3-d3", "a3-d6", "a3-e3", "a3-f3", // from a3
				"c1-a1", "c1-b1", "c1-b2", "c1-c2", "c1-c3", "c1-c4", "c1-c5", "c1-c6", "c1-d1", "c1-d2", "c1-e3", "c1-f4", // from c1
				"e1-b4", "e1-c3", "e1-d1", "e1-d2", "e1-e2", "e1-e3", "e1-e4", "e1-e5", "e1-e6", "e1-f1", "e1-f2", "e1-g1", // from e1
				"g3-b3", "g3-c3", "g3-d3", "g3-d6", "g3-e3", "g3-e5", "g3-f2", "g3-f3", "g3-f4", "g3-g1", "g3-g2", "g3-g4", // from g3
			};

			var actualValidMoves = moveDescs
				.Where(m => !m.Is<ResignMove>())
				.Select(m => string.Format("{0}-{1}", m.GetOrigin(), m.GetDestination()));

			actualValidMoves.ShouldContainSameAs(expectedValidMoves);

			var expectedPlayerId = ClassicPlayer.White;
			moveDescs.Where(m => !m.Is<ResignMove>()).ShouldEach( m => m.GetPlayerId().ShouldBe(expectedPlayerId) );
		}

		[Fact]
		void should_allow_arrows_after_amazon_move()
		{
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			RName origin = "c1";
			RName destination = "c5";
			PlayMoveTo(state, referee, origin, destination);

			var expectedArrows = new [] {
			    "a7", "b4", "b5", "b6", "c1", "c2", "c3", "c4", "c6", "d4", "d5", "d6", "e3", "e5", "f2", "f5", "g1"
			};

			var moves = referee.GetMoves(state).ToArray();
			VerifyMoveOptionsAfterFirstMove_Descriptions(moves, state.Board, destination, expectedArrows);
		}

		static void VerifyMoveOptionsAfterFirstMove_Descriptions(IEnumerable<IMoveLink> moveDescs, IGameBoard board, RName moveDestination, IEnumerable<string> expectedArrows)
		{
			var expectedPieceClass = AmazonsPieces.Arrow;
			var expectedPlayerId = ClassicPlayer.White;

			var expectedMoves = expectedArrows.Select(
				tile => MoveLink.From(
					AmazonsShootArrowMove.Template,
					AmazonsShootArrowMove.Relation,
					new { PlayerId = expectedPlayerId, Piece = expectedPieceClass, Position = tile }
				).Href
			);

			moveDescs.Where(m => !m.Is<ResignMove>()).Select(m => m.Href).ShouldContainSameAs(expectedMoves);
		}

		[Fact]
		void should_accept_move_if_user_is_authorized()
		{
			var state = mother.GetNewState();
			var referee = new AmazonsReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == AmazonsSlideMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(true).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeTrue("Failed to play move");
			userMock.Verify();
		}

		[Fact]
		void should_reject_move_if_user_is_not_authorized()
		{
			var state = mother.GetNewState();
			var referee = new AmazonsReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == AmazonsSlideMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(false).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeFalse("Referee accepted unauthorized move");
			userMock.Verify();
		}
	}
}
