﻿using System;
using Rook.Games.Amazons;
using Xunit;

namespace Rook.Games.Tests.Amazons
{
	public class AmazonsSlideMoveTests
	{
		private readonly Mother mother = new Mother();

		[Fact]
		void should_move_a_piece_and_update_score()
		{
			var state = new BasicGameState(null, null);
			var piece = new BasicPiece("pawn");
			RName destination = "f3";

			var recorder = new UriCommandRecorder();
			var move = new AmazonsSlideMove(null, piece, destination);
			
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri("/move/pawn/f3", UriKind.Relative),
				new Uri("/set/Rook.Games.Amazons.AmazonsProperties!LastPieceToMove/pawn", UriKind.Relative),
			});
		}
	}
}
