﻿using System.Reflection;

namespace Rook.Games.Tests.Amazons
{
	public class AmazonsSerializationTests : BaseSerializationTests
	{
		private readonly Mother mother = new Mother();

		protected override string GetSerializedInitialGameInProd()
		{
			return ReadTextResource("Rook.Games.Tests.SampleData.initial-amazons.bson.txt", Assembly.GetExecutingAssembly());
		}

		protected override IGameState GetNewInitialState()
		{
			return mother.GetNewState();
		}

		protected override IGameState GetNewChangedState()
		{
			var game = mother.GetNewGameMachine();

			game.PlayMove("c1", "c5");
			game.PlayDropAt("e5");

			return game.State;
		}

		protected override IGameMachine GetNewGameMachine()
		{
			return mother.GetNewGameMachine();
		}

		protected override void VerifyInitialState(IGameState state)
		{
			AmazonsGameModuleTests.VerifyInitialState(state);

			mother.BindState(state);
			var game = mother.GetNewGameMachine();

			AmazonsRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		protected override void VerifyAnyState(IGameState state)
		{
			AmazonsGameModuleTests.VerifyAnyState(state);
		}
	}
}
