﻿using System;
using Rook.Games.Amazons;
using Xunit;

namespace Rook.Games.Tests.Amazons
{
	public class AmazonsShootArrowMoveTests
	{
		private readonly Mother mother = new Mother();

		[Fact]
		void should_move_a_piece_and_update_score()
		{
			var state = new BasicGameState(null, null);
			RName pieceClass = "arrow";
			RName destination = "d4";

			var recorder = new UriCommandRecorder();
			var move = new AmazonsShootArrowMove(null, pieceClass, destination);
			
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri("/add/arrow/d4", UriKind.Relative),
				new Uri("/reset/Rook.Games.Amazons.AmazonsProperties!LastPieceToMove", UriKind.Relative),
				new Uri("/cycle-turn", UriKind.Relative),
				new Uri("/amazons/score", UriKind.Relative),
			});
		}
	}
}
