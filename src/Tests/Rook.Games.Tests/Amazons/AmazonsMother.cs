﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Ninject.Modules;

namespace Rook.Games.Tests.Amazons
{
	internal class Mother : Helper
	{
		public TModule GetModule<TModule>() where TModule : INinjectModule
		{
			return Kernel.GetModules().OfType<TModule>().Single();
		}

		public IGameState GetNewState()
		{
			return Kernel.Get<IGameState>();
		}

		public IGameReferee GetNewReferee()
		{
			return Kernel.GetReferee();
		}

		public IGameMachine GetNewGameMachine()
		{
			return Kernel.Get<IGameMachine>();
		}

		public void BindState(IGameState state)
		{
			Kernel.Rebind<IGameState>().ToConstant(state);
		}

		public void BindBoard(IGameBoard board)
		{
			Kernel.Rebind<IGameBoard>().ToConstant(board);
		}

		public void ParseAndBindBoard(string boardSetup)
		{
			BindBoard(ParseBoard(boardSetup));
		}

		public static IEnumerable<Tuple<string,RName,RName>> GetWinningBoards()
		{
			yield return Tuple.Create(PrepareBoard(@"
				A#.
				##a")
				, ClassicPlayer.White
				, ClassicPlayer.Black
			);
			yield return Tuple.Create(PrepareBoard(@"
				A##
				.#a")
				, ClassicPlayer.Black
				, ClassicPlayer.White
			);
		}

		public static IEnumerable<string> GetGameNotEndedBoards()
		{
			yield return PrepareBoard(@"
				..A.A..
				A.....A
				a.....a
				..a.a..");
			yield return PrepareBoard(@"
				AAAA#..
				####...
				.......
				..#####
				...aaaa");
			yield return PrepareBoard(@"
				A##
				.#a");
			yield return PrepareBoard("\nA.a");
			yield return PrepareBoard("\nA.#a");
		}

		public static IEnumerable<string> GetBoards()
		{
			return GetGameNotEndedBoards()
			.Concat(GetWinningBoards().Select(t => t.Item1));
		}
	}
}
