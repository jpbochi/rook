{
  "_t": "BasicGameState",
  "Aug": {
    "{Rook.BasicGameState}CurrentPlayerId": "\"chinese-checkers.player!_1st\""
  },
  "Players": [
    {
      "_t": "BasicPlayer",
      "_id": "chinese-checkers.player!_1st"
    },
    {
      "_t": "BasicPlayer",
      "_id": "chinese-checkers.player!_2nd"
    }
  ],
  "Board": {
    "_t": "BasicBoard",
    "Pieces": {
      "SerializableBoardDict": [
        {
          "_t": "BasicPiece",
          "Aug": {
            "{Rook.PieceProperties}Banner": "\"chinese-checkers.player!_1st\""
          },
          "_id": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "_class_"
        },
        {
          "_t": "BasicPiece",
          "Aug": {
            "{Rook.PieceProperties}Banner": "\"chinese-checkers.player!_2nd\""
          },
          "_id": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "_class_"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-5fc99ee6",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "e4"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-33b779d3",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "f2"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-731c180a",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "f3"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-5c58c957",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "f4"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-6fc9453",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "g1"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-6b617dbd",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "g2"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-259a2816",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "g3"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-72b505c1",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "g4"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-7673ce7c",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "h3"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!N-3bc94a2",
          "BasePieceId": "chinese-checkers.piece!N",
          "Class": "chinese-checkers.piece!N",
          "Position": "h4"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-3ff2f264",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "e14"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-33b779d3",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "f14"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-731c180a",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "g14"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-5c58c957",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "h14"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-6fc9453",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "f15"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-6b617dbd",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "g15"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-259a2816",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "h15"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-72b505c1",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "f16"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-7673ce7c",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "g16"
        },
        {
          "_t": "BasicPiece",
          "_id": "chinese-checkers.piece!S-3bc94a2",
          "BasePieceId": "chinese-checkers.piece!S",
          "Class": "chinese-checkers.piece!S",
          "Position": "g17"
        }
      ]
    },
    "Graph": {
      "_t": "BasicTileGraph",
      "TileSet": {
        "_t": "BasicTileSet",
        "SerializableTiles": "e4|f2|f3|f4|g1|g2|g3|g4|h3|h4|a5|b5|c5|d5|a6|b6|c6|b7|c7|b8|j5|k5|l5|m5|j6|k6|l6|k7|l7|k8|b10|b11|c11|a12|b12|c12|a13|b13|c13|d13|k10|k11|l11|j12|k12|l12|j13|k13|l13|m13|e14|f14|g14|h14|f15|g15|h15|f16|g16|g17|e5|f5|g5|h5|i5|d6|e6|f6|g6|h6|i6|d7|e7|f7|g7|h7|i7|j7|c8|d8|e8|f8|g8|h8|i8|j8|c9|d9|e9|f9|g9|h9|i9|j9|k9|c10|d10|e10|f10|g10|h10|i10|j10|d11|e11|f11|g11|h11|i11|j11|d12|e12|f12|g12|h12|i12|e13|f13|g13|h13|i13"
      },
      "LinkSet": {
        "_t": "HexLinkSet",
        "Model": 1
      }
    }
  }
}