﻿using System.Linq;
using Ninject;
using Ninject.Modules;

namespace Rook.Games.Tests.ChineseCheckers
{
	internal class ChChMother : ChChHelper
	{
		public TModule GetModule<TModule>() where TModule : INinjectModule
		{
			return Kernel.GetModules().OfType<TModule>().Single();
		}

		public IGameState GetNewState()
		{
			return Kernel.Get<IGameState>();
		}

		public IGameReferee GetNewReferee()
		{
			return Kernel.GetReferee();
		}

		public IGameMachine GetNewGameMachine()
		{
			return Kernel.Get<IGameMachine>();
		}

		public void BindState(IGameState state)
		{
			Kernel.Rebind<IGameState>().ToConstant(state);
		}

		public void BindBoard(IGameBoard board)
		{
			Kernel.Rebind<IGameBoard>().ToConstant(board);
		}

		public void ParseAndBindBoard(string boardSetup)
		{
			BindBoard(ParseBoard(boardSetup));
		}
	}
}
