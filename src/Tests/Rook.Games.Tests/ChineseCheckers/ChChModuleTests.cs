﻿using System.Linq;
using Rook.Games.ChineseCheckers;
using Xunit;

namespace Rook.Games.Tests.ChineseCheckers
{
	public class ChChModuleTests
	{
		private readonly ChChMother mother = new ChChMother();

		[Fact]
		void should_create_initial_state()
		{
			var state = mother.GetNewState();
			
			VerifyInitialState(state, 2);
		}

		internal static void VerifyInitialState(IGameState state, int? playerCount = null)
		{
			int players = playerCount ?? state.Players.Count();

			VerifyAnyState(state, playerCount);

			state.CurrentPlayerId.ShouldBe(ChineseCheckersPlayer.First);
			
			if (players == 2) {
				var expectedBoard
					= "N-e4|N-f2|N-f3|N-f4|N-g1|N-g2|N-g3|N-g4|N-h3|N-h4"
					+ "|S-e14|S-f14|S-f15|S-f16|S-g14|S-g15|S-g16|S-g17|S-h14|S-h15";
				ChChHelper.DumpBoard(state.Board).ShouldBe(expectedBoard);
			}

			state.Board.Pieces.ShouldEach( p => p.Class.Namespace.ShouldBe(ChineseCheckersPiece.Namespace) );
		}

		internal static void VerifyAnyState(IGameState state, int? playerCount = null)
		{
			int players = playerCount ?? state.Players.Count();

			state.Players.Select( p => p.Id ).ShouldBeEqualInOrder(new []{
				ChineseCheckersPlayer.First,
				ChineseCheckersPlayer.Second,
				ChineseCheckersPlayer.Third,
				ChineseCheckersPlayer.Fourth,
				ChineseCheckersPlayer.Fifth,
				ChineseCheckersPlayer.Sixth
			}.Take(players));

			state.Board
			.ShouldNotBeNull()
			.Region(Tiles.MainBoard)
			.ShouldContainSameAs(
				new RName[] {
					"e4", "f2", "f3", "f4", "g1", "g2", "g3", "g4", "h3", "h4", //north region
					"a5", "b5", "c5", "d5", "a6", "b6", "c6", "b7", "c7", "b8", //northwest region
					"j5", "k5", "l5", "m5", "j6", "k6", "l6", "k7", "l7", "k8", //northeast region
					"b10", "b11", "c11", "a12", "b12", "c12", "a13", "b13", "c13", "d13", //southwest region
					"k10", "k11", "l11", "j12", "k12", "l12", "j13", "k13", "l13", "m13", //southeast region
					"e14", "f14", "g14", "h14", "f15", "g15", "h15", "f16", "g16", "g17", //south region
					//no checker's land
					              "e5",  "f5",  "g5",  "h5",  "i5",
					       "d6",  "e6",  "f6",  "g6",  "h6",  "i6",
					       "d7",  "e7",  "f7",  "g7",  "h7",  "i7",  "j7",
					"c8",  "d8",  "e8",  "f8",  "g8",  "h8",  "i8",  "j8",
					"c9",  "d9",  "e9",  "f9",  "g9",  "h9",  "i9",  "j9",  "k9",
					"c10", "d10", "e10", "f10", "g10", "h10", "i10", "j10",
					       "d11", "e11", "f11", "g11", "h11", "i11", "j11",
					       "d12", "e12", "f12", "g12", "h12", "i12",
					              "e13", "f13", "g13", "h13", "i13",
				}
			);

			state.Board.Pieces.ShouldEach(
				p => p.GetBanner().ShouldNotBeNull().Namespace.ShouldBe(ChineseCheckersPlayer.Namespace)
			);
		}
	}
}
