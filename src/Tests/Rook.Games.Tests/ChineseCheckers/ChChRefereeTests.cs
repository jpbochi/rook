﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Moq;
using Rook.Games.ChineseCheckers;
using Rook.Games.Common;
using Xunit;

namespace Rook.Games.Tests.ChineseCheckers
{
	public class ChChRefereeTests
	{
		private readonly ChChMother mother = new ChChMother();

		static void PlayMoveTo(IGameState state, IGameReferee referee, RName origin, RName destination)
		{
			var move = referee.GetMoves(state).Where(
				m => m.GetOrigin() == origin && m.GetDestination() == destination
			).First();

			referee.ExecuteMove(move.Href, new MoveExecutionContext(state, referee, null));
		}

		[Fact]
		void should_provide_initial_move_options()
		{
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			var moves = referee.GetMoves(state).ShouldNotBeNull().ToList();

			VerifyInitialMoveOptions_Links(moves, state.Board);
		}

		[Fact]
		void should_allow_multi_hop_moves()
		{
			var initialBoard = "N-f13|N-f16|N-g16|N-h14|S-e14|S-g17";
			mother.ParseAndBindBoard(initialBoard);

			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			var moves = referee.GetMoves(state).ShouldNotBeNull().ToList();

			var expectedValidMoves = new [] {
				"g17-f15", "g17-e13", "g17-g13", "g17-h15", "g17-i13",
				"e14-e13", "e14-f15", "e14-f14", "e14-f12"
			};

			AssertMoveLinks(moves.Where(m => !m.Is<ResignMove>()), ChineseCheckersPlayer.First, expectedValidMoves);
		}

		internal static void VerifyInitialMoveOptions_Links(IEnumerable<IMoveLink> initialMoves, IGameBoard board)
		{
			var expectedValidMoves = new [] {
				"e14-e13", "e14-f13", "f14-f13", "f14-g13", "g14-g13", "g14-h13", "h14-h13", "h14-i13", //single walk
				"f15-e13", "f15-g13", "g15-f13", "g15-h13", "h15-g13", "h15-i13", //single hop
			};

			AssertMoveLinks(initialMoves.Where(m => !m.Is<ResignMove>()), ChineseCheckersPlayer.First, expectedValidMoves);
		}

		private static void AssertMoveLinks(IEnumerable<IMoveLink> actualMoves, RName expectedPlayerId, string[] expectedMoves)
		{
			var transformedMoves = actualMoves.Select(m => string.Format("{0}-{1}", m.GetOrigin(), m.GetDestination()));

			transformedMoves.ShouldContainSameAs(expectedMoves);

			actualMoves.Where(m => !m.Is<ResignMove>()).ShouldEach( m => m.GetPlayerId().ShouldBe(expectedPlayerId) );
		}

		[Fact]
		void should_accept_move_if_user_is_authorized()
		{
			var state = mother.GetNewState();
			var referee = new ChChReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == CheckerMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(true).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeTrue("Failed to play move");
			userMock.Verify();
		}

		[Fact]
		void should_reject_move_if_user_is_not_authorized()
		{
			var state = mother.GetNewState();
			var referee = new ChChReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == CheckerMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(false).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeFalse("Referee accepted unauthorized move");
			userMock.Verify();
		}
	}
}
