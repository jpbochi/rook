﻿using System;
using Rook.Games.ChineseCheckers;
using Xunit;

namespace Rook.Games.Tests.ChineseCheckers
{
	public class CheckerMoveTests
	{
		private readonly ChChMother mother = new ChChMother();

		[Fact]
		void should_move_a_piece_and_update_score()
		{
			var state = new BasicGameState(null, null);
			var piece = new BasicPiece("a-checker");
			RName destination = "g9";

			var recorder = new UriCommandRecorder();
			var move = new CheckerMove(null, piece, destination);
			
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri("/move/a-checker/g9", UriKind.Relative),
				new Uri("/cycle-turn", UriKind.Relative),
				new Uri("/chinese-checkers/score", UriKind.Relative),
			});
		}
	}
}
