﻿using System.Linq;
using Rook.Games.ChineseCheckers;
using Xunit;

namespace Rook.Games.Tests.ChineseCheckers
{
	public class ChChMachineTests
	{
		private readonly ChChMother mother = new ChChMother();

		[Fact]
		void should_provide_initial_move_options()
		{
			var game = mother.GetNewGameMachine();

			ChChModuleTests.VerifyInitialState(game.State);
			ChChRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		[Fact]
		void should_provide_resources()
		{
			var resources = mother.GetNewGameMachine().Resources.Select(r => r.ShouldBeOfType<WebResourceInfo>());;

			resources.Select(r => r.CssClass).ShouldContainSameAs(new [] { "board-backboard chinese-checkers", "piece-set checkers" });
		}

		[Fact]
		void should_play_valid_first_move()
		{
			var game = mother.GetNewGameMachine();

			game.PlayMove("g15", "h13");

			var expectedSouthernPieces = new [] {"e14", "f14", "g14", "h14", "f15", "h13", "h15", "f16", "g16", "g17"};

			ChChHelper.AssertPiecesPositions(game.State.Board, ChineseCheckersPiece.South, expectedSouthernPieces);

			game.State.CurrentPlayerId.ShouldBe(ChineseCheckersPlayer.Second);
			game.State.AssertScoreIsIndeterminate();
		}

		[Fact]
		void should_play_multi_hop()
		{
			var game = mother.GetNewGameMachine();

			game.PlayMove("h15", "g13");
			game.PlayMove("e4", "e5");
			game.PlayMove("g17", "i13");

			var expectedSouthernPieces = new [] {"e14", "f14", "g14", "h14", "f15", "g15", "g13", "f16", "g16", "i13"};

			ChChHelper.AssertPiecesPositions(game.State.Board, ChineseCheckersPiece.South, expectedSouthernPieces);

			game.State.CurrentPlayerId.ShouldBe(ChineseCheckersPlayer.Second);
			game.State.AssertScoreIsIndeterminate();
		}

		[Fact]
		void should_declare_win_after_killing_South_move()
		{
			var initialBoard = "N-g9|S-e5|S-f2|S-f3|S-f4|S-g1|S-g2|S-g3|S-g4|S-h3|S-h4";
			var finalBoard =   "N-g9|S-e4|S-f2|S-f3|S-f4|S-g1|S-g2|S-g3|S-g4|S-h3|S-h4";
			mother.ParseAndBindBoard(initialBoard);

			var game = mother.GetNewGameMachine();

			game.PlayMove("e5", "e4");

			Assert.Equal(finalBoard, ChChHelper.DumpBoard(game.State.Board));
			
			game.State.GetPlayerById(ChineseCheckersPlayer.First).Score.ShouldBe(GameScore.Won);
			game.State.GetPlayerById(ChineseCheckersPlayer.Second).Score.ShouldBe(GameScore.Lost);
		}

		[Fact]
		void should_declare_win_after_killing_North_move()
		{
			var initialBoard = "N-e13|N-f14|N-f15|N-f16|N-g14|N-g15|N-g16|N-g17|N-h14|N-h15|S-g9";
			var finalBoard =   "N-e14|N-f14|N-f15|N-f16|N-g14|N-g15|N-g16|N-g17|N-h14|N-h15|S-g8";
			mother.ParseAndBindBoard(initialBoard);

			var game = mother.GetNewGameMachine();

			game.PlayMove("g9", "g8");
			game.PlayMove("e13", "e14");

			Assert.Equal(finalBoard, ChChHelper.DumpBoard(game.State.Board));
			
			game.State.GetPlayerById(ChineseCheckersPlayer.First).Score.ShouldBe(GameScore.Lost);
			game.State.GetPlayerById(ChineseCheckersPlayer.Second).Score.ShouldBe(GameScore.Won);
		}
	}
}
