﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;
using JpLabs.Extensions;

namespace Rook.Browser.Tests
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public class IISParamsAttribute : Attribute
	{
		public string RelativeAppPath { get; set; }
		public int Port { get; set; }

		public IISParamsAttribute(string relativeAppPath, int port = 9090)
		{
			RelativeAppPath = relativeAppPath;
			Port = port;
		}
	}
}
