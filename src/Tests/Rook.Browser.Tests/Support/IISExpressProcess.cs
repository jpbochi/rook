﻿using System;
using System.Diagnostics;
using System.IO;
using Rook.Tests;

namespace Rook.Browser.Tests
{
	public class IISExpressProcess : IDisposable
	{
		public Process Process { get; private set; }
		public int Port { get; private set; }

		public IISExpressProcess(string relativeAppPath, int port = 9090)
		{
			Port = port;
			Process = StartIIS(relativeAppPath, Port);
		}

		public Uri BaseUri
		{
			get { return new Uri(string.Format("http://localhost:{0}", Port), UriKind.Absolute); }
		}

		public void Dispose()
		{
			if (!Process.HasExited) {
				Trace.WriteLine("Closing IIS Express...");
				Process.Kill();
				if (!Process.WaitForExit(3000)) throw new TimeoutException("Failed to close IIS Express!");

				Process.StandardOutput.TraceWriteWithPrefix("iisexpress: ", _ => true);
				Trace.WriteLine(string.Format("IIS Express exited with {0}.", Process.ExitCode));
			}
		}
		
		static Process StartIIS(string relativeAppPath, int port)
		{
			var appPath = GetApplicationPath(relativeAppPath);
			if (!Directory.Exists(appPath)) throw new ApplicationException(string.Format("Path '{0}' doesn't exist.", appPath));

			var programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);

			var startInfo = new ProcessStartInfo
			{
				FileName = Path.Combine(programFiles, "IIS Express\\iisexpress.exe"),
				Arguments = string.Format("/path:\"{0}\" /port:{1} /systray:false", appPath, port),
				UseShellExecute = false,
				RedirectStandardError = true,
				RedirectStandardOutput = true,
				CreateNoWindow = true
			};
			Trace.WriteLine(string.Format("Running \"{0}\" {1}", startInfo.FileName, startInfo.Arguments));

			var iisProcess = new Process { StartInfo = startInfo };
			iisProcess.Start();
			return iisProcess;
		}

		static string GetApplicationPath(string relativeAppPath)
		{
			return Path.Combine(ProcessHelper.SolutionDir, relativeAppPath);
		}
	}
}
