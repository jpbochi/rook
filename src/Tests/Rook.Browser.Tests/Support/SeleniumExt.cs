﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Xunit;

namespace Rook.Browser.Tests
{
	public static class WebDriverExtensions
	{
		public static IWebElement WaitForElement(this IWebDriver driver, int timeoutInSeconds, By by)
		{
			if (timeoutInSeconds > 0) {
				var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
				return wait.Until(ExpectedConditions.ElementIsVisible(by));
			}
			return driver.FindElement(by);
		}

		public static void ShouldBeVisible(this IWebElement element, string identifier)
		{
			Assert.True(element.Displayed, string.Format("Expected element '{0}' to be visible.", identifier));
		}
	}
}