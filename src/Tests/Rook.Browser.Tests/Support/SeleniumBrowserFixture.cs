﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace Rook.Browser.Tests
{
    public class SeleniumBrowserFixture : IDisposable
	{
		public RemoteWebDriver Driver { get; private set; }

		public SeleniumBrowserFixture()
		{ 
			Driver = new FirefoxDriver();
		}

		public void Dispose()
		{
			((IDisposable)Driver).Dispose();
		}
	}
}
