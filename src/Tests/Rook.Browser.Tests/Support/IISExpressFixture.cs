﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Rook.Tests;

namespace Rook.Browser.Tests
{
	public class IISExpressFixture : IDisposable
	{
		private IISExpressProcess iis;

		public IISExpressFixture(IISExpressProcess iis)
		{
			this.iis = iis;
		}

		public void Dispose()
		{
			iis.Process.StandardOutput.TraceWriteWithPrefix("iisexpress: ", line =>
				!line.StartsWith("IncrementMessages called") &&
				!line.StartsWith("Request started:")
			);
		}

		public Uri BaseUri
		{
			get { return iis.BaseUri; }
		}
	}
}
