﻿using System.Xml.Linq;
using Newtonsoft.Json;
using Xunit;

namespace Rook.MvcApp.Tests
{
	public class tests
	{
		[Fact]
		void Test()
		{
			var x = new XElement(
				"moves",
				new XObject[] {
					new XElement(
						"move", new [] {
							new XAttribute("{Rook.MoveDesc}MoveType", ":Rook.MoveType:PassTurn"),
							new XAttribute("{Rook.MoveDesc}PlayerId", ":Rook.ClassicPlayer:Black")
						}
					),
					new XElement(
						"move", new [] {
							new XAttribute("{Rook.MoveDesc}MoveType", ":Duelo.MoveType:Shoot"),
							new XAttribute("{Rook.MoveDesc}PlayerId", ":Rook.ClassicPlayer:Black"),
							new XAttribute("{Duelo.Firearms}ShootMode", ":Duelo.ShootMode:Snap")
						}
					),
				}
			);
			
			//new XAttribute(XNamespace.Xmlns + "ns0", "Rook.MoveDesc"),

			MvcExtensions.InsertNamespaceDeclarations(x);

			//x.ToString(SaveOptions.OmitDuplicateNamespaces);

			//System.Diagnostics.Debug.WriteLine(x);
			//x = XElement.Parse(x.ToString());
			//System.Diagnostics.Debug.WriteLine(x);

			string json = JsonConvert.SerializeObject(x, Newtonsoft.Json.Formatting.Indented);
			string expected = 
@"{
  ""moves"": {
	""@xmlns:n0"": ""Rook.MoveDesc"",
	""@xmlns:n1"": ""Duelo.Firearms"",
	""move"": [
	  {
		""@n0:MoveType"": "":Rook.MoveType:PassTurn"",
		""@n0:PlayerId"": "":Rook.ClassicPlayer:Black""
	  },
	  {
		""@n0:MoveType"": "":Duelo.MoveType:Shoot"",
		""@n0:PlayerId"": "":Rook.ClassicPlayer:Black"",
		""@n1:ShootMode"": "":Duelo.ShootMode:Snap""
	  }
	]
  }
}";
			Assert.Equal(expected.Replace("\t", "    "), json);

			var dJson = JsonConvert.DeserializeObject<XElement>(json);
			Assert.Equal(x.ToString(), dJson.ToString());
		}

		[Fact(Skip="not working")]
		void DeserializeMoveRequest()
		{
			string json = 
@"{
	""@xmlns:j0"": ""Rook.MoveDesc"",
	""@j0:MoveType"": "":Rook.MoveType:PassTurn"",
	""@j0:PlayerId"": "":Rook.ClassicPlayer:Black""
}";
			var dJson = JsonConvert.DeserializeObject<XElement>(json);
		}
	}
}
