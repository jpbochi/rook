﻿using System;
using System.Diagnostics;
using System.IO;
using JpLabs.Extensions;
using Rook.Tests;
using Xunit;

namespace Rook.MvcApp.Tests
{
	public class JSUnitTests
	{
		[Fact]
		void should_pass_all_javascript_tests()
		{
			var slnFolder = ProcessHelper.SolutionDir;
			var phantomJSpath = Directory.GetFiles(Path.Combine(slnFolder, "packages"), "phantomjs.exe", SearchOption.AllDirectories).Last();
			var testsHtmlPath = Path.Combine(slnFolder, "Rook.MvcApp", "runtests.html");
			var testsUri = new UriBuilder("file", null, -1, testsHtmlPath).Uri.AbsoluteUri;

			var startInfo = new ProcessStartInfo {
				FileName = phantomJSpath,
				Arguments = string.Format("run-qunit.js {0}", testsUri),
				WorkingDirectory = Path.Combine(slnFolder, "Tests", "Rook.MvcApp.Tests"),
				UseShellExecute = false,
				RedirectStandardError = true,
				RedirectStandardOutput = true,
				CreateNoWindow = true
			};
			Trace.WriteLine(string.Format("executing \"{0}\" {1}", startInfo.FileName, startInfo.Arguments));

			var phantomProcess = new Process { StartInfo = startInfo };
			phantomProcess.Start();

			if (!phantomProcess.WaitForExit(9000)) throw new TimeoutException("phantomjs.exe took too long to close.");

			phantomProcess.StandardError.TraceWriteWithPrefix("phantomjs[error]: ", _ => true);
			phantomProcess.StandardOutput.TraceWriteWithPrefix("phantomjs: ", _ => true);
			phantomProcess.ExitCode.ShouldBe(0);
		}
	}
}
