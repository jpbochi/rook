﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Newtonsoft.Json.Linq;
using Rook.MvcApp;
using Xunit;

namespace Rook.MvcApp.Tests
{
	public class GameViewModelSerializerTests
	{
		[Fact]
		void should_output_version()
		{
			var state = new BasicGameState(null, null);
			state.Revise();

			var model = new GameViewModelSerializer().From(state);

			model.ShouldBeOfType<JObject>();
			model["version"].ShouldBeOfType<JValue>().ToString().ShouldBe(state.Version);
		}

		[Fact]
		void should_output_current_player()
		{
			var state = new BasicGameState(null, null) { CurrentPlayerId = "mr_white" };

			var model = new GameViewModelSerializer().From(state);

			model.ShouldBeOfType<JObject>();
			model["currentPlayer"].ShouldBeOfType<JValue>().ToString().ShouldBe("mr_white");
		}

		[Fact]
		void should_output_players_with_links()
		{
			var state = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");
			state.Id = "feiradafruta";
			state.Players.ElementAt(2).SetValue(BasicPlayer.ScoreProperty, GameScore.Resigned);

			var model = new GameViewModelSerializer().From(state);

			model.ShouldBeOfType<JObject>();
			var players = model["players"].ShouldBeOfType<IEnumerable<JToken>>();
			players.ToString().ShouldBe(new JArray(
				new JObject {
					{ "id", "batima" },
					{ "score", GameScore.StillPlaying.Name },
					{ "links", new JArray(
						new JObject { { "rel", "game-for-player" }, { "href", "/game/feiradafruta/for/batima" } }
					)},
				},
				new JObject {
					{ "id", "robin" },
					{ "score", GameScore.StillPlaying.Name },
					{ "links", new JArray(
						new JObject { { "rel", "game-for-player" }, { "href", "/game/feiradafruta/for/robin" } }
					)},
				},
				new JObject {
					{ "id", "coringa" },
					{ "score", GameScore.Resigned.Name },
					{ "links", new JArray(
						new JObject { { "rel", "game-for-player" }, { "href", "/game/feiradafruta/for/coringa" } }
					)},
				}
			).ToString());
		}

		[Fact]
		void should_output_board_with_list_of_tiles()
		{
			var board = new BasicBoard(null, new BasicTileGraph(new BasicTileSet(new RName[] { "a1", "a2", "b2", }), null));
			var state = new BasicGameState(null, board);

			var model = new GameViewModelSerializer().From(state);

			model.ShouldBeOfType<JObject>();
			var tiles = model["tiles"].ShouldBeOfType<IEnumerable<JToken>>();
			tiles.ShouldContainSameAs(new []{
				new JObject {
					{ "name", "a1" },
					{ "position", new JObject { { "x", 0 }, { "y", 0 } } }
				},
				new JObject {
					{ "name", "a2" },
					{ "position", new JObject { { "x", 0 }, { "y", 1 } } }
				},
				new JObject {
					{ "name", "b2" },
					{ "position", new JObject { { "x", 1 }, { "y", 1 } } }
				}
			}, JToken.EqualityComparer);
		}

		[Fact]
		void should_output_board_with_list_of_pieces_on_board()
		{
			var pieces = new BasicPieceContainer {
				{ new BasicPiece("king"), "e1" },
				{ new BasicPiece("bishop"), "c1" },
				{ new BasicPiece("rook"), "a1" }
			};
			var board = new BasicBoard(pieces, null);
			var state = new BasicGameState(null, board);
			var model = new GameViewModelSerializer().From(state);

			model.ShouldBeOfType<JObject>();
			var tiles = model["pieces"].ShouldBeOfType<IEnumerable<JToken>>();
			tiles.ShouldContainSameAs(new []{
				new JObject {
					{ "id", "king" },
					{ "position", "e1" },
					{ "imagePos", new JObject { { "x", 7 }, { "y", 1 } } }
				},
				new JObject {
					{ "id", "bishop" },
					{ "position", "c1" },
					{ "imagePos", new JObject { { "x", 7 }, { "y", 1 } } }
				},
				new JObject {
					{ "id", "rook" },
					{ "position", "a1" },
					{ "imagePos", new JObject { { "x", 7 }, { "y", 1 } } }
				},
			}, JToken.EqualityComparer);
		}

		[Fact]
		void should_output_game_state_from_machine()
		{
			var state = new BasicGameState(null, null) { CurrentPlayerId = "ze_player" };
			var mockMachine = new Mock<IGameMachine>();
			mockMachine.Setup(m => m.State).Returns(state);

			var model = new GameViewModelSerializer().From(mockMachine.Object, "ze_player");

			model.ShouldBeOfType<JObject>();
			var stateModel = model["state"].ShouldBeOfType<JObject>();
			stateModel["currentPlayer"].ShouldBeOfType<JValue>().ToString().ShouldBe("ze_player");
		}

		[Fact]
		void should_output_moves_for_selected_player_from_machine()
		{
			RName playerOne = "one";
			RName playerTwo = "two";
			 
			var mockMachine = new Mock<IGameMachine>();
			mockMachine.Setup(m => m.State).Returns(new BasicGameState(null, null));
			mockMachine.Setup(m => m.GetMoveOptions()).Returns(new [] {
				MoveLink.From(new UriTemplate("/doit/better"), "/test/daft", null, new { playerid = playerOne, title = "Better" }),
				MoveLink.From(new UriTemplate("/doit/harder"), "/test/daft", null, new { playerid = playerOne, title = "Harder" }),
				MoveLink.From(new UriTemplate("/celebrate"), "/test/onemoretime", null, new { playerid = playerTwo }),
			});

			var model = new GameViewModelSerializer().From(mockMachine.Object, playerOne);

			model.ShouldBeOfType<JObject>();
			var moves = model["moves"].ShouldBeOfType<IEnumerable<JToken>>();
			moves.ShouldContainSameAs(new []{
				new JObject {
					{ "href", "/doit/better" },
					{ "rel", "/test/daft" },
					{ "data", new JObject {
						{ "playerid", "one" },
						{ "title", "Better" },
					}},
				},
				new JObject {
					{ "href", "/doit/harder" },
					{ "rel", "/test/daft" },
					{ "data", new JObject {
						{ "playerid", "one" },
						{ "title", "Harder" },
					}},
				},
			}, JToken.EqualityComparer);
		}
	}
}
