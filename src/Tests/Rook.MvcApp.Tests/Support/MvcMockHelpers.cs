﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Moq;

namespace Rook.MvcApp.Tests
{
	public static class MvcMockHelpers
	{
		public static HttpContextBase FakeHttpContext()
		{
			var context = new Mock<HttpContextBase>();
			var session = new Mock<HttpSessionStateBase>();
			var server = new Mock<HttpServerUtilityBase>();

			context.Setup(ctx => ctx.Request).Returns(FakeHttpRequest());
			context.Setup(ctx => ctx.Response).Returns(FakeHttpResponse());
			context.Setup(ctx => ctx.Session).Returns(session.Object);
			context.Setup(ctx => ctx.Server).Returns(server.Object);

			return context.Object;
		}

		public static HttpContextBase FakeHttpContext(string url)
		{
			HttpContextBase context = FakeHttpContext();
			context.Request.SetupRequestUrl(url);
			return context;
		} 

		public static T WithFakeContext<T>(this T controller) where T : Controller
		{
			var httpContext = FakeHttpContext();
			var context = new ControllerContext(new RequestContext(httpContext, new RouteData()), controller);
			controller.ControllerContext = context;
			return controller;
		}
		
		public static T WithFakeUser<T>(this T controller, IPrincipal user) where T : Controller
		{
			var context = Mock.Get(controller.ControllerContext.RequestContext.HttpContext);
			context.SetupProperty(ctx => ctx.User, user);

			return controller;
		}

		static HttpRequestBase FakeHttpRequest()
		{
			var request = new Mock<HttpRequestBase>();

			var headers = new NameValueCollection();
			request.SetupGet(r => r.Headers).Returns(headers);

			return request.Object;
		}
		
		static HttpResponseBase FakeHttpResponse()
		{
			var response = new Mock<HttpResponseBase>();

			response.SetupProperty(r => r.StatusCode, 200);
			response.SetupProperty(r => r.ContentType);
			response.SetupProperty(r => r.CacheControl);

			var cache = new Mock<HttpCachePolicyBase>();
			response.SetupGet(r => r.Cache).Returns(cache.Object);
			var cookies = new HttpCookieCollection();
			response.SetupGet(r => r.Cookies).Returns(cookies);
			var headers = new NameValueCollection();
			response.SetupGet(r => r.Headers).Returns(headers);

			return response.Object;
		}

		static string GetUrlFileName(string url)
		{
			if (url.Contains("?"))
				return url.Substring(0, url.IndexOf("?"));
			else
				return url;
		}

		static NameValueCollection GetQueryStringParameters(string url)
		{
			var parameters = new NameValueCollection();
			if (!url.Contains("?")) return parameters;

			string[] parts = url.Split("?".ToCharArray());
			string[] keys = parts[1].Split("&".ToCharArray());

			foreach (string key in keys)
			{
				string[] part = key.Split("=".ToCharArray());
				parameters.Add(part[0], part[1]);
			}

			return parameters;
		}

		public static void SetHttpMethodResult(this HttpRequestBase request, string httpMethod)
		{
			Mock.Get(request)
				.Setup(req => req.HttpMethod)
				.Returns(httpMethod);
		}

		public static void SetupRequestUrl(this HttpRequestBase request, string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			if (!url.StartsWith("~/"))
				throw new ArgumentException("Sorry, we expect a virtual url starting with \"~/\".");

			var mock = Mock.Get(request);

			mock.Setup(req => req.QueryString)
				.Returns(GetQueryStringParameters(url));
			mock.Setup(req => req.AppRelativeCurrentExecutionFilePath)
				.Returns(GetUrlFileName(url));
			mock.Setup(req => req.PathInfo)
				.Returns(string.Empty);
		}
	}
}
