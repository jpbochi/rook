﻿using System;
using System.Collections.Specialized;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Rook.MvcApp.RookAuthentication;

namespace Rook.MvcApp.Tests
{
	internal static class Fake
	{
		public static IPrincipal User(string userName = "Bruce Lee")
		{
			return new GenericPrincipal(new RookIdentity(RookIdentity.GenerateTicket(userName)), null);
		}
	}
}
