﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using JpLabs.Extensions;
using Moq;
using Rook.MvcApp.Controllers;
using Rook.MvcApp.Models;
using Xunit;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Web;

namespace Rook.MvcApp.Tests
{
	public class PlayControllerTests
	{
		const int gameId = 42;

		Mock<IGameViewModelSerializer> mockSerializer;
		Mock<IGameReferee> mockReferee;
		BasicGameMachine machine;
		PlayController controller;
		IGameState gameState;
		System.Security.Principal.IPrincipal user;

		public PlayControllerTests()
		{
			const string creator = "http://rook/maester";

			this.gameState = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");
			this.mockReferee = new Mock<IGameReferee>();
			this.machine = new BasicGameMachine(gameState, mockReferee.Object.ToEnumerable(), null);
			var entry = GameEntry.CreateNew(machine, creator, null, null);

			var mockRepository = new Mock<IGameRepository>();
			mockRepository.Setup(r => r.GetById(gameId)).Returns(entry);

			this.mockSerializer = new Mock<IGameViewModelSerializer>();

			this.user = Fake.User("TiaDoBatima");
			this.controller = new PlayController(mockRepository.Object, mockSerializer.Object).WithFakeContext().WithFakeUser(user);
		}

		[Fact]
		void should_list_moves()
		{
			var player = gameState.GetCurrentPlayer();
			player.SetController(user.GetOpenIdIdentity());
			var playerid = player.Id;

			this.mockReferee.Setup(r => r.GetMoves(It.IsAny<IGameState>())).Returns(new [] {
				MoveLink.From(new UriTemplate("/doit/better"), "/test/daft", null, new { playerid }),
				MoveLink.From(new UriTemplate("/doit/harder"), "/test/daft", null, new { playerid }),
				MoveLink.From(new UriTemplate("/celebrate"), "/test/onemoretime", null, new { playerid }),
			});

			var result = this.controller.Moves(gameId, playerid);

			var partialView = result.ShouldBeOfType<PartialViewResult>();
			var moves = partialView.Model.ShouldBeOfType<IEnumerable<IMoveLink>>();

			moves.Select(m => Tuple.Create(m.Href.ToString(), m.Rel)).ShouldContainSameAs(new [] {
				Tuple.Create("/doit/better", "/test/daft"),
				Tuple.Create("/doit/harder", "/test/daft"),
				Tuple.Create("/celebrate", "/test/onemoretime")
			});
		}

		[Fact]
		void should_return_serialized_game_machine_for_authorized_user()
		{
			var playingAs = "coringa";
			gameState.GetPlayerById("coringa").SetController(user.GetOpenIdIdentity());
			mockSerializer.Setup(s => s.From(machine, playingAs)).Returns(new JObject { { "game_for", "coringa" } });

			var result = this.controller.GameForPlayer(gameId, playingAs);
			result.ExecuteResult(controller.ControllerContext);

			controller.Response.StatusCode.ShouldBe((int)HttpStatusCode.OK);
			var jsonView = result.ShouldBeOfType<ContentResult>();
			jsonView.ContentType.ShouldStartWith("application/json");

			var json = JObject.Parse(jsonView.Content);
			json.ShouldBe(new JObject { { "game_for", "coringa" } });
		}

		[Fact]
		void should_return_404_when_game_not_found()
		{
			var result = this.controller.GameForPlayer(666, "batima");
			result.ExecuteResult(controller.ControllerContext);

			controller.Response.StatusCode.ShouldBe((int)HttpStatusCode.NotFound);
		}

		[Fact]
		void should_return_404_when_player_not_found()
		{
			var result = this.controller.GameForPlayer(gameId, "comissario");
			result.ExecuteResult(controller.ControllerContext);

			controller.Response.StatusCode.ShouldBe((int)HttpStatusCode.NotFound);
		}

		[Fact]
		void should_return_not_modified_when_ETag_mathed_current()
		{
			gameState.GetPlayerById("batima").SetController(user.GetOpenIdIdentity());
			gameState.Revise();
			controller.Request.Headers["If-None-Match"] = gameState.Version;

			var result = controller.GameForPlayer(gameId, "batima");
			result.ExecuteResult(controller.ControllerContext);

			controller.Response.StatusCode.ShouldBe((int)HttpStatusCode.NotModified);
		}

		[Fact]
		void should_enable_public_cache_and_send_current_ETag_of_game()
		{
			gameState.GetPlayerById("robin").SetController(user.GetOpenIdIdentity());
			gameState.Revise();
			var expectedETag = gameState.Version;
			var mockCache = Mock.Get(controller.Response.Cache);

			var result = controller.GameForPlayer(gameId, "robin");
			result.ExecuteResult(controller.ControllerContext);

			mockCache.Verify(c => c.SetCacheability(HttpCacheability.Public));
			mockCache.Verify(c => c.SetNoServerCaching());
			mockCache.Verify(c => c.SetETag(expectedETag));
		}

		[Fact]
		void should_return_Forbidden_if_user_does_not_control_player()
		{
			gameState.GetPlayerById("robin").SetController(Fake.User("Clotilde").GetOpenIdIdentity());
			
			var result = controller.GameForPlayer(gameId, "robin");
			result.ExecuteResult(controller.ControllerContext);

			controller.Response.StatusCode.ShouldBe((int)HttpStatusCode.Forbidden);
		}
	}
}
