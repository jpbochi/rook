﻿using System.Linq;
using Xunit.Extensions;

namespace Rook.Tests
{
	public class HexLinkSetTests
	{
		[Theory]
		//a1  b1  c1  d1
		//  a2  b2  c2  d2
		//a3  b3  c3  d3
		//  a4  b4  c4  d4
		[InlineData(HexModel.KeepLinesAlined, "b2", new [] {"b1-NW", "c1-NE", "a2-W", "c2-E", "b3-SW", "c3-SE"})]
		[InlineData(HexModel.KeepLinesAlined, "b3", new [] {"a2-NW", "b2-NE", "a3-W", "c3-E", "a4-SW", "b4-SE"})]
		[InlineData(HexModel.KeepLinesAlined, "c2", new [] {"c1-NW", "d1-NE", "b2-W", "d2-E", "c3-SW", "d3-SE"})]
		[InlineData(HexModel.KeepLinesAlined, "c3", new [] {"b2-NW", "c2-NE", "b3-W", "d3-E", "b4-SW", "c4-SE"})]
		//a1  c1  e1
		//  b1  d1
		//a2  c2  e2
		//  b2  d2
		//a3  c3  e3
		//  b3  d3
		//a4  c4  e4
		[InlineData(HexModel.KeepColumnsAligned, "b2", new [] {"a2-NW", "b1-N", "c2-NE", "a3-SW", "b3-S", "c3-SE"})]
		[InlineData(HexModel.KeepColumnsAligned, "b3", new [] {"a3-NW", "b2-N", "c3-NE", "a4-SW", "b4-S", "c4-SE"})]
		[InlineData(HexModel.KeepColumnsAligned, "c2", new [] {"b1-NW", "c1-N", "d1-NE", "b2-SW", "c3-S", "d2-SE"})]
		[InlineData(HexModel.KeepColumnsAligned, "c3", new [] {"b2-NW", "c2-N", "d2-NE", "b3-SW", "c4-S", "d3-SE"})]
		void should_yield_links_according_to_model(HexModel model, string origin, string[] expectedTargets)
		{
			var linkSet = new HexLinkSet(model);

			var links = linkSet.GetOutLinks(origin).ToArray();

			links.Select(link => string.Format("{0}-{1}", link.Target, link.Label)).ShouldContainSameAs(expectedTargets);
			links.ShouldEach(link => link.Origin.ShouldBe<RName>(origin));
		}
	}
}
