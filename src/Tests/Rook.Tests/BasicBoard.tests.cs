﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using Moq;
using Xunit;
using Xunit.Extensions;
using Rook.Tests.Extensions;

namespace Rook.Tests
{
	public class BasicBoardTests
	{
		private static IGameBoard GetNewBoard(ushort columns, ushort rows)
		{
			IGraphTileSet set = new RectangularGridTileSet(columns, rows);

			return new BasicBoard(
				new BasicPieceContainer(),
				new BasicTileGraph(set, null)
			);
		}
		
		private static IGameBoard GetNew3x2Board()
		{
			return GetNewBoard(3, 2);
		}

		private static IGamePiece AddNewPieceToPosition(IGameBoard board, RName position)
		{
			return board.AddPiece(new BasicPiece(), position);
		}

		private static IEnumerable<RName> GetValidPointsIn3x2Board()
		{
			return new RName[] { "a1", "b1", "c1", "a2", "b2", "c2" };
		}

		private static IEnumerable<RName> GetInvalidPointsIn3x2Board()
		{
			return new RName[] { "d1", "d2", "a3", "c3" };
		}
		
		public class contains_tiles
		{
			[Theory]
			[FunctionData("GetValidPointsIn3x2Board")]
			void should_contain_position(RName position)
			{
				Assert.True(GetNew3x2Board().Graph.Contains(position));
			}

			[Theory]
			[FunctionData("GetInvalidPointsIn3x2Board")]
			void should_not_contain_position(RName position)
			{
				Assert.False(GetNew3x2Board().Graph.Contains(position));
			}
		}

		public class contains_pieces
		{
			[Fact]
			void should_have_no_pieces_when_created()
			{
				var board = GetNew3x2Board();

				Assert.Empty(board.Pieces);
			}

			[Theory]
			[FunctionData("GetValidPointsIn3x2Board")]
			void should_yield_no_pieces_for_any_position_when_empty(RName position)
			{
				var board = GetNew3x2Board();

				Assert.Empty(board.PiecesAt(position));
			}
		}

		public class when_adding_pieces
		{
			[Theory]
			[FunctionData("GetValidPointsIn3x2Board")]
			void should_set_position_of_added_pieces(RName position)
			{
				var board = GetNew3x2Board();
				var mockPiece = new Mock<IGamePiece>();

				mockPiece.SetupGet(p => p.Position).Returns(position);
				var addedPiece = board.AddPiece(mockPiece.Object, position);

				Assert.Equal(addedPiece, mockPiece.Object);
				mockPiece.Verify( p => p.SetPosition(position), Times.Once());

				Assert.Single(board.Pieces);
			}

			[Fact]
			void should_add_more_than_one_piece_to_offboard_tile()
			{
				var board = GetNew3x2Board();
				var firstPiece = AddNewPieceToPosition(board, Tiles.OffBoard);
				var secondPiece = AddNewPieceToPosition(board, Tiles.OffBoard);

				board.PiecesAt(Tiles.OffBoard).ShouldContainSameAs(new [] { firstPiece, secondPiece });
			}

			[Fact]
			void should_yield_all_added_pieces()
			{
				var board = GetNew3x2Board();
				var positions = GetValidPointsIn3x2Board();
				var expectedPieces = positions.Select( pos => AddNewPieceToPosition(board, pos) ).ToArray();

				board.Pieces.ShouldContainSameAs(expectedPieces);
			}

			[Fact]
			void should_yield_pieces_added_for_each_position()
			{
				var board = GetNew3x2Board();
				var positions = GetValidPointsIn3x2Board();
				var addedPieces = positions.Select( pos => AddNewPieceToPosition(board, pos) ).ToArray();

				var groupedPieces = addedPieces.GroupBy( p => p.Position );

				foreach (var expectedPiecesAtPos in groupedPieces) {
					board.PiecesAt(expectedPiecesAtPos.Key).ShouldContainSameAs(expectedPiecesAtPos);
				}
			}
		}

		public class contains_regions
		{
			[Fact]
			void should_yield_tiles_at_main_board_region()
			{
				var board = GetNew3x2Board();

				var expectedTiles = new RName[] { "a1", "b1", "c1", "a2", "b2", "c2" };

				board.Region(Tiles.MainBoard).ShouldContainSameAs(expectedTiles);
			}

			[Theory]
			[InlineData("a1")]
			void should_yield_the_tile_when_querying_for_a_tile(string tileToQuery)
			{
				var board = GetNew3x2Board();
				
				board.Region(tileToQuery).ShouldContainSameAs(new RName[] { tileToQuery });
			}

			[Theory]
			[InlineData("h8")]
			[InlineData("_demilitarized_zone_")]
			[InlineData("_off_")]
			void should_yield_nothing_for_regions_not_in_the_board(string zoneToQuery)
			{
				var board = GetNew3x2Board();
				
				board.Region(zoneToQuery).ShouldBeEmpty();
			}

			[Fact]
			void should_yield_pieces_at_main_board_region()
			{
				var board = GetNew3x2Board();
			
				var positions = new RName[] { "a1", "b1", "c1", "a2", "b2", "c2" };
				var piecesInMainBoard = positions.Select( pos => AddNewPieceToPosition(board, pos) ).ToArray();
				var pieceOffBoard = AddNewPieceToPosition(board, Tiles.OffBoard);

				board.PiecesAt(Tiles.MainBoard).ShouldContainSameAs(piecesInMainBoard);
			}
		}

		public class when_moving_pieces
		{
			[Theory]
			[InlineData("a1", "b2")]
			[InlineData("c2", "_off_")]
			[InlineData("_off_", "c1")]
			void should_move_piece(string origin, string destination)
			{
				var board = GetNew3x2Board();

				var mockPiece = new Mock<IGamePiece>();
				mockPiece.SetupGet(p => p.Position).Returns(origin);
				board.AddPiece(mockPiece.Object, origin);

				var movedPiece = board.MovePiece(mockPiece.Object, destination);

				Assert.Equal(movedPiece, mockPiece.Object);
				mockPiece.Verify( p => p.SetPosition(destination), Times.Once());
			}
		}
	}
}
