﻿using JpLabs.Geometry;
using Xunit;
using Xunit.Extensions;

namespace Rook.Tests
{
	public class TilePosTests
	{
		[Theory]
		[InlineData("a1", 0, 0)]
		[InlineData("b2", 1, 1)]
		[InlineData("a2", 0, 1)]
		[InlineData("b1", 1, 0)]
		[InlineData("a11", 0, 10)]
		[InlineData("z1", 25, 0)]
		[InlineData("aa1", 26, 0)]
		[InlineData("ab1", 27, 0)]
		[InlineData("az1", 51, 0)]
		[InlineData("ba1", 52, 0)]
		[InlineData("zz1", 701, 0)]
		[InlineData("aaa1", 702, 0)]
		[InlineData("aab1", 703, 0)]
		[InlineData("aba1", 728, 0)]
		[InlineData("_a1", -1, 0)]
		[InlineData("a-1", 0, -1)]
		void should_convert_rname_into_point(string expectedName, int col, int row)
		{
			var point = new PointInt(col, row);

			Assert.Equal(expectedName, TilePos.ToTileName(point).FullName);
		}

		[Theory]
		[InlineData("a1", 0, 0)]
		[InlineData("b2", 1, 1)]
		[InlineData("a2", 0, 1)]
		[InlineData("b1", 1, 0)]
		[InlineData("a11", 0, 10)]
		[InlineData("z1", 25, 0)]
		[InlineData("aa1", 26, 0)]
		[InlineData("ab1", 27, 0)]
		[InlineData("az1", 51, 0)]
		[InlineData("ba1", 52, 0)]
		[InlineData("zz1", 701, 0)]
		[InlineData("aaa1", 702, 0)]
		[InlineData("aab1", 703, 0)]
		[InlineData("aba1", 728, 0)]
		[InlineData("_a1", -1, 0)]
		[InlineData("a-1", 0, -1)]
		void should_create_rname_for_point(string pointName, int col, int row)
		{
			var expectedPoint = new PointInt(col, row);

			Assert.Equal(expectedPoint, TilePos.FromName(pointName));
		}

		[Fact]
		void should_get_offboard_tile_name_for_offboard_point()
		{
			Assert.Equal(Tiles.OffBoard, Tiles.OffBoardPosition.ToTileName());
		}

		[Fact]
		void should_get_offboard_point_for_offboard_tile_name()
		{
			Assert.Equal(Tiles.OffBoardPosition, Tiles.OffBoard.ToPointInt());
		}
	}
}
