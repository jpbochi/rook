﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit.Extensions;
using System.Reflection;
using System.Collections;

namespace Rook.Tests.Extensions
{
	public class FunctionDataAttribute : DataAttribute
	{
		public const BindingFlags BindingFlagsToGetMethod
			= BindingFlags.Static
			| BindingFlags.FlattenHierarchy
			| BindingFlags.Public
			| BindingFlags.NonPublic;

		public Type SpecifiedType { get; private set; }
		public string MethodName { get; private set; }

		public FunctionDataAttribute(string methodName) : this(null, methodName) {}

		public FunctionDataAttribute(Type type, string methodName)
		{
			SpecifiedType = type;
			MethodName = methodName;
		}

		private MethodInfo FindMethod(MethodInfo methodUnderTest)
		{
			return FindMethod(SpecifiedType ?? methodUnderTest.DeclaringType);
		}

		private MethodInfo FindMethod(Type typeToSearch)
		{
			if (typeToSearch == null) return null;
			return typeToSearch.GetMethod(MethodName, BindingFlagsToGetMethod)
				?? FindMethod(typeToSearch.DeclaringType);
		}

		public override IEnumerable<object[]> GetData(MethodInfo methodUnderTest, Type[] parameterTypes)
		{
			var method = FindMethod(methodUnderTest);
			if (method == null) throw new MissingMethodException();

			var values = (IEnumerable)method.Invoke(null, null);
			return values
				.Cast<object>().Select(data =>
					(data is IEnumerable && !(data is string))
					? ((IEnumerable)data).Cast<object>().ToArray()
					: new object [] { data }
				);
		}
	}
}
