﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Rook.Tests.Extensions
{
	public enum AppEnvironment
	{
		Unknown,
		Debug,
		Release,
		Test
	}

	public static class AppEnvironmentExt
	{
		public static AppEnvironment Current
		{
			get {
				var env = ConfigurationManager.AppSettings["Environment"];

				switch (env) {
					case "Debug": return AppEnvironment.Debug;
					case "Release": return AppEnvironment.Release;
					case "Test": return AppEnvironment.Test;
					default: return AppEnvironment.Unknown;
				}
			}
		}
	}
}
