﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Extensions;

namespace Rook.Tests
{
	public class GameSerializerTests
	{
		[Theory]
		[InlineData("this is an invalid game state")]
		[InlineData("{ _type: 'this is an invalid game state' }")]
		[InlineData("<game>this is an invalid game state</game>")]
		void should_return_false_when_serialization_fails(string invalidGameState)
		{
			IGameState outGameState;
			GameSerializer.TryDeserialize<IGameState>(invalidGameState, out outGameState).ShouldBeFalse();
		}
	}
}
