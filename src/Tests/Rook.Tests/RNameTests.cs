﻿using JpLabs.Geometry;
using Xunit;
using Xunit.Extensions;
using System;

namespace Rook.Tests
{
	public class RNameTests
	{
		[Theory]
		[InlineData("h8", "", "h8")]
		[InlineData("_off_", "", "_off_")]
		[InlineData("short!_", "short", "_")]
		[InlineData("tic-tac-toe!X", "tic-tac-toe", "X")]
		[InlineData("chess!pawn.white-h", "chess", "pawn.white-h")]
		[InlineData("GO_chinese!stone.black-123", "GO_chinese", "stone.black-123")]
		[InlineData("stratego!flag_blue", "stratego", "flag_blue")]
		[InlineData("rook.amazons!arrow-10ff", "rook.amazons", "arrow-10ff")]
		void should_parse_expanded_rname(string expandedName, string ns, string name)
		{
			var rname = RName.Get(expandedName);
			Tuple.Create(rname.Namespace, rname.Name).ShouldBe(Tuple.Create(ns, name));

			RName.TryGet(expandedName, out rname).ShouldBeTrue();
			Tuple.Create(rname.Namespace, rname.Name).ShouldBe(Tuple.Create(ns, name));
		}

		[Theory]
		[InlineData("_board_", null, "_board_")]
		[InlineData("tic-tac-toe!O", "tic-tac-toe", "O")]
		[InlineData("chess!rook.black-k", "chess", "rook.black-k")]
		void should_generate_expanded_name(string expandedName, string ns, string name)
		{
			var rname = RName.Get(name, ns);

			rname.FullName.ShouldBe(expandedName);
		}

		[Theory]
		[InlineData("")]
		[InlineData("-dash")]
		[InlineData(".dot")]
		[InlineData("0number")]
		[InlineData("!bang")]
		[InlineData("!!two-bangs")]
		[InlineData("-dash!fail")]
		[InlineData(".dot!fail")]
		[InlineData("0number!fail")]
		[InlineData("no-name!")]
		[InlineData("specials:=+.,;^~@#$%&'\"{}[]\\/?")]
		void should_fail_to_parse_expanded_rname(string expandedName)
		{
			Assert.Throws<ArgumentException>(() => RName.Get(expandedName));

			RName name;
			RName.TryGet(expandedName, out name).ShouldBeFalse();
		}

		[Theory]
		[InlineData("")]
		[InlineData("-dash")]
		[InlineData(".dot")]
		[InlineData("0number")]
		[InlineData("!shot")]
		void should_reject_local_name(string localName)
		{
			Assert.Throws<ArgumentException>(() => RName.Get(localName, null));
		}

		[Fact]
		void should_reject_null_name_or_expanded_name()
		{
			Assert.Throws<ArgumentNullException>(() => RName.Get(null));
			Assert.Throws<ArgumentNullException>(() => RName.Get(null, null));
		}

		[Theory]
		[InlineData("-dash")]
		[InlineData(".dot")]
		[InlineData("0number")]
		[InlineData("!shot")]
		void should_reject_namespace(string ns)
		{
			Assert.Throws<ArgumentException>(() => RName.Get("_test_", ns));
		}
	}
}
