﻿using Moq;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Commands
{
	public class CapturePieceCommandTests
	{
		[Fact]
		void should_be_considered_equal()
		{
			var piece = new BasicPiece("trader");

			var cmd1 = new CapturePieceCommand(piece);
			var cmd2 = new CapturePieceCommand(piece);

			RookTestHelper.AssertLinkEquality(cmd1.Link, cmd2.Link);
		}

		[Fact]
		void should_move_captured_piece_off_board()
		{
			var mockBoard = new Mock<IGameBoard>();

			var state = new BasicGameState(null, mockBoard.Object);

			var piece = new Mock<IGamePiece>().Object;

			var cmd = new CapturePieceCommand(piece);
			cmd.Execute(new MoveExecutionContext(state, null, null));

			mockBoard.Verify( b => b.MovePiece(piece, Tiles.OffBoard) );
		}
	}
}
