﻿using System.Linq;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Commands
{
	public class CycleTurnCommandTests
	{
		[Fact]
		void should_be_considered_equal()
		{
			var cmd1 = new CycleTurnCommand();
			var cmd2 = new CycleTurnCommand();

			RookTestHelper.AssertLinkEquality(cmd1.Link, cmd2.Link);
		}

		[Fact]
		void should_cycle_from_first_to_second()
		{
			var players = RookTestHelper.CreatePlayers("one", "other").ToArray();
			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = "one";

			var cmd = new CycleTurnCommand();
			cmd.Execute(new MoveExecutionContext(state, null, null));

			Assert.Equal((RName)"other", state.CurrentPlayerId);
		}

		[Fact]
		void should_cycle_from_third_to_fourth()
		{
			var players = RookTestHelper.CreatePlayers("one", "two", "three", "four").ToArray();
			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = "three";

			var cmd = new CycleTurnCommand();
			cmd.Execute(new MoveExecutionContext(state, null, null));

			Assert.Equal((RName)"four", state.CurrentPlayerId);
		}

		[Fact]
		void should_cycle_from_last_to_first()
		{
			var players = RookTestHelper.CreatePlayers("first", "last").ToArray();
			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = "last";

			var cmd = new CycleTurnCommand();
			cmd.Execute(new MoveExecutionContext(state, null, null));

			Assert.Equal((RName)"first", state.CurrentPlayerId);
		}

		[Fact]
		void should_cycle_over_resigned_players()
		{
			var players = RookTestHelper.CreatePlayers("playing", "looser", "next").ToArray();
			players[1].Score = GameScore.Resigned;

			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = "playing";

			var cmd = new CycleTurnCommand();
			cmd.Execute(new MoveExecutionContext(state, null, null));

			Assert.Equal((RName)"next", state.CurrentPlayerId);
		}
	}
}
