﻿using Moq;
using Rook.Augmentation;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Commands
{
	public class SetPlayerPropertyCommandTests
	{
		[Fact]
		void should_set_value_on_player()
		{
			var player = new BasicPlayer("the_player");
			var property = AugProperty.Create("Test", typeof(string), typeof(SetPlayerPropertyCommandTests), "default");
			var value = "expected";
			var state = new BasicGameState(new [] { player }, null);

			var cmd = new SetPlayerPropertyCommand(player.Id, property, value);
			cmd.Execute(new MoveExecutionContext(state, null, null));

			player.GetValue(property).ShouldBe(value);
		}
	}
}
