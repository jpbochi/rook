﻿using Moq;
using Rook.Augmentation;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Commands
{
	public class ResetGamePropertyCommandTests
	{
		[Fact]
		void should_reset_value_on_game_state()
		{
			var property = AugProperty.Create("Test", typeof(object), typeof(ResetGamePropertyCommandTests));
			var value = new object();
			var cmd = new ResetGamePropertyCommand(property);

			var mockState = new Mock<IGameState>();
			cmd.Execute(new MoveExecutionContext(mockState.Object, null, null));

			mockState.Verify( t => t.ClearValue(property) );
		}
	}
}
