﻿using Moq;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Commmands
{
	public class AddPieceCommandTests
	{
		[Fact]
		void should_be_considered_equal()
		{
			var piece = "_the-piece_";
			var pos = Tiles.OffBoard;

			var cmd1 = new AddPieceCommand(piece, pos);
			var cmd2 = new AddPieceCommand(piece, pos);

			RookTestHelper.AssertLinkEquality(cmd1.Link, cmd2.Link);
		}

		[Fact]
		void should_create_piece_from_class_and_add_it_to_the_board()
		{
			//TODO: The setup of this test is too complex. Wrap it somewhere else. Also, the IPieceClassDomain part of the board has to be tested.

			var piece = new Mock<IGamePiece>().Object;

			var mockPieceClass = new Mock<IGamePiece>();
			var classSymbol = RName.Get("pieceClassToBeAdded", "test");
			mockPieceClass.Setup( p => p.New() ).Returns(piece);

			var mockContainer = new Mock<IPieceContainer>();
			mockContainer.Setup(b => b.ById(classSymbol)).Returns(mockPieceClass.Object);

			var mockBoard = new Mock<IGameBoard>();
			mockBoard.SetupGet(b => b.Pieces).Returns(mockContainer.Object);

			var state = new BasicGameState(null, mockBoard.Object);
			RName position = "h8";

			var cmd = new AddPieceCommand(classSymbol, position);
			cmd.Execute(new MoveExecutionContext(state, null, null));

			mockPieceClass.Verify( p => p.New() );
			mockBoard.Verify( b => b.AddPiece(piece, position) );
		}
	}
}
