﻿using Moq;
using Rook.Augmentation;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Commands
{
	public class SetGamePropertyCommandTests
	{
		[Fact]
		void should_be_considered_equal()
		{
			var prop = AugProperty.Create("null", typeof(object), typeof(SetGamePropertyCommandTests));
			var value = new object();

			var cmd1 = new SetGamePropertyCommand(prop, value);
			var cmd2 = new SetGamePropertyCommand(prop, value);

			RookTestHelper.AssertLinkEquality(cmd1.Link, cmd2.Link);
		}

		[Fact]
		void should_set_value_on_game_state()
		{
			var property = AugProperty.Create("Test", typeof(object), typeof(SetGamePropertyCommandTests));
			var value = new object();
			var cmd = new SetGamePropertyCommand(property, value);

			var mockState = new Mock<IGameState>();
			cmd.Execute(new MoveExecutionContext(mockState.Object, null, null));

			mockState.Verify( t => t.SetValue(property, value) );
		}
	}
}
