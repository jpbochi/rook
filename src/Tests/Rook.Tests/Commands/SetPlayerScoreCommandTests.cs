﻿using System.Linq;
using Moq;
using Rook.Augmentation;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Commands
{
	public class SetPlayerScoreCommandTests
	{
		[Fact]
		void should_set_value_on_game_state()
		{
			var state = RookTestHelper.CreateGameWithPlayers(3);
			var player = state.Players.Last();
			var cmd = new SetPlayerScoreCommand(player.Id, GameScore.Won);

			cmd.Execute(new MoveExecutionContext(state, null, null));

			player.Score.ShouldBe(GameScore.Won);
		}
	}
}
