﻿using Moq;
using Rook.Commands;
using Xunit;

namespace Rook.Tests.Comands
{
	public class MovePieceCommandTests
	{
		[Fact]
		void should_be_considered_equal()
		{
			var piece = new BasicPiece("rook");
			var pos = Tiles.OffBoard;

			var cmd1 = new MovePieceCommand(piece, pos);
			var cmd2 = new MovePieceCommand(piece, pos);

			RookTestHelper.AssertLinkEquality(cmd1.Link, cmd2.Link);
		}

		[Fact]
		void should_move_piece_on_board()
		{
			var mockBoard = new Mock<IGameBoard>();

			var state = new BasicGameState(null, mockBoard.Object);

			RName position = "c5";
			var piece = new Mock<IGamePiece>().Object;

			var cmd = new MovePieceCommand(piece, position);
			cmd.Execute(new MoveExecutionContext(state, null, null));

			mockBoard.Verify( b => b.MovePiece(piece, position) );
		}
	}
}
