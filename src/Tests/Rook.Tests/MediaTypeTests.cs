﻿using Xunit;

namespace Rook.Tests
{
	public class MediaTypeTests
	{
		[Fact]
		void should_parse()
		{
			MediaType.Parse("image/gif").Type.ShouldBe(MediaType.Image);
			MediaType.Parse("image/gif").SubType.ShouldBe("gif");
			
			MediaType.Parse("application/javascript").Type.ShouldBe(MediaType.Application);
			MediaType.Parse("application/javascript").SubType.ShouldBe("javascript");

			MediaType.Parse("ImAgE/pNg").Type.ShouldBe(MediaType.Image);
			MediaType.Parse("ImAgE/pNg").SubType.ShouldBe("png");
		}
	}
}
