﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using JpLabs.Symbols;
using Rook.Augmentation;
using Xunit;

namespace Rook.Tests.Augmentation
{
	internal class TestProperties
	{
		public static AugProperty StringProperty
			= AugProperty.Create("String", typeof(String), typeof(TestProperties));

		public static AugProperty IntProperty
			= AugProperty.Create("Int", typeof(int), typeof(TestProperties));

		public static AugProperty NullableIntProperty
			= AugProperty.Create("NullableInt", typeof(int?), typeof(TestProperties));

		public static AugProperty EnumProperty
			= AugProperty.Create("Enum", typeof(TypeCode), typeof(TestProperties));

		public static AugProperty DateTimeProperty
			= AugProperty.Create("DateTime", typeof(DateTime), typeof(TestProperties));

		public static AugProperty SymbolProperty
			= AugProperty.Create("Symbol", typeof(Symbol), typeof(TestProperties));

		public static AugProperty SubObjectProperty
			= AugProperty.Create("SubObject", typeof(object), typeof(TestProperties));

		public static IEnumerable<AugProperty> AllProperties()
		{
			yield return TestProperties.StringProperty;
			yield return TestProperties.IntProperty;
			yield return TestProperties.NullableIntProperty;
			yield return TestProperties.EnumProperty;
			yield return TestProperties.DateTimeProperty;
			yield return TestProperties.SymbolProperty;
			yield return TestProperties.SubObjectProperty;
		}
	}

	public class AugProperty_Tests
	{
		[Fact]
		void SerializeToXName()
		{
			foreach (var prop in TestProperties.AllProperties()) {
				var expected = XName.Get(prop.Name, prop.DeclaringTypeName);
				var actual = prop.ToXName();

				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		void DeserializeFromXElement()
		{
			foreach (var prop in TestProperties.AllProperties()) {
				var deserializedProp = AugProperty.FromXName(prop.ToXName());

				Assert.Equal(prop, deserializedProp);
			}
		}
	}
}
