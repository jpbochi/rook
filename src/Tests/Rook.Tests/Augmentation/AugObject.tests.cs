﻿using System;
using System.Collections;
using System.Linq;
using System.Xml.Linq;
using JpLabs.Symbols;
using Rook.Augmentation;
using Xunit;

namespace Rook.Tests.Augmentation
{
	public class AugObject_Tests
	{
		class TestAugObject : AugObject, IStructuralEquatable
		{
			public TestAugObject() {}

			public TestAugObject(XElement x) : base(x) {}

			public override bool Equals(object obj)
			{
				return StructuralComparisons.StructuralEqualityComparer.Equals(this, obj);
			}

			public override int GetHashCode()
			{ throw new NotImplementedException(); }

			int IStructuralEquatable.GetHashCode(IEqualityComparer comparer)
			{ throw new NotImplementedException(); }

			public bool Equals(object objOther, IEqualityComparer comparer)
			{
				var other = objOther as TestAugObject;
				if (other == null) return false;

				var otherValues = other.GetLocalValues().ToList();
				var thisValues = this.GetLocalValues().ToList();

				if (thisValues.Count != otherValues.Count) return false;

				return thisValues.All( kp => comparer.Equals(kp.Value.GetValue(this), other.GetValue(kp.Key)));
			}
		}

		AugObject CreateAugObject()
		{
			return new TestAugObject();
		}

		AugObject GetFullAugObject()
		{
			var obj = CreateAugObject();

			obj.SetValue(TestProperties.StringProperty, "The quick brown fox jumps over the lazy dog");
			obj.SetValue(TestProperties.IntProperty, 42);
			obj.SetValue(TestProperties.NullableIntProperty, -128);
			obj.SetValue(TestProperties.EnumProperty, TypeCode.DBNull);
			obj.SetValue(TestProperties.DateTimeProperty, new DateTime(1492, 10, 12));
			obj.SetValue(TestProperties.SymbolProperty, Symbol.Declare(typeof(SymbolEnum), "Foo"));

			return obj;
		}

		AugObject GetDeepAugObject()
		{
			var obj = CreateAugObject();
			var subObj = GetFullAugObject();

			obj.SetValue(TestProperties.SubObjectProperty, subObj);

			return obj;
		}

		[Fact]
		void SerializeToXElement()
		{
			var obj = GetFullAugObject();

			var expected = new XElement(
				obj.DefaultXName,
				obj.GetLocalValues().Select(kvp => new XAttribute(kvp.Key.ToXName(), kvp.Value))
			);
			var actual = obj.ToXml();

			Assert.Equal(expected, actual, new XNodeEqualityComparer());
		}

		[Fact]
		void DeserializeFromXElement()
		{
			var obj = GetFullAugObject();

			var deserializedObj = new TestAugObject(obj.ToXml());

			Assert.Equal(obj, deserializedObj);
		}

		[Fact(Skip="feature not implemented yet")]
		void DeepObjectIsReserializable()
		{
			var original = GetDeepAugObject();

			var serialized = original.ToXml();
			var deserialized = new TestAugObject(serialized);
			var reserialized = deserialized.ToXml();

			Assert.Equal(serialized, reserialized, new XNodeEqualityComparer());
		}
	}
}
