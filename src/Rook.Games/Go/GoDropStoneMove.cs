﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rook.Games.Go
{
	internal class GoDropStoneMove : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return GoDropStoneMove.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new GoDropStoneMove(
					match.BoundVariables["playerid"],
					match.BoundVariables["piece"],
					match.BoundVariables["position"]
				);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/drop/{playerid}/{piece}/{position}");
		internal const string Relation = RookRelations.Moves.DropPiece;

		public RName PlayerId { get; private set; }
		public RName Piece { get; private set; }
		public RName Position { get; private set; }

		private WorkBoard boardAfterMove;
		private ICollection<RName> killPositions;

		public GoDropStoneMove(RName playerId, RName pieceClass, RName position)
		{
			this.PlayerId = playerId;
			this.Piece = pieceClass;
			this.Position = position;
		}

		string IGameMove.RequiredRole { get { return PlayerId; } }

		public IMoveLink Link
		{
			get {
				return MoveLink.From(Template, Relation, new { PlayerId, Piece, Position }, new { PlayerId, Destination = Position });
			}
		}

		private void PrepareCache(IGameBoard board)
		{
			var workBoard = GetWorkBoardAfterDrop(board, Position, Piece);

			this.killPositions = GetSurroundedTiles(board, workBoard, Piece).ToArray();

			foreach (var k in killPositions) workBoard.Remove(k);

			this.boardAfterMove = workBoard;
		}

		private static WorkBoard GetWorkBoardAfterDrop(IGameBoard board, RName dropPosition, RName pieceClass)
		{
			var workBoard = WorkBoard.Create(board);
			workBoard.Add(dropPosition, pieceClass.GetHashCode());
			return workBoard;
		}

		private static IEnumerable<RName> GetSurroundedTiles(IGameBoard board, WorkBoard workBoard, RName attackerPieceClass)
		{
			int attackerHash = attackerPieceClass.GetHashCode();
			var groups = board.GroupBy(
				t => { var flag = workBoard.Get(t); return flag == null || flag == attackerHash; }
				//t => workBoard.Get(t) == attackerHash //also works, but much much slower (go figure)
			);

			return groups
				.Where(g => g.All(t => workBoard.Get(t) != null))
				.SelectMany(g => g.Where(t => workBoard.Get(t) != attackerHash));
		}

		public WorkBoard GetWorkBoardAfterMove(IGameBoard board)
		{
			if (boardAfterMove == null) PrepareCache(board);
			return boardAfterMove;
		}

		public IEnumerable<RName> GetKills(IGameBoard board)
		{
			if (killPositions == null) PrepareCache(board);
			return killPositions;
		}

		public bool IsKo(IGameState state)
		{
			var previousBoard = state.GetPreviousBoard();
			if (previousBoard == null) return false;

			var workBoard = GetWorkBoardAfterMove(state.Board);

			//return StructuralComparisons.StructuralEqualityComparer.Equals(workBoard, previousBoard);

			//if (StructuralComparisons.StructuralEqualityComparer.Equals(workBoard, previousBoard)) System.Diagnostics.Debugger.Break();

			return previousBoard == workBoard.GetHashCode();
		}

		public bool IsSuicide(IGameBoard board, RName otherPlayerPiece)
		{
			return GetSuicides(board, otherPlayerPiece).Any();
		}

		private IEnumerable<RName> GetSuicides(IGameBoard board, RName otherPlayerPiece)
		{
			var workBoard = GetWorkBoardAfterMove(board);

			return GetSurroundedTiles(board, workBoard, otherPlayerPiece);
		}

		public void Execute(IMoveExecutionContext context)
		{
			var board = context.State.Board;
			var previousBoard = WorkBoard.Create(board);

			var killerTiles = GetKills(board);
			var killedStones = killerTiles.SelectMany(k => board.PiecesAt(k));
			var cmds = killedStones.Select(s => MoveLink.BuildUri(CommandTemplates.CapturePiece, new { Piece = s.Id })).ToList();

			var previousBoardHash = previousBoard.GetHashCode();

			cmds.Add(MoveLink.BuildUri(CommandTemplates.AddPiece, new { Piece, Position }));
			cmds.Add(MoveLink.BuildUri(CommandTemplates.SetGameProperty, new { Property = GoProperties.PreviousMoveProperty, Value = Link.Href }));
			cmds.Add(MoveLink.BuildUri(CommandTemplates.SetGameProperty, new { Property = GoProperties.PreviousBoardProperty, Value = previousBoardHash }));
			cmds.Add(MoveLink.BuildUri(CommandTemplates.CycleTurn));
			cmds.Add(MoveLink.BuildUri(GoScoreCommand.Template));

			foreach (var cmd in cmds) context.ExecuteMove(cmd);
		}

		[Obsolete("", true)]
		public IEnumerable<IGameMove> Resolve(IGameState state)
		{
			throw new NotSupportedException();
			///// TODO: yield CapturePieceCommands
			///// Problem description:
			///// . In order to know which pieces to capture, I need to add the piece being played in this move;
			///// . I can't add the piece to the current board, because move resolution must not have side-effects;
			///// . Cloning the board makes sense (yet, it's not implemented);
			///// . The captured pieces must be on the original board, not the clone;
			///// . 1) Should the cloned pieces keep a reference to the original pieces?
			///// . 2) Or should I rely on the rule that no two pieces can stand at the same position?
			///// answer= both! I'll create a light-weight clone!

			//var board = state.Board;
			//var previousBoard = WorkBoard.Create(board);

			//var kills = GetKills(board);
			//var killedStoned = kills.Select(k => board.GetFirstPieceAt(k));
			//foreach (var s in killedStoned) yield return new CapturePieceCommand(s);

			//yield return new AddPieceCommand(PieceClass, Position);
			//yield return new SetGamePropertyCommand(GoProperties.PreviousMoveTypeProperty, MoveType.DropPiece);
			//yield return new SetGamePropertyCommand(GoProperties.PreviousBoardProperty, previousBoard);
			//yield return new CycleTurnCommand();
			//yield return new GoScoreCommand();
		}
	}
}
