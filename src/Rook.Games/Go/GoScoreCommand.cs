﻿using System.Collections;
using System.Linq;
using JpLabs.Extensions;
using System;

namespace Rook.Games.Go
{
	internal class GoScoreCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return GoScoreCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new GoScoreCommand();
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/go/score");
		internal const string Relation = RookRelations.Commands.UpdateScore;

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public IMoveLink Link
		{
			get { return MoveLink.From(Template, Relation, null); }
		}

		public void Execute(IMoveExecutionContext context)
		{
			CalculateScore(context);

			var state = context.State;
			if (state.IsOver()) {
				var black = state.Players.First().GetScore();
				var white = state.Players.Last().GetScore();

				var cmds
					= (black > white) ? state.SetWinnerCommands(ClassicPlayer.Black)
					: (white > black) ? state.SetWinnerCommands(ClassicPlayer.White)
					: state.SetDrawCommands();

				foreach (var cmd in cmds) context.ExecuteMove(cmd);
			}
		}

		private void CalculateScore(IMoveExecutionContext context)
		{
			CountTerritories(context);

			var state = context.State;
			foreach (var player in state.Players) {
				var playerStones = state.Board.PiecesAt(Tiles.MainBoard).Where(s => s.GetBanner() == player.Id).Count();
				var area = player.GetTerritory() + playerStones;

				context.ExecuteMove(
					MoveLink.BuildUri(CommandTemplates.SetPlayerProperty, new {
						player = player.Id,
						property = GoProperties.AreaProperty,
						value = area
					})
				);
			}
		}

		private void CountTerritories(IMoveExecutionContext context)
		{
			var state = context.State;
			var board = state.Board;
			var groups = board.GroupBy(tile => board.PiecesAt(tile).Any());

			var territories = state.Players.ToDictionary(p => p.Id, p => 0);
			foreach (var group in groups) {
				var piecesAtGroup = group.SelectMany(t => board.PiecesAt(t));
				var owners = piecesAtGroup
					.Select(p => p.GetBanner())
					.Where(f => f != null)
					.Distinct().ToArray();
				
				if (owners.Length == 1) territories[owners[0]] += group.Where(t => board.PiecesAt(t).IsEmpty()).Count();
			}

			foreach (var playerTerritory in territories) {
				context.ExecuteMove(
					MoveLink.BuildUri(CommandTemplates.SetPlayerProperty, new {
						player = playerTerritory.Key,
						property = GoProperties.TerritoryProperty,
						value = playerTerritory.Value
					})
				);
			}
		}
	}
}
