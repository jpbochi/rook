﻿using System.Collections.Generic;
using System.Linq;

namespace Rook.Games.Go
{
	internal class WorkBoard
	{
		private readonly IDictionary<RName,int> board;

		private WorkBoard(IGameBoard board)
		{
			this.board = GetWorkBoard(board);
		}

		public static WorkBoard Create(IGameBoard board)
		{
			return new WorkBoard(board); 
		}

		private static IDictionary<RName,int> GetWorkBoard(IGameBoard board)
		{
			return board.PiecesAt(Tiles.MainBoard).ToDictionary(p => p.Position, p => p.Class.GetHashCode());
		}

		public void Add(RName position, int flag)
		{
			board[position] = flag;
		}

		public bool Remove(RName position)
		{
			return board.Remove(position);
		}

		public int? Get(RName position)
		{
			int flag;
			return board.TryGetValue(position, out flag) ? (int?)flag : null;
		}

		public override int GetHashCode()
		{
			return board.Aggregate(0, (acc,kvp) => acc ^ kvp.Value ^ kvp.Key.GetHashCode());
		}
	}
}
