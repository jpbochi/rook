﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;

namespace Rook.Games.Go
{
	[GameInitializer(typeof(GoGameModule))]
	public class GoGameModuleInitializer : IGameModuleInitializer
	{
		[Range(3, 19)]
		[DefaultValue(9)]
		[DisplayName("Board Size")]
		public int BoardSize { get; set; }

		public void Initialize(INinjectModule module)
		{
			var game = module as GoGameModule;
			if (game == null) return;

			game.BoardSize = new PointInt(BoardSize, BoardSize);
		}
	}

	[GameModule("Go", WikiUrl="http://en.wikipedia.org/wiki/Rules_of_Go")]
	public class GoGameModule : NinjectModule
	{
		public static readonly Symbol BoardSizeOptionName = Symbol.Declare<GoGameModule>("BoardSize");

		public const int DefaultBoardSize = 9; // classic Go board sizes are: 9, 13, 19

		public PointInt BoardSize { get; set; }

		public override void Load()
		{
			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<IGameBoard>().To<BasicBoard>();
			Bind<IPieceContainer>().To<BasicPieceContainer>().OnActivation( InitializePieceContainer );

			Bind<PointInt>().ToMethod(ctx => GetBoardSizeOrDefault()).Named(BoardSizeOptionName);
			Bind<IGameTileGraph>().ToMethod( CreateInitialTileGraph );

			Bind<IGameMove>().To<GoScoreCommand>().Named(CommandType.UpdateScore);

			Bind<IGameReferee>().To<GoReferee>();

			Bind<IResourceInfo>().ToConstant(Resources.GoBackGround);
			Bind<IResourceInfo>().ToConstant(Resources.ChessPieceSet);
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return new [] {
				new BasicPlayer(ClassicPlayer.Black),
				new BasicPlayer(ClassicPlayer.White)
			};
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = ClassicPlayer.Black;
		}

		private PointInt GetBoardSizeOrDefault()
		{
			var size = BoardSize;
			return (size.Dimensions == 2) ? size : new PointInt(DefaultBoardSize, DefaultBoardSize);
		}

		private void InitializePieceContainer(IPieceContainer container)
		{
			var blackStone = new BasicPiece(GoPieces.BlackStone).SetBanner(ClassicPlayer.Black);
			var whiteStone = new BasicPiece(GoPieces.WhiteStone).SetBanner(ClassicPlayer.White);

			container.Add(blackStone);
			container.Add(whiteStone);
		}

		private static IGameTileGraph CreateInitialTileGraph(IContext context)
		{
			var size = context.Kernel.Get<PointInt>(BoardSizeOptionName.FullName);
			return new BasicTileGraph(
				new RectangularGridTileSet((ushort)size.X, (ushort)size.Y),
				VectorBasedLinkSet.CreateCardinal()
			);
		}
	}
}
