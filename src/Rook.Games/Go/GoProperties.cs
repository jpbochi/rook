﻿using System;
using JpLabs.Symbols;
using Rook.Augmentation;
using System.Collections.Generic;

namespace Rook.Games.Go
{
	public static class GoPieces
	{
		public const string Namespace = "go_stone";

		public static readonly RName BlackStone = RName.Get("black", Namespace);
		public static readonly RName WhiteStone = RName.Get("white", Namespace);

		public static readonly IDictionary<RName,RName> OfPlayer = new Dictionary<RName,RName> {
			{ ClassicPlayer.Black, BlackStone },
			{ ClassicPlayer.White, WhiteStone }
		};
	}

	public static class GoProperties
	{
		public static AugProperty TerritoryProperty =
			AugProperty.Create("Territory", typeof(int), typeof(GoProperties), 0);

		public static AugProperty AreaProperty =
			AugProperty.Create("Area", typeof(int), typeof(GoProperties), 0);

		public static AugProperty PreviousMoveProperty =
			AugProperty.Create("PreviousMove", typeof(Uri), typeof(GoProperties), null);
		
		public static AugProperty PreviousBoardProperty =
			AugProperty.Create("PreviousBoard", typeof(int?), typeof(GoProperties));

		public static int GetTerritory(this IGamePlayer player)
		{
			return player.GetValue<int>(TerritoryProperty);
		}

		public static int GetArea(this IGamePlayer player)
		{
			return player.GetValue<int>(AreaProperty);
		}

		public static int GetScore(this IGamePlayer player)
		{
			/// http://en.wikipedia.org/wiki/Rules_of_Go
			/// In stone or area scoring (including Chinese rules), a player's score is determined by
			/// the number of stones that player has on the board plus the empty area surrounded by that player's stones.
			return player.GetArea();
		}

		internal static Uri GetPreviousMove(this IGameState state)
		{
			return state.GetValue<Uri>(PreviousMoveProperty);
		}

		internal static int? GetPreviousBoard(this IGameState state)
		{
			return state.GetValue<int?>(PreviousBoardProperty);
		}
	}
}
