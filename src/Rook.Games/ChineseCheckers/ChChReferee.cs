﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using System;
using JpLabs.Symbols;

namespace Rook.Games.ChineseCheckers
{
	internal class ChChReferee : BaseReferee, IGameReferee
	{
		private static readonly MoveBuilderTable moveTable = new MoveBuilderTable(new [] {
			CheckerMove.Builder,
			ChineseCheckersScoreCommand.Builder,
		});

		public IEnumerable<IMoveLink> GetMoves(IGameState state)
		{
			if (state.IsOver()) return Enumerable.Empty<MoveLink>();

			var currentPlayerId = state.CurrentPlayerId;
			var checkers = state.Board.PiecesAt(Tiles.MainBoard).Where( p => p.GetBanner() == currentPlayerId );

			return checkers.SelectMany(checker => GetCheckerMoves(state, checker)).Select(m => m.Link);
		}

		public override IGameMove FindMove(Uri moveUri, IGameState state)
		{
			return moveTable.Match(moveUri, state);
		}

		private IEnumerable<IGameMove> GetCheckerMoves(IGameState state, IGamePiece checker)
		{
			return SingleWalkDestinations(checker.Position, state.Board)
			.Concat(HopDestinations(checker.Position, state.Board))
			.Select(destination => new CheckerMove(checker.GetBanner(), checker, destination));
		}

		private IEnumerable<RName> SingleWalkDestinations(RName origin, IGameBoard board)
		{
			return board.Graph
				.GetOutLinks(origin)
				.Where(link => CanWalkTo(board.PiecesAt(link.Target)))
				.Select(link => link.Target);
		}

		private static bool CanWalkTo(IEnumerable<IGamePiece> entries)
		{
			return entries.IsEmpty();
		}

		private IEnumerable<RName> HopDestinations(RName origin, IGameBoard board)
		{
			return HopDestinations(origin, board, new HashSet<RName>{ origin });
		}

		private IEnumerable<RName> HopDestinations(RName origin, IGameBoard board, ISet<RName> notToRepeat)
		{
			foreach(var link in board.Graph.GetOutLinks(origin)) {
				if (CanHopOver(board.PiecesAt(link.Target))) {
					var next = board.Graph.GetLabeledOutLink(link.Target, link.Label);

					if (next != null && CanWalkTo(board.PiecesAt(next.Target)) && notToRepeat.Add(next.Target)) {
						yield return next.Target;
						foreach (var tile in HopDestinations(next.Target, board, notToRepeat)) yield return tile;
					}
				}
			}
		}

		private static bool CanHopOver(IEnumerable<IGamePiece> entries)
		{
			return entries.Any(piece => piece.Class.Namespace == ChineseCheckersPiece.Namespace);
		}
	}
}
