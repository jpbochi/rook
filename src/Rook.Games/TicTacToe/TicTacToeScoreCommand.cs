﻿using System.Collections;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Symbols;
using System;

namespace Rook.Games.TicTacToe
{
	internal class TicTacToeScoreCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return TicTacToeScoreCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new TicTacToeScoreCommand();
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/tic-tac-toe/score");
		internal const string Relation = RookRelations.Commands.UpdateScore;

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public IMoveLink Link
		{
			get { return MoveLink.From(Template, Relation, null); }
		}

		public void Execute(IMoveExecutionContext context)
		{
			var state = context.State;
			var winners = state.Players.Where(p => DidPlayerWon(state, p.Id)).ToArray();

			if (winners.Length == 1) {
				var winner = winners.First().Id;
				var scoreCmds = state.SetWinnerCommands(winner);
				foreach (var cmd in scoreCmds) context.ExecuteMove(cmd);

				context.ExecuteMove(MoveLink.BuildUri(CommandTemplates.SetGameOver));
			} else if (winners.Length == 0 && IsBoardFull(state.Board)) {
				var scoreCmds = state.SetDrawCommands();
				foreach (var cmd in scoreCmds) context.ExecuteMove(cmd);

				context.ExecuteMove(MoveLink.BuildUri(CommandTemplates.SetGameOver));
			}
		}

		private bool IsBoardFull(IGameBoard board)
		{
			return board.Region(Tiles.MainBoard).All( tile => board.PiecesAt(tile).Any() );
		}

		private bool DidPlayerWon(IGameState game, RName playerId)
		{
			var board = game.Board;

			var winningLines =
				Enumerable.Range(0, 3).Select( r => Enumerable.Range(0, 3).Select( c => TilePos.Create(r, c) ) )
				.Concat(
					Enumerable.Range(0, 3).Select( c => Enumerable.Range(0, 3).Select( r => TilePos.Create(r, c) ) )
				)
				.Concat(
					Enumerable.Range(0, 3).Select( d => TilePos.Create(d, d) )
				)
				.Concat(
					Enumerable.Range(0, 3).Select( d => TilePos.Create(d, 2-d) )
				).Select(
					line => line.Select(pos => pos.ToTileName()).ToList()
				).ToList()
			;

			return winningLines.Any(
				line => line.All(
					pos => board.PiecesAt(pos).Any(piece => piece.GetBanner() == playerId)
				)
			);
		}
	}
}
