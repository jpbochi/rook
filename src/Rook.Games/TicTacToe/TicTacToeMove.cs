﻿using System;

namespace Rook.Games.TicTacToe
{
	internal class TicTacToeMove : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return TicTacToeMove.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new TicTacToeMove(
					match.BoundVariables["playerid"],
					match.BoundVariables["piece"],
					match.BoundVariables["position"]
				);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		public static UriTemplate Template = new UriTemplate("/drop/{playerid}/{piece}/{position}");
		internal const string Relation = RookRelations.Moves.DropPiece;

		public RName PlayerId { get; private set; }
		public RName Piece { get; private set; }
		public RName Position { get; private set; }

		public TicTacToeMove(RName playerId, RName pieceClass, RName position)
		{
			this.PlayerId = playerId;
			this.Piece = pieceClass;
			this.Position = position;
		}

		string IGameMove.RequiredRole { get { return PlayerId; } }

		IMoveLink IGameMove.Link
		{
			get {
				return MoveLink.From(Template, Relation, new { PlayerId, Piece, Position }, new { PlayerId, Destination = Position });
			}
		}

		public void Execute(IMoveExecutionContext context)
		{
			var cmds = new [] {
				MoveLink.BuildUri(CommandTemplates.AddPiece, new { Piece, Position }),
				MoveLink.BuildUri(CommandTemplates.CycleTurn),
				MoveLink.BuildUri(TicTacToeScoreCommand.Template),
			};

			foreach (var cmd in cmds) context.ExecuteMove(cmd);
		}
	}
}
