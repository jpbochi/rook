﻿using System.Collections.Generic;
using System.Linq;
using Ninject.Activation;
using Ninject.Modules;

namespace Rook.Games.TicTacToe
{
	public static class TicTacToePlayer
	{
		const string Namespace = "tic-tac-toe-player";

		public static readonly RName X = RName.Get("X", Namespace);
		public static readonly RName O = RName.Get("O", Namespace);

		public static readonly IDictionary<RName,RName> Piece = new Dictionary<RName,RName> {
			{ X, TicTacToePieces.X },
			{ O, TicTacToePieces.O }
		};
	}

	public static class TicTacToePieces
	{
		const string Namespace = "tic-tac-toe-piece";

		public static readonly RName X = RName.Get("X", Namespace);
		public static readonly RName O = RName.Get("O", Namespace);
	}

	[GameModule("Tic-Tac-Toe", WikiUrl="http://en.wikipedia.org/wiki/Tic-tac-toe")]
	public class TicTacToeGameModule : NinjectModule
	{
		public const int BOARD_SIZE = 3;

		public override void Load()
		{
			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<IGameBoard>().To<BasicBoard>();
			Bind<IPieceContainer>().To<BasicPieceContainer>().OnActivation( InitializePieceContainer );
			Bind<IGameTileGraph>().ToMethod( CreateInitialTileGraph );

			Bind<IGameMove>().To<TicTacToeScoreCommand>().Named(CommandType.UpdateScore);

			Bind<IGameReferee>().To<TicTacToeReferee>();

			Bind<IResourceInfo>().ToConstant(Resources.SteelSquareBoardBackGround);
			Bind<IResourceInfo>().ToConstant(Resources.ChessPieceSet);
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return new [] {
				new BasicPlayer(TicTacToePlayer.X),
				new BasicPlayer(TicTacToePlayer.O)
			};
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = TicTacToePlayer.X;
		}

		private void InitializePieceContainer(IPieceContainer container)
		{
			var markX = new BasicPiece(TicTacToePieces.X).SetBanner(TicTacToePlayer.X);
			var markO = new BasicPiece(TicTacToePieces.O).SetBanner(TicTacToePlayer.O);

			container.Add(markX);
			container.Add(markO);
		}

		private static IGameTileGraph CreateInitialTileGraph(IContext context)
		{
			return new BasicTileGraph(new RectangularGridTileSet(BOARD_SIZE, BOARD_SIZE), null);
		}
	}
}
