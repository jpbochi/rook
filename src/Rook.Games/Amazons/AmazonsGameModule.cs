﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;

namespace Rook.Games.Amazons
{
	[GameInitializer(typeof(AmazonsGameModule))]
	public class GoGameModuleInitializer : IGameModuleInitializer
	{
		[Range(4, 13)]
		[DefaultValue(7)]
		[DisplayName("Board Size")]
		public ushort BoardSize { get; set; }

		public void Initialize(INinjectModule module)
		{
			var game = module as AmazonsGameModule;
			if (game == null) return;

			game.BoardSize = BoardSize;
		}
	}

	[GameModule("Game of the Amazons", WikiUrl="http://en.wikipedia.org/wiki/Game_of_the_Amazons")]
	public class AmazonsGameModule : NinjectModule
	{
		public const string DefaultBindName = "Rook.Games.Amazons";
		public static readonly Symbol BoardSizeOptionName = Symbol.Declare<AmazonsGameModule>("BoardSize");

		public const int DefaultBoardSize = 7; // regular Amazons board size is 10

		public ushort BoardSize { get; set; }

		public override void Load()
		{
			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<IGameBoard>().To<BasicBoard>().OnActivation( InitializeBoard );
			Bind<IPieceContainer>().To<BasicPieceContainer>().OnActivation( InitializePieceContainer );

			Bind<ushort>().ToMethod(ctx => GetBoardSizeOrDefault()).Named(BoardSizeOptionName);
			Bind<IGameTileGraph>().ToMethod( CreateInitialTileGraph );

			Bind<IGameMove>().To<AmazonsScoreCommand>().Named(CommandType.UpdateScore);

			Bind<IGameReferee>().To<AmazonsReferee>().Named(DefaultBindName);

			Bind<IResourceInfo>().ToConstant(Resources.SteelSquareBoardBackGround);
			Bind<IResourceInfo>().ToConstant(Resources.ChessPieceSet);
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return new [] {
				new BasicPlayer(ClassicPlayer.White),
				new BasicPlayer(ClassicPlayer.Black)
			};
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = ClassicPlayer.White;
		}

		private ushort GetBoardSizeOrDefault()
		{
			var size = BoardSize;
			return size.Between<ushort>(4, 31) ? size : (ushort)DefaultBoardSize;
		}

		private void InitializePieceContainer(IPieceContainer container)
		{
			var arrow = new BasicPiece(AmazonsPieces.Arrow);
			var amazon = new BasicPiece(AmazonsPieces.Amazon).SetArrowClass(AmazonsPieces.Arrow);

			container.Add(arrow);
			container.Add(amazon);
		}

		private static IGameTileGraph CreateInitialTileGraph(IContext context)
		{
			var size = context.Kernel.Get<ushort>(BoardSizeOptionName.FullName);
			return new BasicTileGraph(
				new RectangularGridTileSet(size, size),
				VectorBasedLinkSet.CreateCardinalPlusIntercardinal()
			);
		}

		private static void InitializeBoard(IContext context, IGameBoard board)
		{
			var amazonPieceClass = board.Pieces.ById(AmazonsPieces.Amazon);

			var size = context.Kernel.Get<ushort>(BoardSizeOptionName.FullName);
			var max = size - 1;
			var lo = max / 3;
			var hi = max - lo;

			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.White), new PointInt(0, lo).ToTileName());
			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.White), new PointInt(lo, 0).ToTileName());
			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.White), new PointInt(hi, 0).ToTileName());
			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.White), new PointInt(max, lo).ToTileName());
			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.Black), new PointInt(0, hi).ToTileName());
			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.Black), new PointInt(lo, max).ToTileName());
			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.Black), new PointInt(hi, max).ToTileName());
			board.AddPiece(amazonPieceClass.New().SetBanner(ClassicPlayer.Black), new PointInt(max, hi).ToTileName());
		}
	}
}
