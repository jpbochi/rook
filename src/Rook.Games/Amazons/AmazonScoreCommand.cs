﻿using System;
using System.Linq;
using Ninject;
using JpLabs.Extensions;

namespace Rook.Games.Amazons
{
	internal class AmazonsScoreCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return AmazonsScoreCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new AmazonsScoreCommand();
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/amazons/score");
		internal const string Relation = RookRelations.Commands.UpdateScore;

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public IMoveLink Link
		{
			get { return MoveLink.From(Template, Relation, null); }
		}

		public void Execute(IMoveExecutionContext context)
		{
			var state = context.State;
			if (IsStalemated(context)) {
				var winner = state.Players.Where(p => p.Id != state.CurrentPlayerId).First().Id;

				var scoreCmds = state.SetWinnerCommands(winner);
				foreach (var cmd in scoreCmds) context.ExecuteMove(cmd);

				context.ExecuteMove(MoveLink.BuildUri(CommandTemplates.SetGameOver));
			}
		}

		private bool IsStalemated(IMoveExecutionContext context)
		{
			var relevantMoves = new [] { AmazonsSlideMove.Relation, AmazonsShootArrowMove.Relation };

			return !context.Referee.GetMoves(context.State).Any(m => relevantMoves.Contains(m.Rel));
		}
	}
}
