﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject.Modules;

namespace Rook.Games.Common
{
	public class ResignModule : NinjectModule
	{
		public const string DefaultBindName = "Rook.Games.Common.Resign";

		public override void Load()
		{
			Bind<IGameReferee>().To<ResignReferee>().Named(DefaultBindName);
		}
	}

	public class ResignReferee : IGameReferee
	{
		public IEnumerable<IMoveLink> GetMoves(IGameState state)
		{
			if (state.IsOver()) yield break;

			foreach (var player in state.Players.Where(p => p.Score == GameScore.StillPlaying)) {
				yield return new ResignMove(player.Id).Link;
			}
		}

		public IGameMove FindMove(Uri moveUri, IGameState state)
		{
			var match = MoveLink.Match(ResignMove.Template, moveUri);
			if (match == null) return null;

			return new ResignMove(match.BoundVariables["playerid"]);
		}

		bool IGameReferee.ExecuteMove(Uri moveUri, IMoveExecutionContext context)
		{
			var move = FindMove(moveUri, context.State);
			if (move == null) return false;
			move.Execute(context);
			return true;
		}
	}

	public class ResignMove : IGameMove
	{
		public static UriTemplate Template = new UriTemplate("/resign/{playerid}");
		internal static string Relation = RookRelations.Moves.Resign;

		public RName PlayerId { get; private set; }

		public ResignMove(RName playerId)
		{
			this.PlayerId = playerId;
		}

		string IGameMove.RequiredRole { get { return PlayerId; } }

		public IMoveLink Link
		{
			get {
				return MoveLink.From(Template, Relation, new { PlayerId }, new { PlayerId, Title = "Resign" });
			}
		}

		public void Execute(IMoveExecutionContext context)
		{
			var state = context.State;
			context.ExecuteMove(MoveLink.BuildUri(CommandTemplates.SetPlayerScore, new { Player = PlayerId, Score = GameScore.Resigned }));

			var stillPlaying = state.Players.Where(p => p.Score == GameScore.StillPlaying && p.Id != PlayerId);

			if (stillPlaying.Count() == 1) {
				var winner = stillPlaying.First();
				context.ExecuteMove(MoveLink.BuildUri(CommandTemplates.SetPlayerScore, new { Player = winner.Id, Score = GameScore.Won }));
				context.ExecuteMove(MoveLink.BuildUri(CommandTemplates.SetGameOver));
			} else if (state.CurrentPlayerId == PlayerId) {
				context.ExecuteMove(MoveLink.BuildUri(CommandTemplates.CycleTurn));
			}
		}
	}
}
