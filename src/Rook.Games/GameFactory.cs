﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Symbols;
using Ninject;
using Ninject.Modules;

namespace Rook.Games
{
	public interface IGameLoader
	{
		IGameLoadingContext CreateContext();
	}

	public class GameLoader : IGameLoader
	{
		public IGameLoadingContext CreateContext()
		{
			return new GameLoadingContext(StaticGameFactory.GetDefaultKernel());
		}
	}

	public interface IGameLoadingContext : IGameLoadingContextWithInitializers
	{
		IKernel Kernel { get; }

		IGameLoadingContextWithInitializers WithInitializers(IEnumerable<IGameModuleInitializer> initializers);
	}

	public interface IGameLoadingContextWithInitializers
	{
		IGameLoadingContextWithModules WithModules(IEnumerable<Type> gameModuleTypes);
	}

	public interface IGameLoadingContextWithModules
	{
		void SetInitialState(IGameState initialState);
		IGameMachine CreateGameMachine();
	}

	public class GameLoadingContext : IGameLoadingContext, IGameLoadingContextWithModules
	{
		public IKernel Kernel { get; private set; }
		private IEnumerable<IGameModuleInitializer> initializers = Enumerable.Empty<IGameModuleInitializer>();

		public GameLoadingContext(IKernel kernel)
		{
			this.Kernel = kernel;
		}

		public IGameLoadingContextWithInitializers WithInitializers(IEnumerable<IGameModuleInitializer> initializers)
		{
			this.initializers = initializers.EmptyIfNull().ToReadOnlyColl();
			return this;
		}

		private INinjectModule CreateAndInitialize(Type gameModuleType)
		{
			var module = StaticGameFactory.CreateModuleInstanceFromType(gameModuleType);
			
			initializers.ForEach(init => init.Initialize(module));
			
			return module;
		}

		IGameLoadingContextWithModules IGameLoadingContextWithInitializers.WithModules(IEnumerable<Type> gameModuleTypes)
		{
			Kernel.Load(gameModuleTypes.Select(CreateAndInitialize));

			return this;
		}

		void IGameLoadingContextWithModules.SetInitialState(IGameState initialState)
		{
			Kernel.Rebind<IGameState>().ToConstant(initialState);
		}

		IGameMachine IGameLoadingContextWithModules.CreateGameMachine()
		{
			return Kernel.Get<IGameMachine>();
		}
	}

	public static class StaticGameFactory
	{
		public static INinjectModule CreateModuleInstanceFromType(Type gameModuleType)
		{
			if (gameModuleType == null) throw new ArgumentNullException();
			if (!typeof(INinjectModule).IsAssignableFrom(gameModuleType)) throw new ArgumentException();

			return (INinjectModule)Activator.CreateInstance(gameModuleType);
		}

		public static IKernel GetDefaultKernel()
		{
			IKernel kernel = new StandardKernel();

			kernel.Bind<IJavascriptRenderer>().ToConstant(new ConstJavascriptRenderer("null"));
			kernel.Bind<Symbol>().ToConstant(Symbol.Null);

			kernel.Load<DefaultCommandsModule>();
			kernel.Load<Common.ResignModule>();

			return kernel;
		}

		public static IKernel GetKernel<TModule>() where TModule : INinjectModule, new()
		{
			var kernel = GetDefaultKernel();
			kernel.Load<TModule>();
			return kernel;
		}

		public static IGameMachine Get<TModule>() where TModule : INinjectModule, new()
		{
			return GetKernel<TModule>().Get<IGameMachine>();
		}

		public static IKernel GetKernelWithModule(INinjectModule module)
		{
			var kernel = GetDefaultKernel();
			kernel.Load(module);
			return kernel;
		}

		public static IGameMachine GetGame(INinjectModule module)
		{
			return GetKernelWithModule(module).Get<IGameMachine>();
		}

		public static IGameMachine GetGame(Type gameModuleType, IGameState state)
		{
			var kernel = GetKernelWithModule(CreateModuleInstanceFromType(gameModuleType));
			kernel.Rebind<IGameState>().ToConstant(state);
		    return kernel.Get<IGameMachine>();
		}
	}
}
