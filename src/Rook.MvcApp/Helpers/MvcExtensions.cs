﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using DotNetOpenAuth.Messaging;

namespace Rook.MvcApp
{
	public enum AppEnvironment
	{
		Unknown,
		Debug,
		Release,
		Test
	}

	public static class MvcExtensions
	{
		private class OutgoingWebResponseActionResult : ActionResult {
			private readonly OutgoingWebResponse response;

			internal OutgoingWebResponseActionResult(OutgoingWebResponse response) {
				this.response = response;
			}

			public override void ExecuteResult(ControllerContext context) {
				this.response.Send();
			}
		}

		public static ActionResult AsResult(this OutgoingWebResponse response)
		{
			return new OutgoingWebResponseActionResult(response);
		}

		public static AppEnvironment GetEnvironment(this System.Web.UI.Control self)
		{
			var env = ConfigurationManager.AppSettings["Environment"];

			switch (env) {
				case "Debug": return AppEnvironment.Debug;
				case "Release": return AppEnvironment.Release;
				case "Test": return AppEnvironment.Test;
				default: return AppEnvironment.Unknown;
			}
		}

		static string GeneratePrefix(int seed, XmlNamespaceManager nsManager)
		{
			var prefix = string.Concat("n", seed);

			if (nsManager.LookupNamespace(prefix) == null) return prefix;

			for (int suffix = 0; ; suffix++) {
				var withSuffix = string.Concat(prefix, suffix);

				if (nsManager.LookupNamespace(withSuffix) == null) return withSuffix;
			}
		}

		public static XElement InsertNamespaceDeclarations(this XElement element)
		{
			var declaredNS = element
				.Descendants()
				.SelectMany(e => e.Attributes())
				.Where(a => a.IsNamespaceDeclaration)
				.Distinct();

			var nsManager = new XmlNamespaceManager(new NameTable());
			foreach (var ns in declaredNS) nsManager.AddNamespace(ns.Name.LocalName, ns.Value);

			var nonDeclaredNS = element
				.Descendants()
				.SelectMany(e => e.Attributes())
				.Where(a => !a.IsNamespaceDeclaration && nsManager.LookupPrefix(a.Name.NamespaceName) == null)
				.Select(a => a.Name.Namespace)
				.Where(ns => ns != XNamespace.None)
				.Distinct();

			element.Add(
				nonDeclaredNS.Select((ns, i) => new XAttribute(XNamespace.Xmlns + GeneratePrefix(i, nsManager), ns.NamespaceName))
			);

			return element;
		}

		public static TimeSpan FromUtcNow(this DateTime datetime)
		{
			return DateTime.UtcNow - datetime;
		}

		public static string ToSmartAgeDisplay(this TimeSpan ageSpan, int minutesThreshold = 89, int hoursThreshold = 35)
		{
			int age = (int)Math.Round(ageSpan.TotalMinutes);			
			if (age < 1) return "less than a minute";

			if (age == 1) return "one minute";
			if (age < 89) return string.Concat(age, " minutes");

			age = (age + 30) / 60;
			if (age == 1) return "one hour";
			if (age < 35) return string.Concat(age, " hours");

			age = (age + 12) / 24;
			if (age == 1) return "one day";
			if (age < 28) return string.Concat(age, " days");

			age = (age + 3) / 7;
			if (age == 1) return "one week";
			return string.Concat(age, " weeks");
		}
	}
}
