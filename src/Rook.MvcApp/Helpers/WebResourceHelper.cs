﻿using System;
using System.Web.Caching;
using System.Web.Mvc;
using JpLabs.Extensions;
using Rook.Games;
using Rook.MvcApp.Controllers;

namespace Rook.MvcApp
{
	public static class WebResourceHelper
	{
		private const string ResourceKeyFormat = "WebResourceHelper.{0}";

		private static TValue EnsureValue<TValue>(this Cache cache, string key, Func<string,TValue> loaderFunc) where TValue : class
		{
			var value = cache[key] as TValue;
			if (value == null) {
			  value = loaderFunc(key);
			  cache.Insert(key, value);
			}
			return value;
		}

		private static string GetCacheKeyForResourceToken(string resourceInfoId)
		{
			return string.Format(ResourceKeyFormat, resourceInfoId);
		}

		public static WebResourceInfo GetResourceFromId(this Cache cache, string token)
		{
			return cache[string.Format(ResourceKeyFormat, token)] as WebResourceInfo;
		}

		public static string GetResourceToken(this Cache cache, WebResourceInfo resourceInfo)
		{
			var token = BitConverter.GetBytes(resourceInfo.GetHashCode()).ToBase64Url();
			cache.EnsureValue(
				GetCacheKeyForResourceToken(token),
				key => resourceInfo
			);
			return token;
		}

		public static string GetResourceUrl(this UrlHelper helper, WebResourceInfo resourceInfo)
		{
			return helper.Action(ResourceController.GetResourceActionName, ResourceController.ControllerName, new { id = helper.RequestContext.HttpContext.Cache.GetResourceToken(resourceInfo) });
		}
	}
}