﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Xml.Linq;
using JpLabs.Extensions;

namespace Rook.MvcApp
{
	public static class MenuItemHelper
	{
		public static bool IsCurrent(this HtmlHelper helper, string controllerName)
		{
			string currentControllerName = (string)helper.ViewContext.RouteData.Values["controller"];

			return currentControllerName.Equals(controllerName, StringComparison.CurrentCultureIgnoreCase);
		}

		public static bool IsCurrent(this HtmlHelper helper, string actionName, string controllerName)
		{
			string currentControllerName = (string)helper.ViewContext.RouteData.Values["controller"];
			string currentActionName = (string)helper.ViewContext.RouteData.Values["action"];

			return currentControllerName.Equals(controllerName, StringComparison.CurrentCultureIgnoreCase)
				&& currentActionName.Equals(actionName, StringComparison.CurrentCultureIgnoreCase);
		}

		/// <summary>
		/// This helper method renders a link within an HTML LI tag.
		/// A class="selected" attribute is added to the tag when
		/// the link being rendered corresponds to the current
		/// controller and action.
		/// 
		/// This helper method is used in the Site.Master View 
		/// Master Page to display the website menu.
		/// </summary>
		public static string MenuItem(this HtmlHelper helper, string linkText, string actionName, string controllerName, params string[] cssClasses)
		{
			string currentControllerName = (string)helper.ViewContext.RouteData.Values["controller"];
			string currentActionName = (string)helper.ViewContext.RouteData.Values["action"];

			var li = new XElement("li");

			if (currentControllerName.Equals(controllerName, StringComparison.CurrentCultureIgnoreCase)
			&& currentActionName.Equals(actionName, StringComparison.CurrentCultureIgnoreCase)) {
				cssClasses = cssClasses.EmptyIfNull().Concat("active").ToArray();
			}
			
			if (cssClasses.Any()) li.Add(new XAttribute("class", string.Join(" ", cssClasses)));

			var anchor = XElement.Parse(helper.ActionLink(linkText, actionName, controllerName).ToString());
			anchor.Add(new XAttribute("rel", string.Format("{0} {1}", controllerName, actionName)));
			li.Add(anchor);

			return li.ToString();
		}
	}
}