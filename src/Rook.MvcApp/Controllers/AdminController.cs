﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using MySql.Data.MySqlClient;

namespace Rook.MvcApp.Controllers
{
	public class AdminController : Controller
	{
		public ActionResult Index()
		{
			var provider = Membership.Provider;

			//MembershipCreateStatus status;
			//provider.CreateUser("userName", "password", "email@apphb.com", null, null, true, null, out status);

			int totalUsers;
			var users = provider.GetAllUsers(0, 15, out totalUsers);
			
			return View(users);
			/*/
			var connStr = ConfigurationManager.ConnectionStrings["sandbox"].ConnectionString;

			using (var conn = new MySqlConnection(connStr)) {
				conn.Open();

				var cmd = conn.CreateCommand();
				cmd.CommandText = "select * from games";
				var games = ReadGames(cmd.ExecuteReader()).ToArray();

				return View(games);
			}//*/
		}

		public IEnumerable<object> ReadGames(MySqlDataReader reader)
		{
			yield return new {
				Id = reader.GetFieldType(0),
				Name = reader.GetFieldType(1),
				Type = reader.GetFieldType(2),
				Data = reader.GetFieldType(3),
			};

			while (reader.Read()) {
				yield return new {
					Id = reader["id"],
					Name = reader["name"],
					Type = reader["type"],
					Data = reader["data"]
				};
			}
		}
	}
}
