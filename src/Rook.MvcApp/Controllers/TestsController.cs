﻿using System.Web.Mvc;

namespace Rook.MvcApp.Controllers
{
    public class TestsController : Controller
    {
		public const string ControllerName = "tests";
		public const string RunTestsActionName = "runtests";

        public ActionResult RunTests()
        {
            return View();
        }
    }
}
