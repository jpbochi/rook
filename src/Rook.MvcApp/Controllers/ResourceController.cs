﻿using System.Web.Mvc;

namespace Rook.MvcApp.Controllers
{
    public class ResourceController : Controller
    {
		public const string ControllerName = "resource";
		public const string GetResourceActionName = "get";

		[HttpGet]
		[OutputCache(Duration=3600, VaryByParam="id")]
		public ActionResult Get(string id)
		{
			var resource = HttpContext.Cache.GetResourceFromId(id);

			if (resource == null) return new HttpNotFoundResult();

			return new FileStreamResult(resource.GetStream(), resource.MediaType.ToString());
		}
    }
}
