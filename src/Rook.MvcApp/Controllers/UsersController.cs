﻿using System;
using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using Rook.MvcApp.Models;
using Rook.MvcApp.RookAuthentication;

namespace Rook.MvcApp.Controllers
{
	[HandleError]
	public class UsersController : Controller
	{
		public const string ControllerName = "users";

		public const string LogInActionName = "login";
		public const string LogOutActionName = "logout";
		public const string LogInAsGuestActionName = "loginasguest";
		public const string EditActionName = "edit";

		static private readonly OpenIdRelyingParty openidParty = new OpenIdRelyingParty();

		public IFormsAuthentication FormsAuth { get; private set; }
		//public IMembershipService MembershipService { get; private set; }

		public UsersController() : this(null, null) {}

		public UsersController(IFormsAuthentication formsAuth, Rook.MvcApp.RookMembership.IMembershipService membershipService)
		{
			FormsAuth = formsAuth ?? new FormsAuthenticationService();
			//MembershipService = membershipService ?? new AccountMembershipService();
		}

		public ActionResult LogInAsGuest(string returnUrl)
		{
			string guestName = "guest" + new Random().Next(1000, 9999).ToString();

			FormsAuth.SignIn(string.Format("rook.apphb.com/{0}", guestName), guestName);

			return RedirectOrGoHome(returnUrl);
		}

		public ActionResult LogIn(string returnUrl, string openid_identifier)//, string oauth_version, string oauth_server)
		{
			// Stage 1: Display login form to user.
			var openidParty = new OpenIdRelyingParty();
			IAuthenticationResponse response = openidParty.GetResponse();
 
			if (response != null) return HandleResponse(response, returnUrl);

			return MakeOpenIDRequest(openidParty, openid_identifier);
		}

		private ActionResult HandleResponse(IAuthenticationResponse response, string returnUrl)
		{
			switch (response.Status) {
				case AuthenticationStatus.Authenticated: {
					var user = RookUser.FromOpenId(response);

					FormsAuth.SignIn(response.ClaimedIdentifier, user.UserName);

					return View(EditActionName, user); //return RedirectOrGoHome(returnUrl);
				}
				case AuthenticationStatus.Canceled: {
					ModelState.AddModelError("loginIdentifier", "Login was cancelled at the provider");
					return View(string.Empty);
				}
				case AuthenticationStatus.Failed: {
					ModelState.AddModelError("loginIdentifier", "Login failed using the provided OpenID identifier");
					return View(string.Empty);
				}
			}
				
			return new EmptyResult();
		}

		private ActionResult MakeOpenIDRequest(OpenIdRelyingParty openidParty, string openid_identifier)
		{
			if (string.IsNullOrWhiteSpace(openid_identifier)) return View(string.Empty);

			// Stage 2: user submitting Identifier
			Identifier id;
			if (!Identifier.TryParse(openid_identifier, out id)) {
				ModelState.AddModelError("", "Invalid OpenID identifier '" + openid_identifier + "'");
				return View(string.Empty);
			}

			try {
				IAuthenticationRequest request = openidParty.CreateRequest(id);
 
				// Require some additional data
				request.AddExtension(new ClaimsRequest{
					Email = DemandLevel.Require,
					FullName = DemandLevel.Require,
					Nickname = DemandLevel.Require,
					//Language = DemandLevel.Require,
					//TimeZone = DemandLevel.Require,
				});

				return request.RedirectingResponse.AsResult();
			} catch (ProtocolException ex) {
				ModelState.AddModelError("", ex.Message);
				ModelState.AddModelError("", "OpenID identifier = '" + openid_identifier + "'");
				return View(string.Empty);
			}
		}

		private ActionResult RedirectOrGoHome(string returnUrl)
		{
			if (!string.IsNullOrWhiteSpace(returnUrl)) return Redirect(returnUrl);
			return RedirectToAction("index", "home");
		}

		/*public ActionResult OAuth(string returnUrl)
		{
			var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, twitterTokenManager);
			string url = Request.Url.ToString().Replace("OAuth", "OAuthCallback");
			var callBackUrl = new Uri(url);
			twitter.Channel.Send(twitter.PrepareRequestUserAuthorization(callBackUrl, null, null));

			return RedirectToAction("Login");
		}

		public ActionResult OAuthCallback()
		{
			var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, twitterTokenManager);
			var accessTokenResponse = twitter.ProcessUserAuthorization();
			if(accessTokenResponse!=null)
			{
				string userName = accessTokenResponse.ExtraData["screen_name"];
				return TryLogin(userName, null, null);
			}
			ModelState.AddModelError("", ("OAuth: No access token response!"));
			return View("Login");
		}//*/

		[Authorize]
		public ActionResult LogOut()
		{
			FormsAuth.SignOut();
			Session["UserName"] = null;
			Session["FullName"] = null;

			return RedirectToAction("index", "home");
		}

		[Authorize][HttpGet]
		public ActionResult Edit()
		{
			var user = RookUser.FromIdentity(User.GetOpenIdIdentity());

			return View(user);
		}

		[Authorize][HttpPost][ActionName(EditActionName)]
		public ActionResult Edit(RookUser user, string returnUrl)
		{
			if (!ModelState.IsValid) return View(user);

			FormsAuth.SignIn(User.GetOpenIdIdentity().OpenId, user.UserName);

			return RedirectOrGoHome(returnUrl);
		}

		/*
		private ActionResult TryLogin(string openid, string fullName, string nickname, string email)
		{
			var user = MembershipService.GetUser(openid, true);

			if (user == null) {
				var userModel = new UserModel(){
					OpenId = openid,
					FullName = fullName,
					Nickname = nickname,
					Email = email
				};
				FormsAuth.SignIn(openid, false);
				return View("Register", userModel);
			}

			FormsAuth.SignIn(user.UserName, false);

			return RedirectToAction("index", "home");
		}//*/

	}
}
