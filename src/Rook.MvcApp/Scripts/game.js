﻿/*globals Helpers, Consts */

///<reference path="lib/jquery.min.js" />
///<reference path="lib/json2.js" />
///<reference path="lib/underscore-min.js" />

///<reference path="game/gameconsts.js" />
///<reference path="game/gamecontroller.js" />
///<reference path="helpers.js" />

var Game = Game || {};

Game.Controller = null;
Game.MoveLinks = [];

Game.areBoardMovesAvailable = function () {
	return $("#button-play-move").length > 0;
};

Game.submitMove = function (move) {
	$("#move-uri").val(move.link.href);
	$("form").submit();
};

$(function () {
	var allMoves = $('.hidden-move-list a').map(function() {
		var $a = $(this);
		return {
			player: $a.data('playerid'),
			origin: $a.data('origin'),
			destination: $a.data('destination'),
			title: $a.data('title'),
			link: {
				href: $a.attr('href'),
				rel: $a.attr('rel')
			}
		};
	});
	var movesPerPlayer = _.groupBy(allMoves, 'player');
	Game.Moves = movesPerPlayer;
});