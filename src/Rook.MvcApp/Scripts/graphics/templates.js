﻿/// <reference path="../lib/linq.js" />
/// <reference path="boardCoordinateSystem.js" />

var Graphics = Graphics || {};

Graphics.CircleTemplate = function(defaults) {
	defaults = defaults || { radius: 0.5 };
	return {
		draw: function (ctx, radius) {
			radius = radius || defaults.radius;

			ctx.beginPath();
			ctx.arc(0, 0, radius, 0, Math.PI * 2, true);
			ctx.closePath();
		}
	};
};

Graphics.ImageCutTemplate = function(defaults) {
	return {
		draw: function (ctx, imageTilePos, imageTileSize, image) {
			imageTilePos = imageTilePos || defaults.imageTilePos;
			imageTileSize = imageTileSize || defaults.imageTileSize;
			image = image || defaults.image;

			ctx.drawImage(
				image,
				imageTilePos.dx * imageTileSize, imageTilePos.dy * imageTileSize, imageTileSize, imageTileSize,
				-0.5, -0.5, 1, 1
			);
		}
	};
};

Graphics.PieceTemplate = function(pieceSetImage, imageTileSize) {
	var innerTemplate = Graphics.ImageCutTemplate({
		image: pieceSetImage,
		imageTileSize: imageTileSize || parseInt($(pieceSetImage).data('tile-size'), 0)
	});

	var applyTransformationFunc = null;
	
	return {
		withTransformation: function (newTransformationFunc) {
			applyTransformationFunc = newTransformationFunc;
			return this;
		},
		draw: function (ctx, piece, board) {
			if (!pieceSetImage.complete) { return; }

			ctx.drawAt(piece.position, function() {
				applyTransformationFunc && applyTransformationFunc(ctx, piece, board);

				innerTemplate.draw(ctx, piece.img);
			});
		}
	};
};

Graphics.SquareTileTemplate = function(defaults) {
	defaults = defaults || {
		lineWidth: 0.05,
		lineJoin: 'round'
	};
	return {
		draw: function (ctx, lineWidth, lineJoin) {
			lineWidth = lineWidth || defaults.lineWidth;
			lineJoin = lineJoin || defaults.lineJoin;

			ctx.lineWidth = lineWidth;
			ctx.lineJoin = lineJoin;

			ctx.translate(-0.5, -0.5);

			var halfPixel = 0.5 * ctx.lineWidth;
			ctx.beginPath();
			ctx.rect(-halfPixel, -halfPixel, 1 + 2 * halfPixel, 1 + 2 * halfPixel);
			ctx.closePath();
		}
	};
};

Graphics.HexTileTemplate = function(defaults) {
	defaults = defaults || {
		lineWidth: 0.05,
		lineJoin: 'round'
	};
	return {
		draw: function (ctx, lineWidth, lineJoin) {
			lineWidth = lineWidth || defaults.lineWidth;
			lineJoin = lineJoin || defaults.lineJoin;

			ctx.lineWidth = lineWidth;
			ctx.lineJoin = lineJoin;

			ctx.scale(0.5, 0.5);
				
			ctx.beginPath();
			ctx.moveTo(0, -1);
				
			var sixtyDegrees = Math.PI / 3;
			var angle = sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
			ctx.closePath();
		}
	};
};

