﻿/// <reference path="../lib/linq.js" />
/// <reference path="boardCoordinateSystem.js" />

var Graphics = Graphics || {};

Graphics.BackgroundRenderer = function (backgroundImage, boardRenderer) {
	var consts = {
		BoardBorderStyle: "rgb(102, 102, 102)"
	};

	this.zorder = -1;
	this.hasCustomTransformation = true;

	this.draw = function (ctx) {
		ctx.save();

		//*

		function createImageNotFoundFillStyle(ctx) {
			var style = ctx.createLinearGradient(0, 0, 100, 100);//ctx.canvas.width / boardRenderer.zoom, ctx.canvas.height / boardRenderer.zoom);
			style.addColorStop(0, "grey");
			style.addColorStop(0.25, "white");
			style.addColorStop(0.5, "grey");
			style.addColorStop(0.75, "white");
			style.addColorStop(1, "grey");
			return style;
		}

		//warning: backgroundImage.complete does not mean that the Image has loaded successfully

		ctx.fillStyle = (backgroundImage.complete) ? ctx.createPattern(backgroundImage, "no-repeat") : createImageNotFoundFillStyle(ctx);
		ctx.rect(0, 0, ctx.canvas.width / boardRenderer.zoom, ctx.canvas.height / boardRenderer.zoom);
		ctx.fill();

		/*/
		ctx.translateToBoard();
		ctx.scaleFromPixelToTile();

		ctx.strokeStyle = 'rgb(73, 73, 73)';

		var p = {};
		for (p.x = 0; p.x < boardRenderer.board.cols; p.x++) {
		for (p.y = 0; p.y < boardRenderer.board.rows; p.y++) {

		ctx.strokeAt(p, 1, function () {
		//ctx.save();
		//ctx.scale(0.5, 0.5);
		ctx.strokeRect(.5, .5, consts.TileSize, consts.TileSize);
		//ctx.restore();
		});
		}
		}
		//*/

		ctx.restore();
	};
};

Graphics.HexBoardRenderer = function (canvas, board, pieceSetImg) {
	var consts = {
		Margin: 4,
		TileSize: 82,
		PieceTileSize: 56
	};

	this.canvas = canvas;
	this.board = board;
	this.zoom = 0.75;
	this.coordinates = new Graphics.BoardCoordinates(this.board);

	this.decorations = [];
	this.pieceSetImg = pieceSetImg;

	this.mouseToPosition = function (mouse) {
		return this.coordinates.pointToPosition({
			x: Math.floor(((mouse.x / this.zoom) - consts.Margin) / consts.TileSize),
			y: Math.floor(((mouse.y / this.zoom) - consts.Margin) / consts.TileSize)
		});
	};

	this.resetCanvas = function () {
		this.canvas.width = (consts.Margin * 2 + this.board.cols * consts.TileSize) * this.zoom;
		this.canvas.height = (consts.Margin * 2 + this.board.rows * consts.TileSize) * this.zoom;
	};

	this.getContext = function () {
		var ctx = this.canvas.getContext("2d");
		var that = this;

		ctx.canvas = canvas;
		ctx.lineWidth = 1.0;

		ctx.applyZoom = function () {
			ctx.scale(that.zoom, that.zoom);
		};

		ctx.translateToBoard = function () {
			ctx.translate(consts.Margin, consts.Margin);
		};

		ctx.scaleFromPixelToTile = function () {
			var scale = consts.TileSize;
			ctx.lineWidth /= scale;
			ctx.scale(scale, scale);
		};

		ctx.scaleFromTileToPixel = function () {
			var scale = 1 / consts.TileSize;
			ctx.scale(scale, scale);
		};

		ctx.translateFromBoardToTile = function (position) {
			position = that.coordinates.positionToPoint(position);
			ctx.translate(position.x, position.y);
		};

		ctx.drawAt = function (position, drawFunc) {
			ctx.save();
			ctx.translateFromBoardToTile(position);

			drawFunc();

			ctx.restore();
		};

		ctx.drawTile = function (lineWidth, lineJoin) {
			lineWidth = lineWidth || 1;
			lineJoin = lineJoin || "round";

			ctx.lineWidth *= lineWidth;
			ctx.lineJoin = lineJoin;

			ctx.translate(0.5, 0.5);
			ctx.scale(0.5, 0.5);
				
			ctx.beginPath();
			ctx.moveTo(0, -1);
				
			var sixtyDegrees = Math.PI / 3;
			var angle = sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
				
			angle += sixtyDegrees;
			ctx.lineTo(Math.sin(angle), -Math.cos(angle));
			ctx.closePath();
		};

		return ctx;
	};

	this.drawPiece = function (ctx, piece) {
		if (!this.pieceSetImg.complete) return;

		ctx.save();

		ctx.translateFromBoardToTile(piece.position);

		ctx.drawImage(
			this.pieceSetImg,
			piece.img.dx * consts.PieceTileSize, piece.img.dy * consts.PieceTileSize, consts.PieceTileSize, consts.PieceTileSize,
			0, 0, 1, 1
		);

		ctx.restore();
	};

	this.drawDecoration = function (ctx, decoration) {
		if (!decoration) return;
		ctx.save();

		if (!decoration.hasCustomTransformation) {
			ctx.translateToBoard();
			ctx.scaleFromPixelToTile();
		}

		if (decoration.position) ctx.translateFromBoardToTile(decoration.position);
		decoration.draw(ctx);

		ctx.restore();
	};

	this.drawBoardContent = function (ctx) {
		ctx.save();
		ctx.applyZoom();

		var that = this;

		var getAllValues = function (obj) {
			var values = [];
			for (var key in obj) values.push(obj[key]);
			return values;
		};

		Enumerable.From(getAllValues(this.decorations)).OrderBy(function (dec) {
			return dec.zorder || 0;
		}).ForEach(function (dec) {
			that.drawDecoration(ctx, dec);
		});

		ctx.translateToBoard();
		ctx.scaleFromPixelToTile();

		Enumerable.From(this.board.pieces).OrderBy(function (piece) {
			return piece.zorder || 0;
		}).ForEach(function (piece) {
			that.drawPiece(ctx, piece);
		});

		ctx.restore();
	};

	this.drawBoard = function (ctx) {
		//Read this: http://diveintohtml5.org/canvas.html
		//Sduty this: http://www.html5canvastutorials.com/
		//Research here: http://www.w3.org/TR/2dcontext/
		//Cheat from here: http://simon.html5.org/dump/html5-canvas-cheat-sheet.html

		this.drawBoardContent(ctx);
	};

	this.repaint = function () {
		this.resetCanvas();
		this.drawBoard(this.getContext());
	};

	this.removeDecoration = function (key) {
		delete this.decorations[key];
	};

	this.clearDecorations = function () {
		this.decorations = [];
	};
};
