﻿/*globals Game */

/// <reference path="../lib/underscore-min.js" />
/// <reference path="gameconsts.js" />
/// <reference path="board.js" />

var Rook = Rook || {};

Rook.GameController = function (board, currentPlayer, availableMoves, graphics) {
	graphics = graphics || {
		origin: {style:'rgba(111, 255, 111, 0.9)', template: 'tile' },
		target: {style:'rgba(111, 255, 111, 0.9)', template: 'tile' },
		move: {style:'rgba(55, 222, 55, 0.7)', template: 'circle', args: [0.2]}
	};
	var currentMode = Game.Modes.Start;
	var selection = {};

	var controller = {
		currentMode: function() {
			if (arguments.length === 1) {
				currentMode = arguments[0];
			}
			return currentMode;
		},
		selection: function () {
			if (arguments.length === 1) {
				selection = arguments[0];
			}
			return selection;
		},
		graphics: function(name) {
			return graphics[name];
		},

		resetPiece: function () {
			if (selection.piece) {
				selection.piece.move();
				selection.piece.resetZorder();
				selection.piece = null;
			}
		},

		resetMode: function () {
			this.resetPiece();

			currentMode = Game.Modes.Start;
			selection = { origin: null, target: null };
		},

		updateMode: function (newMode, clickTileName) {
			this.resetPiece();

			currentMode = newMode;

			switch (currentMode) {
				case Game.Modes.Start:
					selection = { origin: null, target: null };
				break;

				case Game.Modes.DropSelected:
				case Game.Modes.PieceSelected:
					selection = { origin: null, target: clickTileName };
				break;

				case Game.Modes.MoveSelected:
					selection = { origin: selection.target, target: clickTileName };

					selection.piece = Enumerable.From(board.getPiecesAt(selection.origin)).FirstOrDefault(null);
					if (selection.piece) {
						selection.piece.move(selection.target);
						selection.piece.setZorder(1);
					}
				break;
			}
		},

		click: function (clickTileName) {
			var lastClick = selection.target;
			var piecesAtClick = board.getPiecesAt(clickTileName);
			var hasPiecesAtClick = (piecesAtClick.length > 0);

			switch (currentMode) {
				case Game.Modes.Start:
					if (!hasPiecesAtClick) {
						this.updateMode(Game.Modes.DropSelected, clickTileName);
					} else {
						this.updateMode(Game.Modes.PieceSelected, clickTileName);
					}
				break;

				case Game.Modes.DropSelected:
					if (clickTileName === lastClick) {
						this.updateMode(Game.Modes.Start);
					} else if (!hasPiecesAtClick) {
						this.updateMode(Game.Modes.DropSelected, clickTileName);
					} else {
						this.updateMode(Game.Modes.PieceSelected, clickTileName);
					}
				break;

				case Game.Modes.PieceSelected:
					if (clickTileName === lastClick) {
						this.updateMode(Game.Modes.Start);
					} else {
						this.updateMode(Game.Modes.MoveSelected, clickTileName);
					}
				break;

				case Game.Modes.MoveSelected:
					this.updateMode(Game.Modes.Start);
				break;
			}
		},

		getMovesToHighlight: function () {
			switch (currentMode) {
				case Game.Modes.Start:
				case Game.Modes.DropSelected:
					return _.filter(availableMoves[currentPlayer], function(move) {
						return !move.origin
							&& move.destination;
					});

				case Game.Modes.PieceSelected:
					return _.filter(availableMoves[currentPlayer], function(move) {
						return move.origin === selection.target
							&& move.destination;
					});

				case Game.Modes.MoveSelected:
					return _.filter(availableMoves[currentPlayer], function(move) {
						return move.origin === selection.origin
							&& move.destination;
					});

				default: return [];
			}
		},

		getSelectedMove: function () {
			switch (currentMode) {
				case Game.Modes.DropSelected:
					return _.filter(availableMoves[currentPlayer], function(move) {
						return !move.origin
							&& move.destination === selection.target;
					})[0] || null;

				case Game.Modes.MoveSelected:
					return _.filter(availableMoves[currentPlayer], function(move) {
						return move.origin === selection.origin
							&& move.destination === selection.target;
					})[0] || null;

				default: return null;
			}
		},

		getBoardHighlights: function () {
			var list = [];

			(selection.origin) && list.push({ position: selection.origin, graphic: graphics.origin });
			(selection.target) && list.push({ position: selection.target, graphic: graphics.target });

			var moves = _.map(this.getMovesToHighlight(), function(move) {
				return { position: move.destination, graphic: graphics.move };
			});

			return list.concat(moves);
		},

		createDecorationWithMoveHighlights: function () {
			var highlights = this.getBoardHighlights();
			return {
				draw: function (ctx) {
					_.each(highlights, function(hl) {
						hl.position && ctx.drawAt(hl.position, function() {
							ctx.drawTemplate(hl.graphic.template, hl.graphic.args);
							if (hl.graphic.style) { ctx.fillStyle = hl.graphic.style; }
							ctx.fill();
						});
					});
				}
			};
		}
	};
	return controller;
};
