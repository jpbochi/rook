﻿/// <reference path="board.js" />
/// <reference path="gameparser.js" />

var Rook = Rook || {};
Rook.GameClient = {
	load: function(gameHref, callback) {
		$.ajax({
			url: gameHref,
			type: 'GET',
			success: function(data) {
				var game = Rook.Parser.parse(data);
				callback(game);
			}
		});
	}
};