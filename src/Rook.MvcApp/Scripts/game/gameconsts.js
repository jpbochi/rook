﻿var Game = Game || {};

Game.Modes = {
	Start: { instructions: "Click on the board to play." },
	PieceSelected: { instructions: "Click to select a destination for this piece." },
	DropSelected: { instructions: "Move selected: Drop new Piece.", isFinal: true },
	MoveSelected: { instructions: "Move selected: Move Piece.", isFinal: true }
};
