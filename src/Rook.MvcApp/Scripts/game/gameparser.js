﻿/// <reference path="../lib/underscore-min.js" />
/// <reference path="board.js" />

var Rook = Rook || {};
Rook.Parser = function() {
	function groupByKey(list, key, selector) {
		var hash = _(list).groupBy(key, selector);
		_.chain(hash).keys().each(function (k) {
			hash[k] = _(hash[k]).first();
		});
		return hash;
	}

	return {
		parse: function (game) {
			return {
				version: game.version,
				currentPlayer: game.currentPlayer,
				players: this.parsePlayers(game.players),
				board: this.parseBoard(game),
			};
		},
		parsePlayers: function(players) {
			return groupByKey(
				players,
				function(p) { return p.id; },
				function(p) {
					var links = groupByKey(p.links, 'rel', 'href');
					return {
						score: p.score,
						links: links
					};
				}
			);
		},
		parseBoard: function (game) {
			return new Rook.Board(game.tiles, this.parsePieces(game.pieces));
		},
		parsePieces: function (pieces) {
			return $.map(pieces, this.parsePiece);
		},
		parsePiece: function (piece) {
			return new Rook.Piece(
				piece.id,
				piece.position,
				{ dx: piece.imagePos.x, dy: piece.imagePos.y }
			);
		}
	};
}();
