﻿/// <reference path="lib/jquery.min.js" />
/// <reference path="lib/json2.js" />

var Consts = {
	GameId: null,
	CurrentPlayer: null,
	AvailableMoves: Enumerable.Empty()
};

var Mouse = {
	getRelativeCoordinates: function (event, elem) {
		var offset = $(elem).offset();
		return {
			x: event.clientX - Math.floor(offset.left) + document.body.scrollLeft + document.documentElement.scrollLeft,
			y: event.clientY - Math.floor(offset.top) + document.body.scrollTop + document.documentElement.scrollTop
		};
	}
};

var Rook = Rook || {};
Rook.Config = function() {
	var localStorage = window.localStorage || {
		setItem: function() {},
		removeItem: function() {},
		getItem: function() { return null; }
	};

	return {
		autoPlay: function() {
			var key = 'rook.auto-play-off';
			if (arguments.length > 0) {
				arguments[0] ? localStorage.removeItem(key) : localStorage.setItem(key, true);
			}
			return !localStorage.getItem(key);
		}
	};
}();

