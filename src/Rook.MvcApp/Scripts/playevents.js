﻿/*globals Game, GameController, UI, Consts */

/// <reference path="lib/jquery.min.js" />
/// <reference path="lib/jquery.linq.js" />
/// <reference path="lib/linq.js" />
/// <reference path="lib/json2.js" />

/// <reference path="viewui.js" />
/// <reference path="helpers.js" />
/// <reference path="game.js" />

function showNonBoardMoves() {
	$('.moves-for-player').each(function() {
		var player = $(this).data('playerid');
		var moves = _.filter(Game.Moves[player], function(m) {
			return m.title;
		});
		
		var movelist = $(this).find('.list');
		_.each(moves, function(m) {
			var link = $('<a></a>')
				.attr('href', m.link.href)
				.attr('rel', m.link.rel)
				.addClass('move-link')
				.html(m.title);
			var li = $('<li></li>').append(link);
			movelist.append(li);
		});

		$(this).toggleClass('empty', moves.length === 0);
	});

	var playersWithMoves = $('.moves-for-player:not(.empty)');
	playersWithMoves.toggleClass('single', playersWithMoves.length === 1);

	$('.moves-for-player .move-link').click(function (e) {
		e.preventDefault();
		Game.submitMove({ link: { href: $(this).attr('href') } });
	});
}

function setupCanvas(canvas, game) {
	if (canvas.getContext) {
		var imageRepository = new Graphics.ImageRepository($('#board-image-repository'));
		var board = game.board;

		var rendererArgs = UI.getBoardRendererArgs ? UI.getBoardRendererArgs(board, imageRepository) : {
			rootObj: new Graphics.RectBoardRoot(board.getSize()),
			templates: {
				circle: new Graphics.CircleTemplate(),
				tile: new Graphics.SquareTileTemplate(),
				coordinates: new Graphics.RectBoardCoordinatesTemplate(),
				piece: new Graphics.PieceTemplate(imageRepository.get('.piece-set'))
			},
			coordinateSystem: new Graphics.BoardCoordinateSystem(board.getSize())
		};

		UI.boardRenderer = new Graphics.BoardRenderer(canvas, game.board, imageRepository, rendererArgs);

		$('#board-image-repository img').load(function () {
			UI.boardRenderer.repaint();
		});
	}
}

function startGame(game) {
	setupCanvas($('#board_canvas').get(0), game);

	$('#board_canvas').mouseout(function () {
		UI.boardRenderer.removeDecoration('mouse');
		UI.boardRenderer.repaint();
	});

	UI.boardRenderer.mousemove('.tile', function(hit) {
		UI.boardRenderer.decoration('mouse', {
			draw: function (ctx) {
				ctx.drawAt(hit.position, function() {
					ctx.drawTemplate('tile', 0.07);
					ctx.strokeStyle = 'rgba(73, 73, 255, 0.8)';
					ctx.stroke();
				});
			}
		});
		UI.boardRenderer.repaint();
	});

	showNonBoardMoves();
	UI.boardRenderer.repaint();

	if (Game.areBoardMovesAvailable()) {

		var refreshUI = function () {
			UI.displayMessage(Game.Controller.currentMode().instructions);
			UI.boardRenderer.decoration('controller', Game.Controller.createDecorationWithMoveHighlights());
			UI.boardRenderer.repaint();
		};

		var tryPlay = function () {
			var move = Game.Controller.getSelectedMove();

			if (move === null) {
				UI.displayMessage('Invalid move selected. Click again to clear.');
			} else {
				Game.submitMove(move);
			}
		};

		var isAutoPlayOn = function () {
			return $('#auto-play-checkbox').is(':checked');
		};

		var refreshAutoPlayUI = function () {
			var isOn = isAutoPlayOn();
			$('#auto-play-checkbox-label').text(isOn ? 'disable move auto-commit' : 'enable move auto-commit');
			$('#button-play-move').toggle(!isOn);
		};

		$('#auto-play-checkbox').attr('checked', Rook.Config.autoPlay() ? 'checked' : null);
		$('#auto-play-checkbox').change(function () {
			Rook.Config.autoPlay(isAutoPlayOn());

			refreshAutoPlayUI();
		});

		Game.Controller = new Rook.GameController(game.board, game.currentPlayer, Game.Moves);
		refreshUI();
		refreshAutoPlayUI();

		UI.boardRenderer.click('.tile', function (hit) {
			Game.Controller.click(hit.position.name);
			refreshUI();

			if (isAutoPlayOn() && Game.Controller.currentMode().isFinal) {
				UI.displayMessage('playing&hellip;');
				tryPlay();
            }
		});

		$('#button-play-move').click(function () { tryPlay(); });
	}
}

$(function () {
	var url = $("a[rel='game-for-public']").attr('href');
	Rook.GameClient.load(url, function(game) {
		startGame(game);
	});
});
