﻿/*globals UI */

/// <reference path="../lib/jquery.min.js" />
/// <reference path="../lib/jquery.linq.js" />
/// <reference path="../lib/linq.js" />
/// <reference path="../lib/json2.js" />

var AjaxWaiter = function(getAjaxParamsFunc) {
	var that = {
		getAjaxParamsFunc: getAjaxParamsFunc,
		processResponseFunc: null,
		onerror: null,
		onstop: null,
		waitButton: null,
		loadingDiv: null,
		interval: 4000
	};

	that.stop = function() {
		that.loadingDiv.hide();
		that.waitButton.removeClass("waiting");
		that.waitButton.val("Wait");

		(that.onstop) && that.onstop();
	};

	var callAsync = function() {
		if (!that.waitButton.hasClass("waiting")) { return; }

		var errorFunc = function (jqXHR, textStatus, errorThrown) {
			that.stop();

			(that.onerror) && that.onerror(jqXHR, textStatus, errorThrown);
		};

		var successFunc = function (response) {
			if (that.processResponseFunc(response)) {
				that.stop();
			} else {
				setTimeout(callAsync, that.interval);
			}
		};

		var ajaxParams = that.getAjaxParamsFunc();
		ajaxParams.success = successFunc;
		ajaxParams.error = errorFunc;

		try {
			jQuery.ajax(ajaxParams);
		} catch (err) {
			UI.displayMessage(JSON.stringify(err, null, " "));
		}
	};

	that.start = function() {

		that.waitButton.addClass("waiting");
		that.waitButton.val("Waiting...");
		that.loadingDiv.show();

		callAsync();
	};

	that.attachTo = function(waitButton, loadingDiv) {
		that.waitButton = waitButton;
		that.loadingDiv = loadingDiv;

		that.waitButton.click(function (e) {

			if (that.waitButton.hasClass("waiting")) {
				return that.stop();
			}

			that.start();
			that.waitButton.blur();
		});
	};

	return that;
};
