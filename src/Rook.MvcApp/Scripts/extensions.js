﻿
// jQuery extensions

jQuery && jQuery.fn.extend({
	externalHtml: function() {
		return $('<div>').append(this.clone()).html();
	}
});

// String extensions

String.prototype.lineBreaksToBrs = function () {
	return this.replace(/\n/gm, '<br/>');
};

String.prototype.startsWith = function (str) {
	return (this.indexOf(str) === 0);
};

String.prototype.format = function () {
	var args = arguments;
	return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
		if (m == "{{") { return "{"; }
		if (m == "}}") { return "}"; }
		return args[n];
	});
};

String.prototype.ellipsis = function(length) {
	if (this.length <= length) return this;
	return this.substring(0, length) + '...';
};

String.prototype.empty = function() {
	return this.trim().length == 0;
};

String.prototype.notEmpty = function() {
	return this.trim().length > 0;
};

if (!''.trim) {
	String.prototype.trim = function() {
		return $.trim(this);
	};
}

String.prototype.camelCaseToPhrase = function() {
	return this.replace(/([A-Z])/g, function(letter, dunno, index) {
		return index == 0 ? letter : ' ' + letter;
	});
};

String.prototype.capitalize = function() {
	return this.substring(0, 1).toUpperCase() + this.substring(1).toLowerCase();
};

String.prototype.uncapitalizeFirstChar = function() {
	return this.substring(0, 1).toLowerCase() + this.substring(1);
};

String.prototype.contains = function(s) {
	return this.indexOf(s) >= 0;
};

String.prototype.clearTabsAndReturns = function() {
	return this.replace(/[\n\t]/g, '');
};

String.prototype.escapeQuotes = function() {
	return this.replace(/"/gi, '\\"');
};

String.prototype.escapeSlashes = function() {
	return this.replace(/\\/gi, '\\\\');
};

String.prototype.escapeAll = function() {
	return this.clearTabsAndReturns().escapeSlashes().escapeQuotes();
};

String.prototype.htmlEncode = function() {
	return $('<div/>').text(this.toString()).html().replace(/"/gi, '&quot;');
};

String.prototype.extractId = function () {
	//deprecated('put a "data-id" in the element and call data(\'id\') instead.');

	var id;
	// handles negative ids and element ids with numbers other than the id itself.
	this.replace(/(\-*\d+)$/g, function(match) {
		id = match.replace('--', '___').replace('-', '').replace('___', '-');
	});
	return parseInt(id, 10);
};

// Array extensions

Array.prototype.first = function() {
	return this[0];
};

Array.prototype.last = function() {
	return this[this.length - 1];
};

Array.prototype.map = function(func) {
	var array = [];
	for (var i = 0; i < this.length; i++) {
		array.push(func.call(this[i], this[i]));
	}
	return array;
};

Array.prototype.each = function(func) {
	for (var i = 0; i < this.length; i++) {
		func.call(this[i], this[i], i);
	}
	var array = this;
	return {
		ifEmpty: function (closure) {
			if (array.empty())
				closure();
		}
	};
};

Array.prototype.copy = function() {
	return this.slice(0);
};

Array.prototype.sortBy = function(func) {
	var copy = this.copy();
	copy.sort(func);
	return copy;
};

Array.prototype.empty = function() {
	return this.length == 0;
};

Array.prototype.notEmpty = function() {
	return this.length > 0;
};

Array.prototype.remove = function(indexOrFunction) {
	if (indexOrFunction.constructor == Number) {
		return this.slice(0, indexOrFunction).concat(this.slice(indexOrFunction + 1));
	}

	var index = this.findIndex(indexOrFunction);
	return index >= 0 ? this.remove(index) : this;
};

Array.prototype.removeWithIndex = function (from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};

Array.prototype.findIndex = function(criteria) {
	var found = -1;
	this.each(function(element, index) {
		if (criteria.call(this, this)) { found = index; }
	});
	return found;
};

Array.prototype.find = function(criteria) {
	var found = null;
	this.each(function() {
		if (!found && criteria.call(this, this)) { found = this; }
	});
	return found;
};

Array.prototype.contains = function(funcOrElement) {
	var criteria = funcOrElement;
	if (funcOrElement.constructor != Function) {
		criteria = function() {
			return this == funcOrElement;
		};
	}
	return !!this.find(criteria);
};

Array.prototype.groupCollectionsBy = function(groupFunction) {
	var map = {};
	this.each(function() {
		var key = groupFunction.call(this);
		if (!map[key]) { map[key] = []; }
		map[key].push(this);
	});

	map.each = function(closure) {
		for (var key in this) {
			if (this.hasOwnProperty(key) && !(this[key] instanceof Function)) {
				closure.call(this[key], key);
			}
		}
	};

	map.map = function(closure) {
		var transformed = [];
		for (var key in this) {
			if (this.hasOwnProperty(key) && !(this[key] instanceof Function)) {
				transformed.push(closure.call(this[key], key));
			}
		}
		return transformed;
	};

	return map;
};

Array.prototype.groupBy = function(groupFunction) {
	var map = {};
	this.each(function() {
		var key = groupFunction.call(this);
		map[key] = this;
	});
	return map;
};

Array.prototype.filter = function(criteria) {
	var filtered = [];
	this.each(function() {
		criteria.call(this, this) && filtered.push(this);
	});
	return filtered;
};

Array.prototype.any = function(criteria) {
	return this.filter(criteria).length > 0;
};

Array.prototype.all = function(criteria) {
	return this.filter(criteria).length == this.length;
};

Array.prototype.flatten = function () {
	var flattened = [];
	this.each(function () {
		flattened = flattened.concat(this);
	});
	return flattened;
};

Array.prototype.except = function (another, criteria) {
	return this.filter(function (element) {
		return !another.contains(function (o2) {
			return criteria(element, o2);
		});
	});
};

Array.prototype.distinct = function (criteria) {
	criteria = criteria || function (one, another) { return one == another; };
	var filtered = [];
	this.each(function (element) {
		if (!filtered.contains(function (o2) { return criteria(element, o2); })) {
			filtered.push(this);
		}
	});

	return filtered;
};

// indexOf method for IE - from https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/IndexOf
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(searchElement /*, fromIndex */) {
		"use strict";

		if (this === void 0 || this === null) {
			throw new TypeError();
		}

		var t = Object(this);
		var len = t.length >>> 0;

		if (len === 0) {
			return -1;
		}
	
		var n = 0;
		if (arguments.length > 0) {
			n = Number(arguments[1]);	
			if (n !== n) { // shortcut for verifying if it's NaN
				n = 0;
			} else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0)) {
			  n = (n > 0 || -1) * Math.floor(Math.abs(n));
		}
		}

		if (n >= len) {
			return -1;
		}
	
		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
	
		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) {
				return k;
		}
		}
	
		return -1;
	};
}

Array.prototype.toArrayParameter = function (name) {
	return this.map(function () { return name + '=' + this; }).join('&');
};

Array.prototype.pushIfTruthy = function (possibleElement) {
	possibleElement && this.push(possibleElement);
};

Array.prototype.foldLeft = function(acc, fn) {
	this.each(function () {
		acc = fn(acc, this);
	});
	return acc;
};

Array.prototype.sum = function (propFunction) {
	return this.foldLeft(0, function (a, b) { return a + propFunction(b); });
};

Array.prototype.toJson = function () {
	return '[' + this.map(function() { return this.toJson ? this.toJson() : this; }).join(',') + ']';
};

// Global functions

function log() {
	if (window.console && window.console.log && (typeof window.console.log) == 'function') {
		console.log.apply(console, Array.prototype.slice.call(arguments));
	}
}

function deprecated(newOne) {
	if ((typeof console) == 'object' && (typeof console.trace) == 'function') {
		console.trace('This function has been deprecated.' + (newOne ? ' You should use ' + newOne : ''));
	}
}

// Number extensions

Number.prototype.times = function (closure) {
	for (var i = 1; i <= this; i++) {
		closure.call(i);
	}
};
Number.prototype.reverseTimes = function (closure) {
	for (var i = this; i >= 1; i--) {
		closure.call(i);
	}
};
Number.prototype.isValidInt32 = function() {
	return this < 2147483647;
};
Number.prototype.fromBytesToKBytes = function() {
	return this < 1024 ? (this + 'b') : (parseInt(this / 1024, 10) + 'k');
};

// Function extensions

Function.prototype.and = function(func) {
	var self = this;
	return function() {
		var args = $.makeArray(arguments);
		self.call(self, args);
		func.call(self, args);
	};
};
Function.prototype.curry = function() {
	if (arguments.length<1) {
		return this; //nothing to curry with - return function
	}
	var method = this;
	var args = $.makeArray(arguments);
	return function() {
		return method.apply(this, args.concat($.makeArray(arguments)));
	};
};
Function.prototype.curryAndApply = function(thisToApply) {
	if (arguments.length<2) {
		return this; //nothing to curry with - return function
	}
	var method = this;
	var args = $.makeArray(arguments);
	return function() {
		return method.apply(thisToApply, args.slice(1).concat($.makeArray(arguments)));
	};
};
Function.prototype.times = function(times) {
	for (var i = 0; i < times; i++) {
		this();
	}
};
