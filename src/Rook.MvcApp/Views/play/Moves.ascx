﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<IMoveLink>>" %>
<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="JpLabs.Extensions" %>

<span>You have <%: Model.Count() %>  moves available.</span>
<ul>
<% if (ViewContext.IsChildAction) foreach (var move in Model) { %>
	<li><%= move.ToHyperlink() %></li>
<% } else {
	var gameId = ViewContext.RouteData.Values["gameId"].ToString();
	var linkTemplate = new UriTemplate("/play/moves/{id}/{*href}");
	var baseUri = new Uri("http://rook");
	
	foreach (var move in Model) { %>
		<%
		var link = move.ToHyperlink();
		var href = link.Attribute("href").Value;
		href = linkTemplate.BindByPosition(baseUri, gameId, href.TrimStart('/')).PathAndQuery;
		link.Attribute("href").Value = href;
		%>
		<li><%= link %></li>
	<% } %>
<% } %>
</ul>
