﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rook.MvcApp.Models.GameViewModel>" %>
<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="JpLabs.Extensions" %>
<%
	var game = Model.Entry;
%>

<script type="text/javascript">
<!--
//<![CDATA[
	Consts.RootPath = '<%= ResolveUrl("~") %>';
	Consts.GameId = '<%= game.Id %>';
	UI.getBoardRendererArgs = <%= game.Machine.GetJSBoardRendererFunc() %>;
//]]>-->
</script>