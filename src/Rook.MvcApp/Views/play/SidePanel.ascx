﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rook.MvcApp.Models.GameViewModel>" %>

<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="Rook.MvcApp.Controllers" %>

<%
var game = Model.Entry;
var machine = game.Machine;
var currentPlayer = game.State.GetCurrentPlayer();
%>

<div class="side-panel">
	<div class="player-status">
		<% if (currentPlayer == null) { %>
			<h4>Game Over</h4>
		<% } else if (currentPlayer.IsControlledBy(Context.User.GetOpenIdIdentity())) { %>
			<h4>Your turn</h4>
		<% } else { %>
			<div class="inline">
				Waiting for:
				<img src="<%= ResolveUrl("~/content/img/" + currentPlayer.GetImage())%>" alt="current player avatar" />
				<h4 class="inline player-name"><%= currentPlayer.GetHtmlName() %></h4>
			</div>
		<% } %>
	</div>

	<div class="player-details">
		<ul class="info-list">
			<% var winner = game.State.Players.Where(p => p.Score == GameScore.Won).FirstOrDefault(); %>
			<% if (winner != null) { %>
				<li class="winner-player">
					<span><%= winner.GetHtmlName() %> (<%: winner.Id.Name %>) <span class="score">Won</span></span>
				</li>
			<% } %>

			<% foreach (var player in machine.State.Players) { %>
				<li class="moves-for-player" data-playerid="<%: player.Id %>">
					<span class="hotseat">As <span class="player-name"><%: player.Id.Name %></span> player:</span>
					<span class="single">You can:</span>
					<ul class="list"></ul>
				</li>
			<% } %>
		</ul>
	</div>

</div>