﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rook.MvcApp.Models.GameViewModel>" %>

<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>

<%
var game = Model.Entry;
var currentPlayer = game.State.GetCurrentPlayer();
var controlsCurrentPlayer = (currentPlayer != null) && currentPlayer.IsControlledBy(this.Context.User.GetOpenIdIdentity());
%>

<h3>Player List</h3>

<ul class="player-list">
<% foreach (var player in game.State.Players) { %>
	<li style="list-style-image: url('<%= ResolveUrl("~/content/img/" + player.GetImage())%>');">
		<div style="display:inline-block;">
			<div>
				<span class="player-name">
					<% if (player.IsOpen()) { %>
						<span>Open</span>
						<a href="<%= ResolveUrl("~/play/join/" + game.Id + "?player=" + player.Id ) %>">join</a>
					<% } else { %>
						<%: player.GetName() %>
						<%: player.IsControlledBy(this.Context.User.GetOpenIdIdentity()) ? "(you)" : "" %>
						<%: (player.Score != GameScore.StillPlaying) ? player.Score.Name : "" %>
					<% } %>
				</span>
			</div>
			<div>
				<span><%: player.Id.Name %></span>
				<span><%: (player == currentPlayer) ? "(current)" : "" %></span>
			</div>
			<div>
				<%: player.GetExtraScoreDisplay() %>
			</div>
		</div>
	</li>
<% } %>
</ul>