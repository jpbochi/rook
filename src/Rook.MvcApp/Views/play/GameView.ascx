﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rook.MvcApp.Models.GameViewModel>" %>

<%@ Import Namespace="JpLabs.Extensions" %>
<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="Rook.MvcApp.Controllers" %>

<% var game = Model.Entry; %>
	
<div class="game-header">
	<h1 class="inline"><%: game.GameType.GetDescription() %></h1>
	&nbsp;&nbsp;
	<h2 class="inline"><%: game.DisplayName %></h2>
</div>

<% using (Html.BeginForm()) { %>
	<input id="move-uri" name="moveUri" type="hidden" value="" />
<% } %>

<div class="hidden">
	<%= Html.ActionLink(
		"game for public",
		PlayController.GameForPublicActionName,
		new { id = game.Id },
		new { rel = "game-for-public" }
	) %>
</div>

<div class="game-view">
		
	<div class="game-panel">
		<div class="player-list-panel">
			<% Html.RenderPartial("playerlistpanel"); %>
		</div>
		<div class="player-panel">
			<% Html.RenderPartial("sidepanel"); %>
		</div>
	</div>

	<div class="game-panel">
		<div class="board-panel">
			<% Html.RenderPartial("boardpanel"); %>
		</div>

		<% Html.RenderPartial("messagepanel"); %>
	</div>

	<div class="clear"></div>
</div>

