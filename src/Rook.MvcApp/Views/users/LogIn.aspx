﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Rook.MvcApp.Controllers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
Rook - Log In
</asp:Content>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
	<link href="<%= ResolveUrl("~/content/css/openid.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content  ContentPlaceHolderID="MainContent" runat="server">
	<h2>Log In</h2>

	<div>
		<%
			var returnUrl = Request.QueryString["ReturnUrl"];
			var queryString = string.IsNullOrWhiteSpace(returnUrl) ? "" : "?ReturnUrl=" + HttpUtility.UrlEncode(returnUrl);
		%>
		<%-- using (var form = Html.BeginForm("login", "account", FormMethod.Post, new { id = "openid_form" })) {--%>
		<form action="<%= Url.Action(UsersController.LogInActionName)%><%= queryString %>" id="openid_form" method="post">
			<%: Html.ValidationSummary() %>
			
			<!-- OAuth form elements -->
			<!--<input type="hidden" id="oauth_version" name="oauth_version">
			<input type="hidden" id="oauth_server"  name="oauth_server">-->

			<div id="openid_choice">
				<span>Select your OpenID:</span>
				<div id="openid_btns"></div>
			</div>
			<div id="openid_input_area"></div>
			<div>
				<noscript>
				<p>OpenID is a service that allows you to log on to many different websites using a single identity.
				Find out <a href="http://openid.net/what/">more about OpenID</a> and <a href="http://openid.net/get/">how to get an OpenID enabled account</a>.</p>
				</noscript>
			</div>
			<div>
				<span>Or, enter your OpenID:</span>

				<input id="openid_identifier" name="openid_identifier" class="openid-identifier" type="text" />
				<input id="submit-button" class="button" type="submit" value="Log In" />
			</div>
			<div class="last-option">
				<span>Or, play anonymously:</span>

				<a class="button" href="<%= Url.Action(UsersController.LogInAsGuestActionName)%><%= queryString %>">Enter as Guest</a>
			</div>
		</form>
		<%-- } --%>
	</div>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/jquery.min.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/openid/openid-jquery.js")%>"></script>

	<script type="text/javascript"><!--
	//<![CDATA[
		$(document).ready(function() {
			openid.img_path = '<%= ResolveClientUrl("~/content/img/openid-logos.png")%>';
			openid.init('openid_identifier');
			//openid.setDemoMode(true);
		});
	//]]>-->
	</script>
</asp:Content>
