﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

	<h2>Rook</h2>

	<img class="imgl" src="../../content/img/wizard_tophat.png")" alt="Logo" />

	<h3>What is Rook?</h3>
	<p>
		Rook means Rook Abstract Game Engine (yes, it's a recursive name). It's a framework capable of running
		<a href="http://en.wikipedia.org/wiki/Abstract_strategy_game">abstract strategy</a> board turn-based games,
		such as <a href="http://en.wikipedia.org/wiki/Chess">Chess</a> and <a href="http://en.wikipedia.org/wiki/Checkers">Checkers</a>.
		The idea is not completely different from <a href="http://en.wikipedia.org/wiki/Zillions_of_Games">Zillions of Games</a>,
		even though both ideas were born independently.
	</p>
	<p>
		Rook is composed by two parts. The site, where you can play the games, is free. The framework is open source.
		Both of them are in early stage of development, let's say alpha. The site is missing user registration.
		Started games are not being persisted to the DB, yet. The framework has a lot to evolve, too.
		Contributors are more than welcome.
	</p>

	<h3>Why was Rook created?</h3>
	<p>
		Rook's objective is to support the implementation of games that don't need graphic effects to be interesting,
		even if they can become more interesting with such effects. In addition to strict abstract games like Chess,
		Rook will support games with random/luck factor, aka <a href="http://en.wikipedia.org/wiki/Stochastic_game">stochastic</a>. 
		The key feature will be the coverage of <a href="http://en.wikipedia.org/wiki/Complete_information">incomplete</a>
		and <a href="http://en.wikipedia.org/wiki/Perfect_information">imperfect</a> information games,
		like <a href="http://en.wikipedia.org/wiki/Stratego">Stratego</a>.
	</p>

	<h3>Which games are currently implemented?</h3>
	<ul>
		<li><a href="http://en.wikipedia.org/wiki/Tic-tac-toe">Tic-Tac-Toe</a>, just as a proof-of-concept;</li>
		<li><a href="http://en.wikipedia.org/wiki/Game_of_the_Amazons">Amazons</a>, a simple board game;</li>
		<li><a href="http://en.wikipedia.org/wiki/Go_(board_game)">Go</a>, a classic board game.</li>
	</ul>

	<h3>Which games are in the queue?</h3>
	<ul>
		<li><a href="http://en.wikipedia.org/wiki/Chess">Chess</a>;</li>
		<li><a href="http://en.wikipedia.org/wiki/Stratego">Stratego</a>, a simple partial information game;</li>
		<li><a href="http://en.wikipedia.org/wiki/Scrabble">Scrabble</a>, a not-so-simple partial information game,
		with a small stochastic factor;</li>
		<li>Duelo, a game that I created in my youth. Reimplementing it is the primary objective of Rook.</li>
	</ul>

	<h3>How can I contact you?</h3>

	<p>
		If you have ideas to suggest, go on and click the "feedback" button. No registration is necessary,
		and you can remain anonymous if you like.
		If you are a developer, web designer, or just a clever guy or girl that wants to contribute, please, shoot me an email.
	</p>

	<p>
		<span style="font-style:italic">João Paulo Bochi</span><br />
		jpbochi at gmail dot com
	</p>

	<div class="clear"></div>

</asp:Content>
