﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<KeyValuePair<string,Type>>>" %>

<%@ Import Namespace="JpLabs.Extensions" %>
<%@ Import Namespace="Rook.MvcApp.Models" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

	<h2>Which game would you like to play?</h2>

	<ul class="game-type-list">
		<% foreach (var gameType in Model) {
			var attr = gameType.Value.GetSingleAttr<Rook.GameModuleAttribute>(false);
		%>
			<li>
				<div>
					<%: Html.ActionLink(attr.DisplayName, "new", "game", new { id = gameType.Key }, new { @class = "button" }) %>
					<% if (!string.IsNullOrWhiteSpace(attr.WikiUrl)) { %>
						<span>(<a href="<%: attr.WikiUrl %>">rules</a>)</span>
					<% } %>
					<% if (attr.InDev) { %>
						<span>* This game is in development. Beware that some features might be missing. It might contain bugs to be fixed.</span>
					<% } %>
				</div>
			</li>
		<% } %>
	</ul>
</asp:Content>
