﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Rook.MvcApp.Models.GameViewModel>>" %>

<%@ Import Namespace="JpLabs.Extensions" %>
<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="Rook.MvcApp.Controllers" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

	<h2>Current games</h2>

	<%
	var games = Model.ToArray();
	if (!games.Any()) { %>
		<ul><li>No games are currently available.</li></ul>
	<% } else { %>
		<table class="game-list-table">
		  <thead>
				<tr>
					<th>Game Name</th>
					<th>Type</th>
					<th>Players</th>
					<th>Status</th>
					<th>Last move</th>
				</tr>
		  </thead>
		  <tbody>
			<%
			bool isLightRow = false;
			foreach (var game in games.Select(m => m.Entry)) {
				isLightRow = !isLightRow;
			%>
				<tr class="<%: isLightRow ? "light" : "dark" %>">
				<td><%: Html.ActionLink(game.DisplayName, PlayController.PlayActionName, PlayController.ControllerName, new { id = game.Id }, null) %></td>
				<td><%: game.GameType.GetDescription() %></td>
				<td><%= (!game.IsValid) ? "-" : string.Concat(game.State.Players.Select(p => p.GetHtmlName()).Intersperse(" vs. ")) %></td>
				<td>
					<% if (!game.IsValid) { %>
						<%: (game.LoadException == null) ? "?!" : game.LoadException.Message %>
					<% } else { %>
						<%= game.State.IsOver() ? "Over" : game.State.GetCurrentPlayer().GetHtmlName() + "'s turn" %>
					<% } %>
				</td>
				<td><%= (game.Record.LastPlayedAt == null) ? "-" : game.Record.LastPlayedAt.Value.FromUtcNow().ToSmartAgeDisplay() + " ago" %></td>
				</tr>
			<% } %>
		  </tbody>
		</table>
	<% } %>
</asp:Content>
