﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head>
<title>Rook - QUnit Tests</title>
	<link rel="stylesheet" href="<%= ResolveUrl("~/tests/lib/qunit.css")%>" type="text/css" media="screen">

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/jquery.min.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/linq.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/underscore-min.js")%>"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/extensions.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/board.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameConsts.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameController.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameParser.js")%>"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/tests/lib/qunit.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/lib/sinon.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/lib/sinon-qunit.js")%>"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/tests/piece.tests.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/board.tests.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/gameParser.tests.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/gameController.tests.js")%>"></script>

</head>
<body>
<h1 id="qunit-header">Rook - QUnit Tests</h1>
<h2 id="qunit-banner"></h2>
<div id="qunit-testrunner-toolbar"></div>
<h2 id="qunit-userAgent"></h2>
<ol id="qunit-tests"></ol>
<div id="qunit-fixture">test markup</div>
</body>
</html>