﻿/*globals module, test, ok, expect, equal, same, deepEqual */
/*globals Piece */

/// <reference path="../scripts/game/board.js" />
/// <reference path="lib/qunit.js" />

module("Piece");

test("new Piece", function() {
	var id = '';
	var position = {};
	var img = {};

	var piece = new Rook.Piece(id, position, img);

	equal(piece.id, id, "piece.id");
	equal(piece.position, position, "piece.position");
	equal(piece.img, img, "piece.img");
});

test("move(destination) changes piece.position", function() {
	var testMove = function(origin, destination) {
		var piece = new Rook.Piece('', origin, {});

		piece.move(destination);

		deepEqual(piece.position, destination);
	};

	testMove({x:0, y:0}, {x:0, y:1});
	testMove({x:0, y:1}, {x:1, y:1});
	testMove({x:1, y:1}, {x:1, y:0});
	testMove({x:1, y:0}, {x:0, y:0});
});

test("move() resets piece.position", function() {
	var testMove = function(origin, destination) {
		var piece = new Rook.Piece('', origin, {});

		piece.move(destination);
		piece.move();

		deepEqual(piece.position, origin);
	};

	testMove({x:0, y:0}, {x:0, y:1});
	testMove({x:0, y:1}, {x:1, y:1});
	testMove({x:1, y:1}, {x:1, y:0});
	testMove({x:1, y:0}, {x:0, y:0});
});

test("setZorder()", function() {
	var piece = new Rook.Piece('', {}, {});
	var zorder = {};

	piece.setZorder(zorder);

	deepEqual(piece.zorder, zorder);
});

test("resetZorder()", function() {
	var piece = new Rook.Piece('', {}, {});
	var zorder = {};

	piece.setZorder(zorder);
	piece.resetZorder();

	deepEqual(piece.zorder, undefined);
});
