﻿/*globals module, test, ok, expect, equal, same, deepEqual */
/*globals Game, Rook */

/// <reference path="../scripts/game/board.js" />
/// <reference path="../scripts/game/gameconsts.js" />
/// <reference path="../scripts/game/gamecontroller.js" />
/// <reference path="lib/qunit.js" />
/// <reference path="lib/sinon.js" />
/// <reference path="lib/sinon-qunit.js" />

module("Game Controller",
	{
		getAvailableMoves: function() {
			return {
				"white":[
					{"player":"white", "link":{"href":"/resign/white"}},
					{"player":"white", "origin":"a1", "destination":"a2", "link":{"href":"/move/white/1/a2"}},
					{"player":"white", "origin":"a1", "destination":"b1", "link":{"href":"/move/white/0/b1"}},
					{"player":"white", "origin":"c4", "destination":"c3", "link":{"href":"/move/white/1/c3"}},
					{"player":"white", "destination":"c1", "link":{"href":"/drop/white/X/c1"}}
				],
				"black":[
					{"player":"black", "link":{"href":"/resign/black"}},
					{"player":"black", "origin":"c4", "destination":"b2", "link":{"href":"/move/black/1/b2"}},
					{"player":"black", "destination":"a4", "link":{"href":"/drop/black/O/a4"}},
					{"player":"black", "destination":"a3", "link":{"href":"/drop/black/O/a3"}}
				]
			};
		},

		getBoard: function () {
			return new Rook.Board([
				{ name: 'a1', position: { x: 0, y: 0 } },
				{ name: 'h8', position: { x: 7, y: 7 } }
			], [
				new Rook.Piece('0', 'a1'),
				new Rook.Piece('1', 'c4')
			]);
		},
		
		getController: function() {
			return new Rook.GameController(this.getBoard(), "white", this.getAvailableMoves(), this.getGraphics());
		},
		getGraphics: function() {
			return {
				origin: 'origin',
				target: 'target',
				move: 'move'
			};
		},

		testClickSequence: function(clicks, expectedMode, expectedSelection) {
			var controller = this.getController();

			clicks.each(function(click) {
				controller.click(click);
			});

			strictEqual(controller.currentMode(), expectedMode, 'controller mode');
			deepEqual(controller.selection(), expectedSelection, 'controller selection');

			return controller;
		}
	}
);

test("should start in Start mode", function() {
	var controller = new Rook.GameController();

	equal(controller.currentMode(), Game.Modes.Start, "controller.currentMode()");
});

test("should reset mode to Start and clean selection", function() {
	var controller = new Rook.GameController();
	controller.currentMode({});
	controller.selection().piece = {
		move: function() { ok(true, "piece.move() was called"); },
		resetZorder: function() { ok(true, "piece.resetZorder() was called"); }
	};

	controller.resetMode();

	strictEqual(controller.currentMode(), Game.Modes.Start);
	deepEqual(controller.selection(), { origin: null, target: null });
	expect(4);
});

test("click(empty) ==> DropSelected", function() {
	var click = 'b2';

	this.testClickSequence([click], Game.Modes.DropSelected, { origin: null, target: click });
});

test("click(empty) + click(same empty) ==> Start", function() {
	var click = {name: 'b2'};

	this.testClickSequence([click, click], Game.Modes.Start, { origin: null, target: null });
});

test("click(empty) + click(other empty) === DropSelected", function() {
	var empty1 = 'b2';
	var empty2 = 'c3';

	this.testClickSequence([empty1, empty2], Game.Modes.DropSelected, { origin: null, target: empty2 });
});

test("click(empty) + click(piece) ==> PieceSelected", function() {
	var empty = 'b2';
	var piece = 'a1';

	this.testClickSequence([empty, piece], Game.Modes.PieceSelected, { origin: null, target: piece });
});

test("click(piece) ==> PieceSelected", function() {
	var click = 'a1';

	this.testClickSequence([click], Game.Modes.PieceSelected, { origin: null, target: click });
});

test("click(piece) + click(same piece) ==> Start", function() {
	var click = 'a1';

	this.testClickSequence([click, click], Game.Modes.Start, { origin: null, target: null });
});

test("click(piece) + click(empty destination) ==> MoveSelected", function() {
	var origin = 'a1';
	var destination = 'b2';
	var piece = this.getBoard().getPieceById('0');
	piece.move(destination);
	piece.zorder = 1;

	this.testClickSequence([origin, destination], Game.Modes.MoveSelected, { origin: origin, target: destination, piece: piece });
});

test("click(piece) + click(other piece) ==> MoveSelected with capture", function() {
	var origin = 'a1';
	var destination = 'b4';
	var piece = this.getBoard().getPieceById('0');
	piece.move(destination);
	piece.zorder = 1;

	this.testClickSequence([origin, destination], Game.Modes.MoveSelected, { origin: origin, target: destination, piece: piece });
});

test("click(piece) + click(empty destination) + click(anywhere) ==> Start and unmoved piece", function() {
	var origin = 'a1';
	var destination = 'b2';
	var third = 'c3';

	var controller = this.testClickSequence([origin, destination, third], Game.Modes.Start, { origin: null, target: null });

	var piece = this.getBoard().getPieceById('0');
	deepEqual(piece.position, origin, "selected piece should move back to origin");
});

test("click(piece) + click(other piece) + click(anywhere) ==> Start with no capture", function() {
	var origin = 'a1';
	var destination = 'c4';
	var third = 'c3';

	var controller = this.testClickSequence([origin, destination, third], Game.Modes.Start, { origin: null, target: null });

	var piece = this.getBoard().getPieceById('0');
	deepEqual(piece.position, origin, "selected piece should move back to origin");
});

test("should highlight drop moves in Start mode", function() {
	var controller = this.getController();

	deepEqual(controller.getBoardHighlights(), [
		{ position: 'c1', graphic: controller.graphics('move') } //DropMove
	]);
});

test("should highlight selected drop move and other drop moves in DropSelected mode", function() {
	var controller = this.getController();

	var dropPos = 'b1';
	controller.currentMode(Game.Modes.DropSelected);
	controller.selection({ origin: null, target: dropPos });

	deepEqual(controller.getBoardHighlights(), [
		{ position: dropPos, graphic: controller.graphics('target') },
		{ position: 'c1', graphic: controller.graphics('move') } //DropMove
	]);
});

test("should highlight valid destinations in PieceSelected mode", function() {
	var controller = this.getController();

	var piecePos = 'a1';
	controller.currentMode(Game.Modes.PieceSelected);
	controller.selection({ origin: null, target: piecePos });

	deepEqual(controller.getBoardHighlights(), [
		{ position: piecePos, graphic: controller.graphics('target') },
		{ position: 'a2', graphic: controller.graphics('move') }, //MovePiece
		{ position: 'b1', graphic: controller.graphics('move') } //MovePiece
	]);
});

test("should highlight getBoardHighlights() in MoveSelected mode", function() {
	var controller = this.getController();

	var piecePos = 'a1';
	var destination = 'b2';
	controller.currentMode(Game.Modes.MoveSelected);
	controller.selection({ origin: piecePos, target: destination });

	deepEqual(controller.getBoardHighlights(), [
		{ position: piecePos, graphic: controller.graphics('origin') },
		{ position: destination, graphic: controller.graphics('target') },
		{ position: 'a2', graphic: controller.graphics('move') }, //MovePiece
		{ position: 'b1', graphic: controller.graphics('move') } //MovePiece
	]);
});

test("getSelectedMove() in Start mode", function() {
	var controller = this.getController();

	strictEqual(controller.getSelectedMove(), null);
});

test("should return no selected move in invalid DropSelected mode", function() {
	var controller = this.getController();

	var dropPos = 'b2';
	controller.currentMode(Game.Modes.DropSelected);
	controller.selection({ origin: null, target: dropPos });

	strictEqual(controller.getSelectedMove(), null);
});

test("should get selected move in valid DropSelected mode", function() {
	var controller = this.getController();

	controller.currentMode(Game.Modes.DropSelected);
	controller.selection({
		origin: null,
		target: 'c1'
	});

	var expectedMove = { player: 'white', destination: 'c1', link: { href: "/drop/white/X/c1" } };
	deepEqual(controller.getSelectedMove(), expectedMove);
});

test("should return null selected move in PieceSelected mode", function() {
	var controller = this.getController();
	controller.currentMode(Game.Modes.PieceSelected);

	strictEqual(controller.getSelectedMove(), null);
});

test("should return null selected move in invalid MoveSelected mode", function() {
	var controller = this.getController();

	var piecePos = 'a1';
	var destination = 'b2';
	controller.currentMode(Game.Modes.MoveSelected);
	controller.selection({ origin: piecePos, target: destination });

	strictEqual(controller.getSelectedMove(), null);
});

test("should get selected move in valid MoveSelected mode", function() {
	var controller = this.getController();

	controller.currentMode(Game.Modes.MoveSelected);
	controller.selection({
		origin: 'a1',
		target: 'b1'
	});
	
	var expectedMove = { player: 'white', origin: 'a1', destination: 'b1', link: { href: "/move/white/0/b1" } };
	deepEqual(controller.getSelectedMove(), expectedMove);
});
