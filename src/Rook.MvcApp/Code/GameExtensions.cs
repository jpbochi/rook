﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rook.Augmentation;
using Rook.Games.Amazons;
using Rook.Games.ChineseCheckers;
using Rook.Games.Go;
using Rook.Games.TicTacToe;
using Rook.MvcApp.RookAuthentication;

namespace Rook.MvcApp
{
	public static class GameMachineExt
	{
		public static IEnumerable<IMoveLink> GetMoveOptions(this IGameMachine game, IOpenIdIdentity user)
		{
			var playersControlledByUser = game.State.Players.Where(p => p.IsControlledBy(user)).Select(p => p.Id).ToArray();

			return game.GetMoveOptions().Where(m => playersControlledByUser.Contains(m.GetPlayerId()));
		}

		public static string GetJSBoardRendererFunc(this IGameMachine game)
		{
			return game.JSRenderer == null ? "null" : game.JSRenderer.GetBoardRendererArgsConstructor();
		}

		public static void RenderResourceImages(this IGameMachine game, HtmlTextWriter writer, UrlHelper Url)
		{
			foreach (var res in game.Resources.OfType<WebResourceInfo>().Where(r => r.MediaType.Type == MediaType.Image)) {
				writer.Write(res.AsHtml(Url.GetResourceUrl(res)));
			}
		}
	}

	public static class GameStateExt
	{
		public static bool CanPlayAsCurrentPlayer(this IGameState state, IOpenIdIdentity user)
		{
			if (state.IsOver()) return false;
			if (user == null) return false;

			var currentPlayer = state.GetCurrentPlayer();
			return currentPlayer.IsControlledBy(user);
		}

		public static bool CanPlayOrWaitForTurn(this IGameState state, IOpenIdIdentity user)
		{
			if (state.IsOver()) return false;
			if (user == null) return false;

			return state.Players.Any(p => p.IsControlledBy(user));
		}

		public static bool CanJoinAsPlayer(this IGameState state, IOpenIdIdentity user, RName playerId)
		{
			if (state.IsOver()) return false;
			if (user == null) return false;

			var player = state.GetPlayerById(playerId);
			if (player == null) return false;
			
			return player.IsOpen();
		}

		public static bool TryJoinAsPlayer(this IGameState state, IOpenIdIdentity user, RName playerId)
		{
			if (!CanJoinAsPlayer(state, user, playerId)) return false;

			var player = state.GetPlayerById(playerId);
			if (player == null) return false;

			player.SetController(user);
			return true;
		}
	}

	public static class PlayerExt
	{
		public static AugProperty NameProperty =
			AugProperty.Create("Name", typeof(string), typeof(PlayerExt), null);

		public static AugProperty ControllerIdProperty =
			AugProperty.Create("ControllerId", typeof(string), typeof(PlayerExt), null);

		public static bool IsOpen(this IGamePlayer player)
		{
			return player.IsValueUnset(ControllerIdProperty);
		}

		public static string GetHtmlName(this IGamePlayer player)
		{
			return player.IsOpen() ? "<i>open</i>" : MvcHtmlString.Create(player.GetName()).ToHtmlString();
		}

		public static string GetName(this IGamePlayer player)
		{
			return player.GetValue<string>(NameProperty);
		}

		public static string GetControllerId(this IGamePlayer player)
		{
			return player.GetValue<string>(ControllerIdProperty);
		}

		public static IGamePlayer SetController(this IGamePlayer player, IOpenIdIdentity identity)
		{
			player.SetValue(ControllerIdProperty, identity.OpenId);
			player.SetValue(NameProperty, identity.Name);
			return player;
		}

		public static bool IsControlledBy(this IGamePlayer player, IOpenIdIdentity identity)
		{
			return player.GetControllerId() == identity.OpenId;
		}

		public static string GetImage(this IGamePlayer player)
		{
			if (player.Id == ClassicPlayer.Black) return "playerblack.gif";

			if (player.Id == TicTacToePlayer.O) return "playerblack.gif";

			return "playerwhite.gif";
		}

		public static string GetExtraScoreDisplay(this IGamePlayer player)
		{
			if (player.IsValueUnset(GoProperties.AreaProperty)) return null;

			return string.Concat("Score: ", player.GetArea());
		}
	}

	public static class PieceExt
	{
		public static Point GetDisplayImagePosition(this IGamePiece piece)
		{
			if (piece == null) return default(Point);

			var pieceClass = piece.Class;
			var pieceBanner = piece.GetBanner();

			if (pieceClass == null) new Point(7, 1);

			if (pieceClass.Namespace == GoPieces.Namespace) {
				return (pieceClass == GoPieces.BlackStone) ? new Point(0, 1)
					 : (pieceClass == GoPieces.WhiteStone) ? new Point(0, 0)
					 : new Point(7, 1);
			}

			if (pieceClass.Namespace == AmazonsPieces.Namespace) {
				return (pieceClass == AmazonsPieces.Arrow) ? new Point(7, 0)
					 : (pieceBanner == ClassicPlayer.White) ? new Point(2, 0)
					 : (pieceBanner == ClassicPlayer.Black) ? new Point(2, 1)
					 : new Point(7, 1);
			}

			if (pieceClass.Namespace == ChineseCheckersPiece.Namespace) {
				return (pieceClass == ChineseCheckersPiece.North) ? new Point(1, 0)
					 : (pieceClass == ChineseCheckersPiece.South) ? new Point(6, 0)
					 : (pieceClass == ChineseCheckersPiece.Northeast) ? new Point(2, 0)
					 : (pieceClass == ChineseCheckersPiece.Northwest) ? new Point(3, 0)
					 : (pieceClass == ChineseCheckersPiece.Southeast) ? new Point(4, 0)
					 : (pieceClass == ChineseCheckersPiece.Southwest) ? new Point(5, 0)
					 : new Point(0, 1);
			}

			return (pieceClass == TicTacToePieces.X) ? new Point(0, 0)
				 : (pieceClass == TicTacToePieces.O) ? new Point(0, 1)
				 : new Point(7, 1);
		}
	}
}
