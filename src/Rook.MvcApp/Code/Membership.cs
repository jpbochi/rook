﻿using System.Web.Security;

namespace Rook.MvcApp.RookMembership
{
	public interface IMembershipService
	{
		MembershipUser GetUserByUserName(string username, bool userIsOnline);

		MembershipUser GetUserByOpenId(string openId, bool userIsOnline);

		MembershipCreateStatus CreateUser(string openId, string username, string email);
	}

	public class AccountMembershipService : IMembershipService
	{
		private MembershipProvider provider;

		public AccountMembershipService() : this(null) {}

		public AccountMembershipService(MembershipProvider provider)
		{
			this.provider = provider ?? Membership.Provider;
		}

		public MembershipUser GetUserByUserName(string username, bool userIsOnline)
		{
			return provider.GetUser(username, userIsOnline);
		}

		public MembershipUser GetUserByOpenId(string openId, bool userIsOnline)
		{
			return provider.GetUser((object)openId, userIsOnline);
		}

		public MembershipCreateStatus CreateUser(string openId, string username, string email)
		{
			MembershipCreateStatus status;
			provider.CreateUser(username, username, email, null, null, true, openId, out status);
			return status;
		}
	}
}