﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace Rook.MvcApp
{
	using Rook.MvcApp.RookAuthentication;

	public static class RookIdentityEx
	{
		public static IOpenIdIdentity GetOpenIdIdentity(this IPrincipal principal)
		{
			return principal.Identity.ToOpenIdIdentity();
		}

		public static IOpenIdIdentity ToOpenIdIdentity(this IIdentity identity)
		{
			return (IOpenIdIdentity)identity;
		}
	}
}

namespace Rook.MvcApp.RookAuthentication
{
	public interface IFormsAuthentication
	{
		void SignIn(string userName, string friendlyName);
		void SignOut();
	}

	public class FormsAuthenticationService : IFormsAuthentication
	{
		public void SignIn(string userName, string friendlyName)
		{
			var authTicket = RookIdentity.GenerateTicket(userName, friendlyName);

			string encTicket = FormsAuthentication.Encrypt(authTicket);

			HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
		}

		public void SignOut()
		{
			FormsAuthentication.SignOut();
		}
	}

	public interface IOpenIdIdentity : IIdentity
	{
		string OpenId { get; }
	}

	internal class RookIdentity : IOpenIdIdentity
	{
		private FormsAuthenticationTicket ticket;

		public RookIdentity(FormsAuthenticationTicket ticket)
		{
			this.ticket = ticket;
		}

		public static FormsAuthenticationTicket GenerateTicket(string userName, string friendlyName = null, bool createPersistentCookie = false)
		{
			return new FormsAuthenticationTicket(
				1, //version
				userName, // user name
				DateTime.Now, //creation
				DateTime.Now.AddHours(12), //Expiration
				createPersistentCookie,
				string.IsNullOrWhiteSpace(friendlyName) ? userName : friendlyName
			);
		}

		public string AuthenticationType
		{
			get { return "Rook"; }
		}

		public bool IsAuthenticated
		{
			get { return true; }
		}

		public string OpenId
		{
			get { return ticket.Name; }
		}

		public string FriendlyName
		{
			get { return ticket.UserData; }
		}

		string IIdentity.Name
		{
			get { return FriendlyName; }
		}
	}

	internal class RookPrincipal : IPrincipal
	{
		private IGameState state;
		private IOpenIdIdentity identity;

		public IIdentity Identity { get { return identity; } }

		private RookPrincipal(IOpenIdIdentity identity, IGameState state)
		{
			this.identity = identity;
			this.state = state;
		}

		public static IPrincipal From(IOpenIdIdentity identity, IGameState state)
		{
			return new RookPrincipal(identity, state);
		}

		public bool IsInRole(string role)
		{
			var player = state.GetPlayerById(role);
			return player != null && player.IsControlledBy(identity);
		}
	}
}