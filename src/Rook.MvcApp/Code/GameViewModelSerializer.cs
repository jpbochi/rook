﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using JpLabs.Extensions;

namespace Rook.MvcApp
{
	public interface IGameViewModelSerializer
	{
		JToken From(IGameState state);
		JToken From(IGameMachine machine, RName player);
	}

	internal class GameViewModelSerializer : IGameViewModelSerializer
	{
		static readonly Uri BaseUri = new Uri("http://rook");
		static UriTemplate gameForPlayerUri = new UriTemplate("/game/{gameid}/for/{playerid}");

		private JToken GetPlayer(IGamePlayer player, string gameId)
		{
			return new JObject {
				{ "id", player.Id.FullName },
				{ "score", player.Score.Name },
				{ "links", new JArray(
					new JObject{
						{ "rel", "game-for-player" },
						{ "href", gameForPlayerUri.BindBy(BaseUri, new { gameId, playerid = player.Id }).PathAndQuery }
					}
				)}
			};
		}

		private JToken GetPlayers(IGameState state)
		{
			return new JArray(state.Players.Select(p => GetPlayer(p, state.Id)));
		}

		private JArray GetTilesOnMainBoard(IGameBoard board)
		{
			if (board == null || board.Graph == null) return new JArray();
			return new JArray(
				board
				.Region(Tiles.MainBoard)
				.Where(t => t.ToPointInt() != null)
				.Select(t => {
					var name = t.ToString();
					var position = t.ToPointInt().Value;
					return new JObject {
						{ "name", name },
						{ "position", new JObject { { "x", position.X }, { "y", position.Y } } }
					};
				})
			);
		}

		private JArray GetPieces(IGameBoard board)
		{
			if (board == null || board.Pieces == null) return new JArray();
			return new JArray(
				board
				.Pieces
				.Select(p => {
					var imagePos = p.GetDisplayImagePosition();
					return new JObject {
						{ "id", (string)p.Id },
						{ "position", (string)p.Position },
						{ "imagePos", new JObject { { "x", imagePos.X }, { "y", imagePos.Y } } }
					};
				})
			);
		}

		private JToken GetMovesForPlayer(IGameMachine machine, RName player)
		{
			return new JArray(
				machine.GetMoveOptions()
				.Where(m => m.GetPlayerId() == player)
				.Select(m => {
					var data = new JObject();
					m.Data.ToObjectDictionary().ForEach(
						kvp => data.Add(kvp.Key, kvp.Value.ToString())
					);
					return new JObject { { "href", m.Href.ToString() }, { "rel", m.Rel }, { "data", data } };
				})
			);
		}

		public JToken From(IGameState state)
		{
			var jsonState = new JObject();
			jsonState.Add("version", state.Version);
			jsonState.Add("currentPlayer", (string)state.CurrentPlayerId);
			jsonState.Add("players", GetPlayers(state));
			jsonState.Add("tiles", GetTilesOnMainBoard(state.Board));
			jsonState.Add("pieces", GetPieces(state.Board));
			return jsonState;
		}

		public JToken From(IGameMachine machine, RName player)
		{
			var jsonGame = new JObject();
			jsonGame.Add("state", From(machine.State));
			jsonGame.Add("moves", GetMovesForPlayer(machine, player));
			return jsonGame;
		}
	}
}