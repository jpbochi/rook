﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Rook.MvcApp.Models
{
	public class GameMoveRequest
	{
		public class MoveDesc
		{
			[Required]
			public string MoveType { get; set; }

			public string Origin { get; set; }
			public string Target { get; set; }
		}

		[Required]
		public int GameId { get; set; }

		public MoveDesc Move { get; set; }
	}

	public class GamePlayResult
	{
		public string Message { get; set; }
	}

	public class GameViewModel
	{
		public GameViewModel() {}

		public GameViewModel(GameEntry model) : this()
		{
			this.Entry = model;
		}

		public GameEntry Entry { get; private set; }
	}
}