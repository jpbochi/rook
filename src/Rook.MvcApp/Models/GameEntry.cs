﻿using System;
using JpLabs.Extensions;
using Rook.Games;
using Rook.MvcApp.Entities;

namespace Rook.MvcApp.Models
{
	public class GameEntry
	{
		public GameRec Record { get; private set; }

		public IGameMachine Machine { get; private set; }
		public Exception LoadException { get; private set; }
		public Type GameType { get; private set; }

		public GameEntry() : this(new GameRec()) {}

		public GameEntry(GameRec record)
		{
			this.Record = record;
		}

		public static GameEntry CreateNew(IGameMachine game, NewGameOptionsViewModel gameParams, Type gameType)
		{
			return CreateNew(game, gameParams.CreatorIdentity.OpenId, gameParams.GameName, gameType);
		}

		public static GameEntry CreateNew(IGameMachine game, string creatorOpenId, string gameName, Type gameType)
		{
			var entry = new GameEntry() {
				Name = gameName,
				GameType = gameType,
				Machine = game
			};
			entry.Record.OwnerUserId = creatorOpenId;
			return entry;
		}

		public int Id
		{
			get { return Record.Id; }	
		}

		public string Name
		{
			get { return Record.Name; }
			set { Record.Name = value; }
		}

		public string DisplayName
		{
			get { return Name ?? string.Format("Game #{0}", Id); }
		}

		public IGameState State
		{
			get { return (Machine == null) ? null : Machine.State; }
		}

		public bool IsValid
		{
			get { return LoadException == null && State != null && State.Board != null && State.Players != null; }
		}

		public GameRec GetRecord(IModuleLocator moduleLocator)
		{
			Record.GameTypeName = moduleLocator.GetTypeName(GameType);
			Record.ZippedData = GameSerializer.Serialize(Machine.State).Compress();
			return Record;
		}

		public static GameEntry FromRecord(GameRec record, IModuleLocator moduleLocator)
		{
			if (record == null) return null;

			var entry = new GameEntry(record);

			entry.GameType = moduleLocator.FindType(record.GameTypeName);

			var data = record.ZippedData;
			if (entry.GameType != null && data != null) {
				IGameState state;
				try {
					state = GameSerializer.Deserialize<IGameState>(data.DecompressToString());
					state.Id = record.Id.ToString();
					entry.Machine = StaticGameFactory.GetGame(entry.GameType, state);
				} catch (Exception ex) {
					entry.LoadException = ex;
				}
			}

			return entry;
		}
	}
}