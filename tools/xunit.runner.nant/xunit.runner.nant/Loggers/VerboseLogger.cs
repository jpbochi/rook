﻿using NAnt.Core;

namespace Xunit.Runner.Nant
{
    public class VerboseLogger : StandardLogger
    {
        public VerboseLogger(Task log) : base(log) { }

        public override void TestPassed(string name, string type, string method, double duration, string output)
        {
            log.Log(Level.Verbose, "    PASS:  {0}", name);

            WriteOutput(output);
        }

        public override bool TestStart(string name, string type, string method)
        {
            log.Log(Level.Verbose, "    START: {0}", name);
            return true;
        }
    }
}