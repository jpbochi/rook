﻿using System;
using NAnt.Core;

namespace Xunit.Runner.Nant
{
    public class StandardLogger : IRunnerLogger
    {
        protected readonly Task log;
        public int Total = 0;
        public int Failed = 0;
        public int Skipped = 0;
        public double Time = 0.0;

        public StandardLogger(Task log)
        {
            this.log = log;
        }

        public void AssemblyFinished(string assemblyFilename, int total, int failed, int skipped, double time)
        {
            log.Log(Level.Info,
                           "  Tests: {0}, Failures: {1}, Skipped: {2}, Time: {3} seconds",
                           total,
                           failed,
                           skipped,
                           time.ToString("0.000"));

            Total += total;
            Failed += failed;
            Skipped += skipped;
            Time += time;
        }

        public void AssemblyStart(string assemblyFilename, string configFilename, string xUnitVersion)
        {
        }

        public bool ClassFailed(string className, string exceptionType, string message, string stackTrace)
        {
            log.Log(Level.Error, "[CLASS] {0}: {1}", className, Escape(message));
            log.Log(Level.Error, Escape(stackTrace));
            return true;
        }

        public void ExceptionThrown(string assemblyFilename, Exception exception)
        {
            log.Log(Level.Error, exception.Message);
            log.Log(Level.Error, "While running: {0}", assemblyFilename);
        }

        public void TestFailed(string name, string type, string method, double duration, string output, string exceptionType, string message, string stackTrace)
        {
            log.Log(Level.Error, "{0}: {1}", name, Escape(message));
            log.Log(Level.Error, Escape(stackTrace));
            WriteOutput(output);
        }

        public bool TestFinished(string name, string type, string method)
        {
            return true;
        }

        public virtual void TestPassed(string name, string type, string method, double duration, string output)
        {
            log.Log(Level.Verbose, "    {0}", name);
            WriteOutput(output);
        }

        public void TestSkipped(string name, string type, string method, string reason)
        {
            log.Log(Level.Warning, "{0}: {1}", name, Escape(reason));
        }

        public virtual bool TestStart(string name, string type, string method)
        {
            return true;
        }

        static string Escape(string value)
        {
            return value.Replace(Environment.NewLine, "\n");
        }

        protected void WriteOutput(string output)
        {
            if (output != null)
            {
                log.Log(Level.Info, "    Captured output:");
                foreach (string line in output.Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                    log.Log(Level.Info, "      {0}", line);
            }
        }
    }
}